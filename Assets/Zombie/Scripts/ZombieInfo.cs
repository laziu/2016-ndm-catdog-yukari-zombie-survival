﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class ZombieInfo : NetworkBehaviour {

    [SyncVar]
    public float health;
	[SyncVar]
	public bool dead;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    [Command]
    public void CmdAddHealth(float value)
    {
        health += value;
    }
}
