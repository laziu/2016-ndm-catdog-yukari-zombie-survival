﻿using UnityEngine;
using System.Collections.Generic;

public class CorpseCollector : MonoBehaviour {

	#region Singleton
	private static CorpseCollector _instance = null;
	public static CorpseCollector Instance
	{
		get
		{
			if (_instance == null) Debug.LogError("CorpseCollector has not awaken.");
			return _instance;
		}
	}

	void Awake()
	{
		if (_instance != null)
		{
			Destroy(this.gameObject);
			return;
		}
		else
		{
			_instance = this;
		}
	}
	#endregion

	private Queue<GameObject> _corpses;

	// Use this for initialization
	void Start () {
		_corpses = new Queue<GameObject>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void AddCorpse(GameObject corpse)
	{
		if (corpse != null)
			_corpses.Enqueue(corpse);

		if (_corpses.Count > 30)
		{
			GameObject garbage = _corpses.Dequeue();
			Destroy(garbage);
		}
	}
}
