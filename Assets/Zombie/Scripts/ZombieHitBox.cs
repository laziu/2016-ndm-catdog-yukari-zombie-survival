﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(BoxCollider))]
public class ZombieHitBox : MonoBehaviour {

	private List<Barricade> _barricades;
	private List<GameObject> _players;

	public List<Barricade> GetBarricades() { return _barricades; }
	public List<GameObject> GetPlayers() { return _players; }

	// Use this for initialization
	void Start () {
		_barricades = new List<Barricade>();
		_players = new List<GameObject>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public bool IsEmpty { get { return (_barricades.Count == 0) && (_players.Count == 0); } }

	void OnTriggerEnter(Collider c)
	{
		Barricade barricade = c.gameObject.GetComponent<Barricade>();
		if (barricade != null)
		{
			// zombie only cares planted barricades
			if(barricade.planted)
			{
				_barricades.Add(barricade);
				return;
			}
		}

		PlayerControl control = c.gameObject.GetComponent<PlayerControl>();
		if(control != null)
		{
			_players.Add(c.gameObject);
		}
	}

	void OnTriggerExit(Collider c)
	{
		Barricade barricade = c.gameObject.GetComponent<Barricade>();
		if (_barricades.Contains(barricade))
		{
			_barricades.Remove(barricade);
			return;
		}

		if(_players.Contains(c.gameObject))
		{
			_players.Remove(c.gameObject);
		}
	}
}
