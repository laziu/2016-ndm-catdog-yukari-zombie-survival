﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(NavMeshAgent), typeof(CharacterController))]
public class ZombieControl : Attackable {

    private FSMSystem _fsm;
    private NavMeshAgent _agent;
    private CharacterController _controller;
    private ZombieAnimation _animator;
    private ZombieInfo _info;
    private Vector3 _moveDirection = Vector3.zero;
	private float _originalSpeed;

	private float _attackTimer;
	private float _idleSoundTimer;

	
    public float gravity;
    public float detectionDistance;
	[Tooltip("Attack Cooldown Time")]
	public float cooldownTime;
	[Tooltip("The time gap between when zombie starts to attack and when damage occurs")]
	public float swingTime;

	[Space]
	public float damage;

	[Space]
	public bool attackWhileMoving;

	[Header("Flesh Meshes")]
	public GameObject face;
	public GameObject torso;
	public GameObject rightArm;
	public GameObject leftArm;
	public GameObject rightLeg;
	public GameObject leftLeg;

	[Header("SE")]
	public AudioClip[] pain;
	public AudioClip[] die;
	public AudioClip[] fleshImpact;
	public AudioClip[] idle;
	public float idleSoundInterval;
	public AudioClip[] fight;
	public AudioClip[] hitPlayer;
	public AudioClip[] poundBarricade;
	
	public AudioSource fleshAudio;
	public AudioSource voiceAudio;
	public AudioSource attackAudio;

	// Objective determined by the internal FSM
	// The zombies are chasing the objective
	public GameObject Objective { get; set; }

    // Very Important Objective
    // Objective determined by external intervention
    // It has high priority than Objective
    public GameObject VIO { get; set; }

    // Set false if you want control a zombie manually
    public bool EnableFSM { get; set; }

    public void SetTransition(Transition t) { if (_fsm != null) _fsm.PerformTransition(t); }
    public void SetDestination(Vector3 position) { _agent.SetDestination(position); }

	public ZombieHitBox HitBox { get; set; }


    [SyncVar]
    public bool isMoving;
	[SyncVar]
	public bool isAttacking;
	[SyncVar]
	public Vector3 latestImpulse;
	[SyncVar]
	public float latestDamage;

    // Direction to which zombie wants to go
    // ZombieAnimation requires this field to know where the zombie should look at
    [SyncVar]
    public Vector3 focus;                   

	// Use this for initialization
    [ServerCallback]
	void Start () {
        // Initiate FSM
        MakeFSM();
        EnableFSM = true;

        // RequireComponent attribute guerrantees NavMeshAgent is not null
        _agent = GetComponent<NavMeshAgent>();
		_originalSpeed = _agent.speed;

		// RequireComponent attribute guerrantees CharacterController is not null
		_controller = GetComponent<CharacterController>();

        _info = GetComponent<ZombieInfo>();
        if (_info == null) Debug.LogError("Zombie : Zombie Info not found.");

        _animator = GetComponent<ZombieAnimation>();
        if (_animator == null) Debug.LogError("Zombie : ZombieAnimation not found.");
		

		Transform hitBox = transform.Find("Hit Box");
		if (hitBox != null)
			HitBox = hitBox.GetComponent<ZombieHitBox>();
		else
			Debug.LogError("Zombie : Zombie Hit Box not found.");

		// Idle timer random setting
		_idleSoundTimer = Random.Range(0f, 100f);
	}
	
	// Update is called once per frame
    [ServerCallback]
	void Update () {

        if(EnableFSM)
        {
            // Player Detection
            PlayerDetection();

            // Clear the eliminated objectives
            ClearEliminated();

            // FSM
            if(VIO == null)
            {
                _fsm.CurrentState.Reason(Objective, this.gameObject);
                _fsm.CurrentState.Act(Objective, this.gameObject);
            }
            else
            {
                _fsm.CurrentState.Reason(VIO, this.gameObject);
                _fsm.CurrentState.Act(VIO, this.gameObject);
            }
        }

        _moveDirection.y -= gravity * Time.deltaTime;
        _controller.Move(_moveDirection * Time.deltaTime);

        if(_agent.velocity.sqrMagnitude > 0)
        {
            isMoving = true;
        }
        else
        {
            isMoving = false;
        }

		if (_attackTimer < cooldownTime)
		{
			_attackTimer += Time.deltaTime;
		}
		else
		{
			_attackTimer = cooldownTime;
		}

		if (_idleSoundTimer < idleSoundInterval)
		{
			_idleSoundTimer += Time.deltaTime;
		}
		else
		{
			RpcPlayIdleSound();
			_idleSoundTimer = 0f;
		}
	}

    private void MakeFSM()
    {
        IdleState idle = new IdleState();

        WanderState wander = new WanderState();
        wander.AddTransition(Transition.DetectPlayer, StateID.Chase);

        MoveState move = new MoveState();

        ChaseState chase = new ChaseState(this.gameObject);
        chase.AddTransition(Transition.LostPlayer, StateID.Wander);
        chase.AddTransition(Transition.AttackPlayer, StateID.Attack);

        AttackState attack = new AttackState();
        attack.AddTransition(Transition.EliminateObjective, StateID.Wander);
        attack.AddTransition(Transition.OutOfRange, StateID.Chase);

        _fsm = new FSMSystem();
        _fsm.AddState(wander);
        _fsm.AddState(chase);
        _fsm.AddState(attack);

        // Special States
        _fsm.AddState(idle);
        _fsm.AddState(move);
    }

    /// <summary>
    /// Always watching player and set as Objective if one of player is coming in the view area
    /// </summary>
    private void PlayerDetection()
    {
        // Find player characters in the boundary of view
        GameObject[] list = new GameObject[4];
        int count = 0;

        GameObject yukari = EventObserver.Instance.YuzukiYukari;
        GameObject maki = EventObserver.Instance.TsurumakiMaki;
        GameObject akane = EventObserver.Instance.KotonohaAkane;
        GameObject aoi = EventObserver.Instance.KotonohaAoi;

        if(yukari != null && Vector3.Distance(yukari.transform.position, transform.position) < detectionDistance)
        {
            list[0] = yukari;
            count++;
        }
        if (maki != null && Vector3.Distance(maki.transform.position, transform.position) < detectionDistance)
        {
            list[1] = maki;
            count++;
        }
        if (akane != null && Vector3.Distance(akane.transform.position, transform.position) < detectionDistance)
        {
            list[2] = akane;
            count++;
        }
        if (aoi != null && Vector3.Distance(aoi.transform.position, transform.position) < detectionDistance)
        {
            list[3] = aoi;
            count++;
        }

        // Select the closest player
        if(count > 0)
        {
            float distance = -1;
            int index = 0;

            for(int i=0; i<4; ++i)
            {
                if (list[i] == null) continue;

                float temp = Vector3.Distance(list[i].transform.position, transform.position);

                if (distance == -1 || distance > temp)
                {
                    distance = temp;
                    index = i;
                }
            }

            // Set the closest player as VIO
            Objective = list[index];
        }
        // Set objective as null if detect nothing
        else
        {
            Objective = null;
        }
    }

    /// <summary>
    /// Check whether the Objective and VIO are dead(destroyed).
    /// Update Dead(Destroyed) one to null.
    /// </summary>
    private void ClearEliminated()
    {
        // Check Objective
        if (Objective != null)
        {
            PlayerInfo info = Objective.GetComponent<PlayerInfo>();

            // This is player
            if(info != null)
            {
                if (info.dead) Objective = null;
            }
        }

        //Check VIO
        if(VIO != null)
        {
            PlayerInfo info = VIO.GetComponent<PlayerInfo>();

            // This is player
            if (info != null)
            {
                if (info.dead) VIO = null;
            }
        }
    }

    /// <summary>
    /// When get damaged by player
    /// </summary>
    protected override void React(GameObject agent, Vector3 impulse, float damage)
    {
		// store latest damage informations
		latestImpulse = impulse;
		latestDamage = damage;

		// Decrease HP
		_info.health -= damage;

		if (_info.health <= 0)
		{
			/*
			PlayerControl control = agent.GetComponent<PlayerControl>();
			if(control != null)
			{
				if(control.MeleeHitBox.GetAttackables().Contains(this))
				{
					control.MeleeHitBox.GetAttackables().Remove(this);
				}
			}
			*/
			Die(impulse, damage);
			return;
		}

		if (VIO == null)
        {
			if(agent.GetComponent<PlayerControl>() != null)
				// She is now VIO. He can't see nothing but her.
				VIO = agent;
        }

		// Bleed
		_animator.RpcBleed();

		RpcPlayPainSound();

		RpcPlayFleshSound();

		// Knock back
		StartCoroutine(KnockBack(impulse));
	}

    private IEnumerator KnockBack(Vector3 impulse)
    {
        transform.Translate(impulse * 0.25f, Space.World);
		transform.Translate(Vector3.up * 0.1f, Space.World);
		_agent.speed = 0f;

		yield return new WaitForSeconds(0.5f);

		_agent.speed = _originalSpeed;
    }

	[ClientRpc]
	private void RpcPlayPainSound()
	{
		PlayPainSound();
	}
	private void PlayPainSound()
	{
		if (voiceAudio.isPlaying) return;

		if (pain.Length > 0)
		{
			int rand = Random.Range(0, pain.Length);
			voiceAudio.clip = pain[rand];
			voiceAudio.Play();
		}
	}

	[ClientRpc]
	private void RpcPlayFleshSound()
	{
		PlayFleshSound();
	}
	private void PlayFleshSound()
	{
		if (fleshImpact.Length > 0)
		{
			int rand = Random.Range(0, fleshImpact.Length);
			fleshAudio.clip = fleshImpact[rand];
			fleshAudio.Play();
		}
	}

	[ClientRpc]
	private void RpcPlayIdleSound()
	{
		if(idle.Length > 0)
		{
			int rand = Random.Range(0, idle.Length);
			voiceAudio.clip = idle[rand];
			voiceAudio.Play();
		}
	}

	/// <summary>
	/// Kill this zombie from server
	/// </summary>
	[Server]
	private void Die(Vector3 impulse, float damage)
	{
		if (_info.dead) return;

		Statistics.Instance.AddKill();
		Debug.Log(Statistics.Instance.Kill);
		_info.dead = true;
		NetworkServer.Destroy(this.gameObject);
	}

	private void ThrowFlesh(Vector3 impulse, float damage)
	{
		Debug.Log("ThrowFlesh");
		float k = damage / 5f;

		if (face != null)
		{
			face.transform.SetParent(null);
			face.layer = LayerMask.NameToLayer("Corpse");
			face.GetComponent<BoxCollider>().isTrigger = false;
			Rigidbody rb = face.GetComponent<Rigidbody>();
			rb.isKinematic = false;
			rb.AddForce(impulse * k, ForceMode.Impulse);

			CorpseCollector.Instance.AddCorpse(face);

			// Play die sound
			if(die.Length > 0)
			{
				AudioSource a = face.AddComponent<AudioSource>();
				int rand = Random.Range(0, die.Length);
				a.clip = die[rand];
				a.volume = 1f;
				a.spatialBlend = 1f;
				a.Play();
			}
		}

		if (torso != null)
		{
			torso.transform.SetParent(null);
			torso.layer = LayerMask.NameToLayer("Corpse");
			torso.GetComponent<BoxCollider>().isTrigger = false;
			Rigidbody rb = torso.GetComponent<Rigidbody>();
			rb.isKinematic = false;
			rb.AddForce(impulse * k, ForceMode.Impulse);

			CorpseCollector.Instance.AddCorpse(torso);
		}

		if (rightArm != null)
		{
			rightArm.transform.SetParent(null);
			rightArm.layer = LayerMask.NameToLayer("Corpse");
			rightArm.GetComponent<BoxCollider>().isTrigger = false;
			Rigidbody rb = rightArm.GetComponent<Rigidbody>();
			rb.isKinematic = false;
			rb.AddForce(impulse * k, ForceMode.Impulse);

			CorpseCollector.Instance.AddCorpse(rightArm);
		}

		if (leftArm != null)
		{
			leftArm.transform.SetParent(null);
			leftArm.layer = LayerMask.NameToLayer("Corpse");
			leftArm.GetComponent<BoxCollider>().isTrigger = false;
			Rigidbody rb = leftArm.GetComponent<Rigidbody>();
			rb.isKinematic = false;
			rb.AddForce(impulse * k, ForceMode.Impulse);

			CorpseCollector.Instance.AddCorpse(leftArm);
		}

		if (rightLeg != null)
		{
			rightLeg.transform.SetParent(null);
			rightLeg.layer = LayerMask.NameToLayer("Corpse");
			rightLeg.GetComponent<BoxCollider>().isTrigger = false;
			Rigidbody rb = rightLeg.GetComponent<Rigidbody>();
			rb.isKinematic = false;
			rb.AddForce(impulse * k, ForceMode.Impulse);

			CorpseCollector.Instance.AddCorpse(rightLeg);
		}

		if (leftLeg != null)
		{
			leftLeg.transform.SetParent(null);
			leftLeg.layer = LayerMask.NameToLayer("Corpse");
			leftLeg.GetComponent<BoxCollider>().isTrigger = false;
			Rigidbody rb = leftLeg.GetComponent<Rigidbody>();
			rb.isKinematic = false;
			rb.AddForce(impulse * k, ForceMode.Impulse);

			CorpseCollector.Instance.AddCorpse(leftLeg);
		}
	}

	/// <summary>
	/// Attack() is called when AttackState acts
	/// </summary>
	public void Attack()
	{
		// Attack if cooldown is over
		if (_attackTimer == cooldownTime)
		{
			StartCoroutine(Swing());
		}
	}

	IEnumerator Swing()
	{
		// Start counting cooldown time
		_attackTimer = 0;

		isAttacking = true;

		CmdPlayFightSound();

		// Set animation random key
		_animator.CmdSetAnimationParameterFloat("Random Key", Random.Range(0, 2));

		// Start animation
		_animator.CmdSetAnimationParameterTrigger("Attack");

		// Wait for swing time
		yield return new WaitForSeconds(swingTime);
		
		bool playerHit = false;
		bool barHit = false;

		// Decide attacked object and send message to host
		List<Barricade> barricades = HitBox.GetBarricades();
		foreach (Barricade b in barricades)
		{
			if (b.Player != null) continue;

			Vector3 impulse = b.transform.position - transform.position;
			// i hit you
			b.attackingZombie = this;
			b.GetDamaged(impulse, damage);

			barHit = true;
		}

		List<GameObject> players = HitBox.GetPlayers();
		foreach (GameObject p in players)
		{
			// give damage to player
			p.GetComponent<PlayerInfo>().GetDamaged(damage);

			playerHit = true;
		}

		CmdPlayHitBarricadeSound(barHit);
		CmdPlayHitPlayerSound(playerHit);

		isAttacking = false;
	}

	[Command]
	private void CmdPlayFightSound()
	{
		RpcPlayFightSound();
	}

	[Command]
	private void CmdPlayHitPlayerSound(bool hit)
	{
		RpcPlayHitPlayerSound(hit);
	}

	[Command]
	private void CmdPlayHitBarricadeSound(bool hit)
	{
		RpcPlayHitBarricadeSound(hit);
	}

	[ClientRpc]
	private void RpcPlayFightSound()
	{
		if (voiceAudio.isPlaying) return;

		if(fight.Length > 0)
		{
			int rand = Random.Range(0, fight.Length);
			voiceAudio.clip = fight[rand];
			voiceAudio.Play();
		}
	}

	[ClientRpc]
	private void RpcPlayHitPlayerSound(bool hit)
	{
		if (attackAudio.isPlaying) return;

		if(hit)
		{
			if(hitPlayer.Length > 0)
			{
				int rand = Random.Range(0, hitPlayer.Length);
				attackAudio.clip = hitPlayer[rand];
				attackAudio.Play();
			}
		}
	}

	[ClientRpc]
	private void RpcPlayHitBarricadeSound(bool hit)
	{
		if (attackAudio.isPlaying) return;

		if (hit)
		{
			if (poundBarricade.Length > 0)
			{
				int rand = Random.Range(0, poundBarricade.Length);
				attackAudio.clip = poundBarricade[rand];
				attackAudio.Play();
			}
		}
	}

	// ----------------- Client Callbacks --------------------

	public override void OnNetworkDestroy()
	{
		Debug.Log("OnNetworkDestroy");
		// when dies
		ThrowFlesh(latestImpulse, latestDamage);
	}
}

public class IdleState : FSMState
{
    public IdleState()
    {
        stateID = StateID.Idle;
    }

    public override void Reason(GameObject player, GameObject npc)
    {
        
    }

    public override void Act(GameObject player, GameObject npc)
    {
        
    }
}

public class WanderState : FSMState
{
    private float _timer;

    public WanderState()
    {
        stateID = StateID.Wander;
        _timer = 12.0f;
    }

    public override void DoBeforeEntering()
    {
        _timer = 12.0f;    }

    public override void Reason(GameObject player, GameObject npc)
    {
        // See something
        if(player != null)
        {
            npc.GetComponent<ZombieControl>().SetTransition(Transition.DetectPlayer);
        }
    }

    public override void Act(GameObject player, GameObject npc)
    {
        // Set Destination random regularly
        _timer += Time.deltaTime;
        if (_timer > 12.0f)
        {
            // Decide whether change destination
            _timer = 0f;
            float rand = UnityEngine.Random.Range(0.0f, 1.0f);
            if(rand > 0.2f)
            {
                // Make destination position randomly
                // Somtimes zombie doesn't move but just stare into the air
                rand = UnityEngine.Random.Range(0.0f, 1.0f);
                if(rand > 0.8f)
                {
                    npc.GetComponent<ZombieControl>().SetDestination(npc.transform.position);
                }
                else
                {
                    float x = UnityEngine.Random.Range(-1.0f, 1.0f);
                    float z = UnityEngine.Random.Range(-1.0f, 1.0f);
                    Vector3 temp = new Vector3(x, 0.0f, z) * 10.0f;
                    Vector3 dest = npc.transform.position + temp;

                    npc.GetComponent<ZombieControl>().SetDestination(dest);
                }
            }
        }
    }
}

public class MoveState : FSMState
{
    public MoveState()
    {
        stateID = StateID.Move;
    }

    public override void Reason(GameObject player, GameObject npc)
    {

    }

    public override void Act(GameObject player, GameObject npc)
    {

    }
}

public class ChaseState : FSMState
{
    private float _timer;
    private GameObject _zombie;
    private float _originalSpeed;

    public ChaseState(GameObject zombie)
    {
        stateID = StateID.Chase;
        _zombie = zombie;
    }

    public override void DoBeforeEntering()
    {
        NavMeshAgent agent = _zombie.GetComponent<NavMeshAgent>();
        if(agent != null)
        {
            _originalSpeed = agent.speed;
            agent.speed *= 2;
        }
    }

    public override void DoBeforeLeaving()
    {
        NavMeshAgent agent = _zombie.GetComponent<NavMeshAgent>();
		ZombieControl control = _zombie.GetComponent<ZombieControl>();
		if (agent != null)
		{
			agent.speed = _originalSpeed;
			if(!control.attackWhileMoving)
				agent.SetDestination(agent.transform.position);
		}
    }

    public override void Reason(GameObject player, GameObject npc)
    {
		ZombieControl control = npc.GetComponent<ZombieControl>();

        // Failed to maintain player in his sight
        if(player == null)
        {
            control.SetTransition(Transition.LostPlayer);
        }

		if(!control.HitBox.IsEmpty)
		{
			control.SetTransition(Transition.AttackPlayer);
		}
    }

    public override void Act(GameObject player, GameObject npc)
    {
		ZombieControl control = npc.GetComponent<ZombieControl>();

		if(control.attackWhileMoving)
		{
			if (player != null) UpdateDestination(player, npc);
		}
		else
		{
			// Update player position at regular intervals
			// It is expensive job to update player position at every frame
			_timer += Time.deltaTime;
			if (_timer > 1f)
			{
				_timer = 0f;
				if (player != null) UpdateDestination(player, npc);
			}
		}
    }

    void UpdateDestination(GameObject player, GameObject npc)
    {
        ZombieControl control = npc.GetComponent<ZombieControl>();
        control.SetDestination(player.transform.position);
    }
}

public class AttackState : FSMState
{
    public AttackState()
    {
        stateID = StateID.Attack;
    }

    public override void Reason(GameObject player, GameObject npc)
    {
		ZombieControl control = npc.GetComponent<ZombieControl>();

		if (control.isAttacking) return;

		if(control.HitBox.IsEmpty)
		{
			control.SetTransition(Transition.OutOfRange);
		}

		if (player == null)
			control.SetTransition(Transition.EliminateObjective);
    }

    public override void Act(GameObject player, GameObject npc)
    {
		ZombieControl control = npc.GetComponent<ZombieControl>();

		if(control.attackWhileMoving)
		{
			if (player != null) UpdateDestination(player, npc);
		}

		control.Attack();
    }

	void UpdateDestination(GameObject player, GameObject npc)
	{
		ZombieControl control = npc.GetComponent<ZombieControl>();
		control.SetDestination(player.transform.position);
	}
}