﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

/// <summary>
/// Animation control of zombie
/// </summary>
public class ZombieAnimation : NetworkBehaviour {

    public GameObject blood;

    private ZombieControl _control;
    private Animator _animator;                         // Animator
    private Transform _model;                           // Child object named "Model"
    private NavMeshAgent _agent;
    private readonly float NORMAL_SPEED = 0.5f;
    
    // Use this for initialization
    void Start () {
        _control = GetComponent<ZombieControl>();
        if (_control == null) Debug.LogError("Zombie : Zombie Control not found.");

        _model = transform.FindChild("Model");
        if (_model == null) Debug.LogError("Zombie : Model not found.");

        _animator = _model.GetComponent<Animator>();

        // Set animation speed
        _agent = GetComponent<NavMeshAgent>();
        _animator.speed = _agent.speed / NORMAL_SPEED;
    }
	
	// Update is called once per frame
	void Update () {
	    _animator.SetBool("Walk", _control.isMoving);
	}

    [ClientRpc]
    public void RpcBleed()
    {
		Bleed();
    }

	public void Bleed()
	{
		if (blood != null) Instantiate(blood, transform.position, Quaternion.identity);
	}

	[Command]
	public void CmdSetAnimationParameterTrigger(string parameter)
	{
		RpcSetAnimationParameterTrigger(parameter);
	}
	[ClientRpc]
	private void RpcSetAnimationParameterTrigger(string parameter)
	{
		_animator.SetTrigger(parameter);
	}

	[Command]
	public void CmdSetAnimationParameterFloat(string parameter, float value)
	{
		RpcSetAnimationParameterFloat(parameter, value);
	}
	[ClientRpc]
	private void RpcSetAnimationParameterFloat(string parameter, float value)
	{
		_animator.SetFloat(parameter, value);
	}

	// ---------------------------- client clbks

	public override void OnNetworkDestroy()
	{
		base.OnNetworkDestroy();

		Bleed();
	}
}
