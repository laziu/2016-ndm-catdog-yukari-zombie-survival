﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System;

public class ZombieSpawner : TriggerBehaviour {

	[Header("Spawn Zombie Type")]
	[Tooltip("You can control spawn probability by placing same zombie prefabs repeatedly.")]
	public GameObject[] zombies;

	[Space]
	public bool setPlayerVIO;

	[Space]
	[Range(1, 20)]
	public int numberOfZombie = 1;
	[Tooltip("Available if Number of Zombie is more than 1")]
	[Range(1f, 10f)]
	public float interval = 1f;

	public override void TriggerStart()
	{
		if (zombies.Length == 0)
			Stop();
		else
			StartCoroutine(SpawnZombies());

	}

	public override void TriggerUpdate()
	{
		
	}

	IEnumerator SpawnZombies()
	{
		for(int i=0; i<numberOfZombie; ++i)
		{
			if (!enabled) break;

			int rand = UnityEngine.Random.Range(0, zombies.Length);
			GameObject zombie = (GameObject)Instantiate(zombies[rand], transform.position, transform.rotation);

			if(setPlayerVIO)
			{
				GameObject player = null;
				while (player == null)
				{
					player = EventObserver.Instance.GetPlayerReference((Character)UnityEngine.Random.Range(0, 4));
				}
				zombie.GetComponent<ZombieControl>().VIO = player;
			}

			NetworkServer.Spawn(zombie);

			yield return new WaitForSeconds(interval);
		}

		Stop();
	}
}
