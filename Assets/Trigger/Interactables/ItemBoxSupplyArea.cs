﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;

public class ItemBoxSupplyArea : NetworkBehaviour {

	private List<PlayerInfo> _players;
	private ItemBox _itemBox;

	public float healSpeed;
	[Range(1, 100)]
	public int ammoSupplyRate;

	private float _ammoTimer;

	// Use this for initialization
	void Start () {
		if (ammoSupplyRate <= 0) ammoSupplyRate = 1;

		_players = new List<PlayerInfo>();
		_itemBox = transform.parent.GetComponent<ItemBox>();
		if (_itemBox == null) Debug.LogError("item Box Supply Area : Item Box not found.");
	}
	
	// Update is called once per frame
	[ServerCallback]
	void Update () {

		_ammoTimer += Time.deltaTime;

		foreach(PlayerInfo p in _players)
		{
			if (p.dead) continue;

			if(_itemBox.heal > 0f)
			{
				if(p.health < p.maxHealth)
				{
					// get heal
					float heal = healSpeed * Time.deltaTime;
					if (_itemBox.heal < heal)
					{
						heal = _itemBox.heal;
					}

					// heal
					if (p.health + heal > p.maxHealth)
						p.health = p.maxHealth;
					else
						p.health += heal;

					_itemBox.heal -= heal;
				}
			}

			if (p.currentWeapon == null) continue;
			if (p.currentWeapon.GetComponent<Weapon>().dontUseAmmo) continue;
			if (_ammoTimer < 1f) continue;

			Debug.Log("Supply");

			if(_itemBox.ammo > 0)
			{
				// get ammo
				int ammo = 0;
				if(_itemBox.ammo < ammoSupplyRate)
				{
					ammo = _itemBox.ammo;
				}
				else
				{
					ammo = ammoSupplyRate;
				}

				Weapon w = p.currentWeapon.GetComponent<Weapon>();

				if(w.ammo + ammo > w.maxAmmo)
				{
					_itemBox.ammo -= w.maxAmmo - w.ammo;
					w.ammo = w.maxAmmo;
				}
				else
				{
					_itemBox.ammo -= ammo;
					w.ammo += ammo;
				}
			}
		}

		if (_ammoTimer >= 1f) _ammoTimer = 0f;
	}

	void OnTriggerEnter(Collider c)
	{
		PlayerInfo info = c.GetComponent<PlayerInfo>();

		if(info != null)
		{
			_players.Add(info);
		}
	}

	void OnTriggerExit(Collider c)
	{
		PlayerInfo info = c.GetComponent<PlayerInfo>();

		if (info != null)
		{
			if(_players.Contains(info))
			{
				_players.Remove(info);
			}
		}
	}
}
