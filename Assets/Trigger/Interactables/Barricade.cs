﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System;

public enum BarricadeID
{
	NullID,
	WoodBox,
	Barrel1,
	Barrel2,
	Barrel3
}

public class Barricade : Interactable {

	[SyncVar]
	public bool planted;
	[SyncVar]
	public float health;

	public BarricadeID id;

	[Header("When be destroyed")]
	public GameObject debris;
	public AudioClip[] breakSound;

	// a zombie that currently attack this barricade
	[HideInInspector]
	public ZombieControl attackingZombie;

    public override void TriggerStart()
    {
		PlayerControl control = Player.GetComponent<PlayerControl>();
		PlayerAnimation anim = Player.GetComponent<PlayerAnimation>();
		PlayerInfo info = Player.GetComponent<PlayerInfo>();

		// Disable attack
		control.carrySomething = true;

		// Set player animation parameter
		anim.SetAnimationParameter("Carry", true);

		// Disable player interaction
		control.InteractionBox.Enabled = false;

		// hide weapon
		if(info.currentWeapon != null)
			info.currentWeapon.SetActive(false);
		
		info.currentBarricade = this.gameObject;

		planted = false;

		// Set this child of the player
		Transform slot = Player.transform.Find("Character/Body/Arms/Right Arm/Stuff Slot");
        if (slot != null)
        {
            transform.SetParent(slot);
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;
            Rigidbody rb = GetComponent<Rigidbody>();
            if (rb != null)
            {
                rb.isKinematic = true;
            }

			// Turn off outline
			// everyone should turn off outline
			EnableOutline(false);
        }

		if(isServer)
		{
			RpcGet();
		}
    }

    public override void TriggerUpdate()
    {
		// player's interaction box will enable outline as posible as it can
		// but one that some player is carrying should not to draw outline
		// and that TriggerUpdate is acting means some player is now carrying this
		RpcEnableOutline(false);
    }

	public void GetDamaged(Vector3 impulse, float damage)
	{
		if (!isServer) return;

		health -= damage;

		if (health <= 0)
		{
			// get destroyed
			planted = false;
			NetworkServer.Destroy(this.gameObject);
		}
	}

	public void Drop()
	{
		PlayerControl control = Player.GetComponent<PlayerControl>();
		PlayerAnimation anim = Player.GetComponent<PlayerAnimation>();
		PlayerInfo info = Player.GetComponent<PlayerInfo>();

		// Turn on outline
		// EnableOutline(true);

		// Enable player interaction
		control.InteractionBox.Enabled = true;

		// Enable attack
		control.carrySomething = false;

		// activate weapon
		if(info.currentWeapon != null)
			info.currentWeapon.SetActive(true);

		// Separate this from the player
		transform.SetParent(null);
		Rigidbody rb = GetComponent<Rigidbody>();
		if (rb != null)
		{
			rb.isKinematic = false;
		}

		// Set player animation parameter
		anim.SetAnimationParameter("Carry", false);

		info.currentBarricade = null;
		Player = null;

		// Stop trigger
		Stop();

		if(isServer)
		{
			RpcDrop();
		}
	}

	public void Throw()
	{
		PlayerControl control = Player.GetComponent<PlayerControl>();
		PlayerAnimation anim = Player.GetComponent<PlayerAnimation>();
		PlayerInfo info = Player.GetComponent<PlayerInfo>();

		// Turn on outline
		// EnableOutline(true);

		// Enable player interaction
		control.InteractionBox.Enabled = true;

		// Enable attack
		control.carrySomething = false;

		// activate weapon
		if(info.currentWeapon != null)
			info.currentWeapon.SetActive(true);

		// Separate this from the player
		transform.SetParent(null);
		Rigidbody rb = GetComponent<Rigidbody>();
		if (rb != null)
		{
			rb.isKinematic = false;
		}

		// Set player animation parameter
		anim.SetAnimationParameter("Carry", false);

		// Give force direction of player focus
		Vector3 dir = Player.transform.forward;
		rb.AddForce(dir * 5f, ForceMode.Impulse);

		info.currentBarricade = null;
		Player = null;

		// Stop trigger
		Stop();

		if (isServer)
		{
			RpcThrow();
		}
	}

	[ClientRpc]
	private void RpcGet()
	{
		if (isServer) return;

		PlayerControl control = Player.GetComponent<PlayerControl>();
		PlayerInfo info = Player.GetComponent<PlayerInfo>();

		// Disable player interaction
		control.InteractionBox.Enabled = false;

		// hide weapon
		if(info.currentWeapon != null)
			info.currentWeapon.SetActive(false);

		// Set this child of the player
		Transform slot = Player.transform.Find("Character/Body/Arms/Right Arm/Stuff Slot");
		if (slot != null)
		{
			transform.SetParent(slot);
			transform.localPosition = Vector3.zero;
			transform.localRotation = Quaternion.identity;
			Rigidbody rb = GetComponent<Rigidbody>();
			if (rb != null)
			{
				rb.isKinematic = true;
			}

			// Turn off outline
			// everyone should turn off outline
			EnableOutline(false);
		}
	}

	[ClientRpc]
	private void RpcDrop()
	{
		if (isServer) return;

		Player.GetComponent<PlayerControl>().DropBarricade();
	}

	[ClientRpc]
	private void RpcThrow()
	{
		if (isServer) return;

		Player.GetComponent<PlayerControl>().ThrowBarricade();
	}

	// ----------------- Client callbacks

	public override void OnNetworkDestroy()
	{
		base.OnNetworkDestroy();
		/*
		// remove itself from zombie's hitbox
		if(attackingZombie != null)
		{
			if (attackingZombie.HitBox.GetBarricades().Contains(this))
				attackingZombie.HitBox.GetBarricades().Remove(this);
		}
		*/

		// instantiate particle
		// and replace particle's texture with its texture
		if(debris != null)
		{
			GameObject temp = (GameObject)Instantiate(debris, transform.position, transform.rotation);
			ParticleSystemRenderer renderer = temp.GetComponent<ParticleSystemRenderer>();
			renderer.material.SetTexture("_MainTex", GetComponent<Renderer>().material.GetTexture("_MainTex"));

			if(breakSound.Length > 0)
			{
				AudioSource audio = temp.GetComponent<AudioSource>();
				if (audio != null)
				{
					int rand = UnityEngine.Random.Range(0, breakSound.Length);
					audio.clip = breakSound[rand];
					audio.loop = false;
					audio.Play();
				}
			}
		}
	}
}
