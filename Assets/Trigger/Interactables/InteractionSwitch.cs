﻿using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// Interact to change switch status
/// </summary>
public class InteractionSwitch : Interactable {

    [SerializeField]
    private int _index;
    [SerializeField]
    private bool _state;

    public int Index { get { return _index; } set { _index = value; } }
    public bool State { get { return _state; } set { _state = value; } }

    public override void TriggerStart()
    {
        EventObserver.Instance.SwitchManager.SetSwitch(Index, State);

        if(IsDisposable)
        {
            // Turn off outline
            gameObject.layer = LayerMask.NameToLayer("Default");
        }
    }

    public override void TriggerUpdate()
    {
        
    }
}
