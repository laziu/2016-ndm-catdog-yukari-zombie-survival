﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System;

/// <summary>
/// Player can 'Poke' Interactable trigger by using 'Interaction Key'
/// </summary>
public abstract class Interactable : TriggerBehaviour {

	// The latest player who is trying to interact with it
	public GameObject Player { get; set; }

    public override abstract void TriggerStart();

    public override abstract void TriggerUpdate();

    [ServerCallback]
    void Start () {
        // Turn on outline
        //EnableOutline(true);
	}
	
	[ServerCallback]
	void Update () {
	    if(_enabled)
		{
			TriggerUpdate();
		}
	}

	public override void Stop()
	{
		base.Stop();
		Player = null;
	}
	/// <summary>
	/// Turn on/off outline
	/// </summary>
	public void EnableOutline(bool value)
    {
        string layerName = null;
        if (value) layerName = "Interactable";
        else layerName = "Default";

        SetLayerRecursively(this.gameObject, layerName);
    }

    private void SetLayerRecursively(GameObject obj, string layerName)
    {
        if (obj == null) return;

        obj.layer = LayerMask.NameToLayer(layerName);

        foreach(Transform child in obj.transform)
        {
            if (child == null) continue;

            SetLayerRecursively(child.gameObject, layerName);
        }
    }

	[ClientRpc]
	public void RpcEnableOutline(bool value)
	{
		EnableOutline(value);
	}
}
