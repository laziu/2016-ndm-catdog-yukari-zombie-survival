﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

[RequireComponent(typeof(Collider))]
public class BarricadeSlot : NetworkBehaviour {

	public BarricadeID barricade;

	// latest barricade among that of currently in the collider
	private Barricade currentBarricade;

	// is a barricade planted here?
	private bool _full;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		_full = false;

		if (currentBarricade == null)
		{
			gameObject.layer = LayerMask.NameToLayer("Barricade Slot");
		}
		else
		{
			if (currentBarricade.planted)
			{
				gameObject.layer = LayerMask.NameToLayer("Default");
				_full = true;
			}
			else
			{
				gameObject.layer = LayerMask.NameToLayer("Barricade Slot");
			}
		}

		if (!isServer) return;

		if (_full) return;

		if(currentBarricade != null && currentBarricade.Player == null)
		{
			Plant(currentBarricade);
		}
	}

	[ServerCallback]
	void OnTriggerEnter(Collider c)
	{
		Barricade b = c.GetComponent<Barricade>();

		if (b == null) return;

		if (b.id != barricade) return;

		if (_full) return;

		currentBarricade = b;
		/*
		if(b.Player == null)
		{
			Plant(b);
		}
		else
		{
			// make player drop the barricade
			//b.Drop();
			//Plant(b);
		}
		*/
	}

	[ServerCallback]
	void OnTriggerExit(Collider c)
	{
		Barricade b = c.GetComponent<Barricade>();

		if (b == null) return;

		if (b == currentBarricade)
			currentBarricade = null;
	}

	private void Plant(Barricade b)
	{
		// to avoid being distorted because of collision
		Rigidbody rb = currentBarricade.GetComponent<Rigidbody>();
		if (rb != null) rb.isKinematic = true;

		// will be broadcast to clients
		b.transform.position = transform.position;
		b.transform.rotation = transform.rotation;
		b.planted = true;

		// need to be done locally by each clients
		RpcSetRigidbodyKinematic(b.netId);
	}

	[ClientRpc]
	private void RpcSetRigidbodyKinematic(NetworkInstanceId currentBarricadeId)
	{
		Debug.Log("RpcSetRigidbodyKinematic");

		if (isServer) return;

		GameObject cb = ClientScene.FindLocalObject(currentBarricadeId);
		if(cb != null)
		{
			Debug.Log("Current Barricade well found");
			Rigidbody rb = cb.GetComponent<Rigidbody>();
			if (rb != null)
			{
				Debug.Log("Rigidbody well found");
				rb.isKinematic = true;
				// will be broadcast to clients
				cb.transform.position = transform.position;
				cb.transform.rotation = transform.rotation;
			}
		}
	}
}
