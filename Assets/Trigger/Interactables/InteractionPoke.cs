﻿using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// Interact to poke triggers
/// </summary>
public class InteractionPoke : Interactable {

    [SerializeField]
    private TriggerBehaviour _target;

    public TriggerBehaviour Target { get { return _target; } set { _target = value; } }

    public override void TriggerStart()
    {
        if(Target != null) Target.Poke();

        if (IsDisposable)
        {
            // Turn off outline
            gameObject.layer = LayerMask.NameToLayer("Default");
        }
    }

    public override void TriggerUpdate()
    {
        
    }
}
