﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System;

public class ItemBox : Interactable {

	public float maxHeal;
	public int maxAmmo;

	[SyncVar(hook = "OnHeal")]
	public float heal;
	[SyncVar(hook = "OnAmmo")]
	public int ammo;

	void Start()
	{
		HUD.Instance.itemBox.Target = this;
		HUD.Instance.itemBox.Init();
	}

	public override void TriggerStart()
	{
		PlayerControl control = Player.GetComponent<PlayerControl>();
		PlayerAnimation anim = Player.GetComponent<PlayerAnimation>();
		PlayerInfo info = Player.GetComponent<PlayerInfo>();

		// Disable attack
		control.carrySomething = true;

		// Disable player interaction
		control.InteractionBox.Enabled = false;

		// hide weapon
		if(info.currentWeapon != null)
			info.currentWeapon.SetActive(false);

		info.currentItemBox = this.gameObject;

		// Set this child of the player
		Transform slot = Player.transform.Find("Character/Body/Arms/Right Arm/Stuff Slot");
		if (slot != null)
		{
			transform.SetParent(slot);
			transform.localPosition = Vector3.zero;
			transform.localRotation = Quaternion.identity;
			Rigidbody rb = GetComponent<Rigidbody>();
			if (rb != null)
			{
				rb.isKinematic = true;
			}

			// Turn off outline
			// everyone should turn off outline
			EnableOutline(false);
		}

		// Set player animation parameter
		anim.SetAnimationParameter("Carry", true);

		if(isServer)
		{
			RpcGet();
		}
	}

	public override void TriggerUpdate()
	{
		// player's interaction box will enable outline as posible as it can
		// but one that some player is carrying should not to draw outline
		// and that TriggerUpdate is acting means some player is now carrying this
		RpcEnableOutline(false);
	}

	[ClientRpc]
	private void RpcGet()
	{
		if (isServer) return;

		PlayerControl control = Player.GetComponent<PlayerControl>();
		PlayerInfo info = Player.GetComponent<PlayerInfo>();

		// Disable player interaction
		control.InteractionBox.Enabled = false;

		// hide weapon
		if(info.currentWeapon != null)
			info.currentWeapon.SetActive(false);

		info.currentItemBox = this.gameObject;

		// Set this child of the player
		Transform slot = Player.transform.Find("Character/Body/Arms/Right Arm/Stuff Slot");
		if (slot != null)
		{
			transform.SetParent(slot);
			transform.localPosition = Vector3.zero;
			transform.localRotation = Quaternion.identity;
			Rigidbody rb = GetComponent<Rigidbody>();
			if (rb != null)
			{
				rb.isKinematic = true;
			}

			// Turn off outline
			// everyone should turn off outline
			EnableOutline(false);
		}
	}

	public void Drop()
	{
		PlayerControl control = Player.GetComponent<PlayerControl>();
		PlayerAnimation anim = Player.GetComponent<PlayerAnimation>();
		PlayerInfo info = Player.GetComponent<PlayerInfo>();

		// Turn on outline
		// EnableOutline(true);

		// Enable player interaction
		control.InteractionBox.Enabled = true;

		// Enable attack
		control.carrySomething = false;

		// activate weapon
		if(info.currentWeapon != null)
			info.currentWeapon.SetActive(true);

		info.currentItemBox = null;

		// Separate this from the player
		transform.SetParent(null);
		Rigidbody rb = GetComponent<Rigidbody>();
		if (rb != null)
		{
			rb.isKinematic = false;
		}

		// Set player animation parameter
		anim.SetAnimationParameter("Carry", false);

		// Stop trigger
		Stop();

		if(isServer)
		{
			RpcDrop();
		}
	}

	public void Throw()
	{
		PlayerControl control = Player.GetComponent<PlayerControl>();
		PlayerAnimation anim = Player.GetComponent<PlayerAnimation>();
		PlayerInfo info = Player.GetComponent<PlayerInfo>();

		// Turn on outline
		// EnableOutline(true);

		// Enable player interaction
		control.InteractionBox.Enabled = true;

		// Enable attack
		control.carrySomething = false;

		// activate weapon
		if(info.currentWeapon != null)
			info.currentWeapon.SetActive(true);

		info.currentItemBox = null;

		// Separate this from the player
		transform.SetParent(null);
		Rigidbody rb = GetComponent<Rigidbody>();
		if (rb != null)
		{
			rb.isKinematic = false;
		}

		info.currentItemBox = null;

		// Set player animation parameter
		anim.SetAnimationParameter("Carry", false);

		// Give force direction of player focus
		Vector3 dir = Player.transform.forward;
		rb.AddForce(dir * 5f, ForceMode.Impulse);

		// Stop trigger
		Stop();

		if (isServer)
		{
			RpcThrow();
		}
	}

	[ClientRpc]
	private void RpcDrop()
	{
		if (isServer) return;

		Player.GetComponent<PlayerControl>().DropItemBox();
	}

	[ClientRpc]
	private void RpcThrow()
	{
		if (isServer) return;

		Player.GetComponent<PlayerControl>().ThrowItemBox();
	}

	// ------------------------ SyncVar hooks

	public void OnHeal(float newHeal)
	{
		HUD.Instance.itemBox.SetHP(newHeal);
	}

	public void OnAmmo(int newAmmo)
	{
		HUD.Instance.itemBox.SetAmmo(newAmmo);
	}
}
