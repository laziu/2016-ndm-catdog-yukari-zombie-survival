﻿using UnityEngine;
using System.Collections;
using System;

public class DebugLogTrigger : TriggerBehaviour
{
    [SerializeField]
    private string _text;

    public string Text { get { return _text; } set { _text = value; } }

    public override void TriggerStart()
    {
        Debug.Log(Text);
    }

    public override void TriggerUpdate()
    {
        
    }
}
