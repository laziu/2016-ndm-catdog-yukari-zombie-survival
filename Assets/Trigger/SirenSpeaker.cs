﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System;

[RequireComponent(typeof(AudioSource))]
public class SirenSpeaker : TriggerBehaviour {

	public AudioSource audio;
	public float playTime = 100f;
	public float volume = 0.2f;

	// For spaghetti
	public Location endPoint;

	public override void TriggerStart()
	{
		RpcPlay();
		
		StartCoroutine(CheckTime());
	}

	public override void TriggerUpdate()
	{
		if(endPoint != null)
		{
			if(endPoint.IsCollidingWith("Player"))
			{
				Stop();
			}
		}
	}

	public override void Stop()
	{
		base.Stop();

		RpcStopSiren();
	}

	[ClientRpc]
	private void RpcPlay()
	{
		audio.volume = volume;
		audio.Play();
	}

	[ClientRpc]
	private void RpcStopSiren()
	{
		StartCoroutine(StopSiren());
	}

	IEnumerator StopSiren()
	{
		while(audio.volume > 0f)
		{
			audio.volume -= Time.deltaTime * 0.2f;
			if (audio.volume < 0f) audio.volume = 0f;

			yield return new WaitForEndOfFrame();
		}
	}

	IEnumerator CheckTime()
	{
		yield return new WaitForSeconds(playTime);

		Stop();
	}
}
