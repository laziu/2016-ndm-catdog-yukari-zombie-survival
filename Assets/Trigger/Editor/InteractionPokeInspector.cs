﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(InteractionPoke))]
public class InteractionPokeInspector : Editor {

    public override void OnInspectorGUI()
    {
        InteractionPoke myTarget = (InteractionPoke)target;

        GUILayout.BeginVertical();

        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("일회용 ", GUILayout.Width(50));
        myTarget.IsDisposable = EditorGUILayout.Toggle(myTarget.IsDisposable);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Target ", GUILayout.Width(50));
        EditorGUILayout.ObjectField(myTarget.Target, typeof(TriggerBehaviour), true, GUILayout.Width(200));
        GUILayout.EndHorizontal();

        GUILayout.EndVertical();
    }
}
