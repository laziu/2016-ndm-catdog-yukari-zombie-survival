﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(DebugLogTrigger))]
[CanEditMultipleObjects]
public class DebugLogTriggerInspector : TriggerBehaviourInspector {

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        DebugLogTrigger myTarget = (DebugLogTrigger)target;

        GUILayout.BeginVertical();
        myTarget.Text = EditorGUILayout.TextArea(myTarget.Text, GUILayout.Height(30));
        GUILayout.EndVertical();
    }
}
