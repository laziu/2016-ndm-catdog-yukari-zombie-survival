﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

/// <summary>
/// The conditions to make action are two.
/// Switch state and Poke message from other objects.
/// </summary>
public abstract class TriggerBehaviour : NetworkBehaviour {

    [SerializeField]
    private bool _disposable;
    [SerializeField]
    private int _switch = -1;
    [SerializeField]
    private bool _switchState = true;

    protected bool _enabled = false;
    private bool _dead = false;

    /// <summary>
    /// Is it disposable trigger?
    /// </summary>
    public bool IsDisposable { get { return _disposable; } set { _disposable = value; } }

    /// <summary>
    /// Switch that the trigger behavior is watching.
    /// </summary>
    public int ConnectedSwitch { get { return _switch; } set { _switch = value; } }

    /// <summary>
    /// Trigger behavior is enabled when the state of connected switch is equal to the ExpectedSwitchState.
    /// </summary>
    public bool ExpectedSwitchState { get { return _switchState; } set { _switchState = value; } }

	[ServerCallback]
	void Start () {
	
	}
	
	[ServerCallback]
	void Update () {
        // Always checking switch state
        CheckSwitch();
		
        if(_enabled)
        {
            TriggerUpdate();
        }
	}

    /// <summary>
    /// Dialogue and any other objects can poke to enalbe this.
    /// </summary>
    public void Poke()
    {
        if (_dead) return;

        if(_enabled == false) TriggerStart();
        _enabled = true;
        if(IsDisposable) _dead = true;
    }

    /// <summary>
    /// Stop the trigger behavior.
    /// </summary>
	[ServerCallback]
    public virtual void Stop()
    {
        _enabled = false;
    }

    /// <summary>
    /// TriggerStarte is called on the frame when a trigger behavior is poked or state of connected switch satisfies the condition.
    /// </summary>
	[ServerCallback]
    public abstract void TriggerStart();

    /// <summary>
    /// TriggerUpdate is called every frame if the trigger behavior is enabled.
    /// </summary>
	[ServerCallback]
    public abstract void TriggerUpdate();

	[ServerCallback]
    private void CheckSwitch()
    {
        if(ConnectedSwitch > -1)
        {
            if (EventObserver.Instance.SwitchManager.CheckSwitchOn(_switch) == ExpectedSwitchState)
                Poke();
        }
    }
}
