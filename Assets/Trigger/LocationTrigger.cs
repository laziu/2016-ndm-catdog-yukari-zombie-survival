﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System;

/// <summary>
/// Poke target when player touches it
/// </summary>
[RequireComponent(typeof(Collider))]
public class LocationTrigger : NetworkBehaviour {

	public TriggerBehaviour[] targets;

	public bool disposable;
	
	private bool _dead;

	[ServerCallback]
	void OnTriggerEnter(Collider c)
	{
		if (_dead) return;

		if(c.gameObject.layer == LayerMask.NameToLayer("Player"))
		{
			if(targets.Length > 0)
			{
				foreach (TriggerBehaviour t in targets)
				{
					t.Poke();
				}

				if (disposable)
					_dead = true;
			}
		}
	}
}
