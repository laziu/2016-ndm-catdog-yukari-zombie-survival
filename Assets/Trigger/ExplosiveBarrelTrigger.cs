﻿using UnityEngine;
using System.Collections;
using System;

[RequireComponent(typeof(ExplosiveBarrel))]
public class ExplosiveBarrelTrigger : TriggerBehaviour {

	public override void TriggerStart()
	{
		ExplosiveBarrel barrel = GetComponent<ExplosiveBarrel>();
		barrel.LitFire();
		barrel.health = 30f;
		Stop();
	}

	public override void TriggerUpdate()
	{
		
	}
}
