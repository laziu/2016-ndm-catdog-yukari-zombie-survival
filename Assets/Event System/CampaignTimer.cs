﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class CampaignTimer : NetworkBehaviour {

	#region Singleton
	private static CampaignTimer _instance = null;
	public static CampaignTimer Instance
	{
		get
		{
			if (_instance == null) Debug.LogError("CampaignTimer has not awaken.");
			return _instance;
		}
	}

	void Awake()
	{
		if (_instance != null)
		{
			Destroy(this.gameObject);
			return;
		}
		else
		{
			_instance = this;
		}
	}
	#endregion

	[SyncVar(hook = "OnTime")]
	public int rawTime;

	// Use this for initialization
	[ServerCallback]
	void Start () {
		StartCoroutine(TimeFlow());
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void AddTime(int seconds)
	{
		// let hook method be called once
		if (rawTime + seconds < 0)
		{
			rawTime = 0;
			HUD.Instance.timer.enabled = false;
		}
		else
		{
			rawTime += seconds;
			HUD.Instance.timer.enabled = true;
			if (seconds > 0)
				StartCoroutine(TimeUpdated());
		}
	}

	public int GetMinutes()
	{
		return rawTime / 60;
	}

	public int GetSeconds()
	{
		int minute = GetMinutes();

		return rawTime - (minute * 60);
	}
	
	// ------------------------ SyncVar Hooks

	private void OnTime(int newTime)
	{
		rawTime = newTime;

		HUD.Instance.timer.text = GetMinutes().ToString("00") + " : " + GetSeconds().ToString("00");
	}

	IEnumerator TimeFlow()
	{
		while(true)
		{
			yield return new WaitForSeconds(1f);

			AddTime(-1);
		}
	}

	IEnumerator TimeUpdated()
	{
		Color init = HUD.Instance.timer.color;
		
		HUD.Instance.timer.color = Color.red;
		yield return new WaitForSeconds(0.5f);
		HUD.Instance.timer.color = init;
		yield return new WaitForSeconds(0.5f);
		HUD.Instance.timer.color = Color.red;
		yield return new WaitForSeconds(0.5f);
		HUD.Instance.timer.color = init;
		yield return new WaitForSeconds(0.5f);
		HUD.Instance.timer.color = Color.red;
		yield return new WaitForSeconds(0.5f);
		HUD.Instance.timer.color = init;
		yield return new WaitForSeconds(0.5f);
		HUD.Instance.timer.color = Color.red;
		yield return new WaitForSeconds(0.5f);
		HUD.Instance.timer.color = init;
	}
}
