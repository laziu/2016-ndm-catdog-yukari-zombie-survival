﻿using UnityEngine;
using EventCondition;

public class EventObserver : MonoBehaviour {

	#region Singleton
	private static EventObserver _instance = null;
	public static EventObserver Instance
	{
		get
		{
			if (_instance == null) Debug.LogError("EventObserver has not awaken.");
			return _instance;
		}
	}
	#endregion

	public LocationObserver LocationObserver { get; private set; }
	public SwitchManager SwitchManager { get; private set; }

    // Event Observer knows player references
    public GameObject YuzukiYukari { get; private set; }
    public GameObject TsurumakiMaki { get; private set; }
    public GameObject KotonohaAkane { get; private set; }
    public GameObject KotonohaAoi { get; private set; }

    public GameObject GetPlayerReference(Character character)
    {
        switch(character)
        {
            case Character.YuzukiYukari:
                return YuzukiYukari;

            case Character.TsurumakiMaki:
                return TsurumakiMaki;

            case Character.KotonohaAkane:
                return KotonohaAkane;

            case Character.KotonohaAoi:
                return KotonohaAoi;

            default:
                return null;
        }
    }

	void Awake()
	{
		if(_instance != null)
		{
			Destroy(this.gameObject);
			return;
		}
		else
		{
			_instance = this;
		}
	}

	// Use this for initialization
	void Start () {
		LocationObserver = GetComponentInChildren<LocationObserver>();
		SwitchManager = GetComponentInChildren<SwitchManager>();

		if (LocationObserver == null) Debug.LogError("Event Observer : Location Observer not found.");
		if (SwitchManager == null) Debug.LogError("Event Observer : Switch Manager not found.");
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	/// <summary>
	/// 이벤트 체크 콜. 각 EventCondition들이 Update 로직에서 호출한다.
	/// EventObserver는 EventType에 해당하는 적절한 처리를 거친 후 이벤트 발생 여부를 전달한다.
	/// </summary>
	public bool CheckEventCall(BaseEventCondition eventCondition)
	{
		bool result = false;

		switch(eventCondition.Type)
		{
			case EventType.Unconditional:
				result = true;
				break;

			case EventType.LocationTouch:
				result = LocationObserver.CheckEvent((LocationTouch)eventCondition);
				break;

			case EventType.TotalZombieKillCount:
				TotalZombieKillCount killCount = (TotalZombieKillCount)eventCondition;
				int count = killCount.Count;
				result = (Statistics.Instance.Kill == count);
				break;

			case EventType.PlayTime:
                PlayTime playTime = (PlayTime)eventCondition;
                result = Statistics.Instance.CampaignPlayTime >= (playTime.Minutes * 60f);
				break;

			case EventType.SwitchState:
				SwitchState switchState = (SwitchState)eventCondition;
				result = SwitchManager.CheckSwitchOn(switchState.Index);
				break;

			case EventType.PlayerDie:
				PlayerDie playerDie = (PlayerDie)eventCondition;
				GameObject player = GetPlayerReference(playerDie.Character);
				if(player != null)
				{
					result = GetPlayerReference(playerDie.Character).GetComponent<PlayerInfo>().dead;
				}
				break;

			case EventType.RemainTime:
				RemainTime remainTime = (RemainTime)eventCondition;
				result = CampaignTimer.Instance.rawTime <= remainTime.Seconds;
				break;
		}

		return result;
	}

    /// <summary>
    /// PlayerInfo submits its reference
    /// </summary>
    public void SubmitPlayerReference(PlayerInfo info)
    {
		Debug.Log("EventObserver : SubmitPlayerReference - " + info.character);
        if(info != null)
        {
            switch(info.character)
            {
                case Character.YuzukiYukari:
                    YuzukiYukari = info.gameObject;
                    break;

                case Character.TsurumakiMaki:
                    TsurumakiMaki = info.gameObject;
                    break;

                case Character.KotonohaAkane:
                    KotonohaAkane = info.gameObject;
                    break;

                case Character.KotonohaAoi:
                    KotonohaAoi = info.gameObject;
                    break;

                default:
                    break;
            }
        }
    }
}
