﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using EventCondition;

[CustomEditor(typeof(Failure))]
public class FailureInspector : Editor {

	private EventType _selectedEventType;

	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI();

		Failure myTarget = (Failure)target;

		// Add Condition Button
		GUILayout.BeginHorizontal();
		_selectedEventType = (EventType)EditorGUILayout.EnumPopup(_selectedEventType, GUILayout.Width(100));

		if (GUILayout.Button("Add Condition"))
		{
			CreateNewCondition(myTarget);
		}
		GUILayout.EndHorizontal();
	}

	private void CreateNewCondition(Failure myTarget)
	{

		GameObject go = new GameObject(SelectName());
		go.transform.parent = myTarget.transform;

		switch (_selectedEventType)
		{
			case EventType.LocationTouch:
				go.AddComponent<LocationTouch>();
				break;

			case EventType.TotalZombieKillCount:

				break;

			case EventType.PlayTime:
				go.AddComponent<PlayTime>();
				break;

			case EventType.SwitchState:
				go.AddComponent<SwitchState>();
				break;

			case EventType.PlayerDie:
				go.AddComponent<PlayerDie>();
				break;

			case EventType.RemainTime:
				go.AddComponent<RemainTime>();
				break;
		}
	}

	// Set name of new EventCondition
	private string SelectName()
	{
		string namePrefix = "";

		switch (_selectedEventType)
		{
			case EventType.LocationTouch:
				namePrefix = "Location Touch ";
				break;

			case EventType.TotalZombieKillCount:
				namePrefix = "Total Zombie Kill Count ";
				break;

			case EventType.PlayTime:
				namePrefix = "Play Time ";
				break;

			case EventType.SwitchState:
				namePrefix = "Check Switch ";
				break;

			case EventType.PlayerDie:
				namePrefix = "Player Die ";
				break;
				
			case EventType.RemainTime:
				namePrefix = "Remain Time ";
				break;
		}

		int count = 1;
		string name = namePrefix + count;

		BaseEventCondition[] ecs = FindObjectsOfType<BaseEventCondition>();
		for (int i = 0; i < ecs.Length; ++i)
		{
			name = namePrefix + count;
			foreach (BaseEventCondition ec in ecs)
			{
				if (ec.name.Equals(name))
				{
					count++;
					break;
				}
			}
		}

		return namePrefix + count;
	}
}
