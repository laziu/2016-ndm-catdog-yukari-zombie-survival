﻿using UnityEngine;
using System.Collections;
using EventCondition;

public class LocationObserver : MonoBehaviour {

	private LocationCollector m_collector;

	// Use this for initialization
	void Start () {
		m_collector = GetComponent<LocationCollector>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public bool CheckEvent(LocationTouch locationTouch)
	{
        LocationTouchOption option = locationTouch.Option;
        Character character = locationTouch.Character;
		Location location = locationTouch.Location;
		GameObject subject = locationTouch.Subject;

        switch(option)
        {
            case LocationTouchOption.Player:
				if(character == Character.NullCharacter)
				{
					return m_collector.GetLocation(location).IsCollidingWith("Player");
				}
				else
				{
					GameObject player = EventObserver.Instance.GetPlayerReference(character);
					if (player != null) return m_collector.GetLocation(location).IsCollidingWith(player);
					else return false;
				}

            case LocationTouchOption.Object:
                return m_collector.GetLocation(location).IsCollidingWith(subject);

			case LocationTouchOption.Zombie:
				return m_collector.GetLocation(location).IsCollidingWith("Zombie");
				

            default:
                return false;
        }
	}
}
