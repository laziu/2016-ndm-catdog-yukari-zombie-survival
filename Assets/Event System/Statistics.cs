﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

/// <summary>
/// Statistics collects all of play information
/// </summary>
public class Statistics : NetworkBehaviour {

    #region Singleton
    private static Statistics _instance = null;
    public static Statistics Instance
    {
        get
        {
            if (_instance == null) Debug.LogError("Statistics has not awaken.");
            return _instance;
        }
    }

    void Awake()
    {
        if (_instance != null)
        {
            Destroy(this.gameObject);
            return;
        }
        else
        {
            _instance = this;
        }
    }
    #endregion

    [SyncVar]
    private int _kill;                      // Total zombie kill
    [SyncVar]
    private float _startTime;               // Campaign start time (sec)
    [SyncVar]
    private float _playTime;                // Elapsed time since a campaign starts (sec)
    [SyncVar]
    private int _totalDialogue;             // Total number of dialogues
    [SyncVar]
    private int _playedDialogue;            // The number of played dialogues

    #region Getters 
    public int Kill { get { return _kill; } }
    public float CampaignStarttime { get { return _startTime; } }
    public float CampaignPlayTime { get { return _playTime; } }
    public int TotalNumberOfDialogues { get { return _totalDialogue; } }
    public int NumberOfPlayedDialogue { get { return _playedDialogue; } }
    #endregion

    [ServerCallback]
    void Start()
    {
        Init();
    }

    [ServerCallback]
    void Update()
    {
        // Update play time
        _playTime = Time.realtimeSinceStartup - _startTime;
    }

    private void Init()
    {
        _kill = 0;
        _startTime = 0;
        _playTime = 0;
        _totalDialogue = 0;
        _playedDialogue = 0;
    }

    /// <summary>
    /// Write start time when the campaign starts
    /// </summary
    [ServerCallback]
    public void WriteStartTime()
    {
        _startTime = Time.realtimeSinceStartup;
    }

    /// <summary>
    /// Zombies say "I'm dead!" when they die
    /// </summary>
    [ServerCallback]
    public void AddKill()
    {
        _kill++;
    }

    /// <summary>
    /// Each dialogue will report themselves when a scene starts then Statistics can count the number of dialogues
    /// </summary>
    [ServerCallback]
    public void AddTotalDialogue(int val)
    {
        _totalDialogue = val;
    }

    /// <summary>
    /// Each dialogue report that they are played when they starts to be played
    /// </summary>
    [ServerCallback]
    public void AddPlayedDialogue()
    {
        _playedDialogue++;
    }
}