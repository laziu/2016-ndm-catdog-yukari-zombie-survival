﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using EventCondition;

public class Victory : NetworkBehaviour {

	private bool _dirty = false;

	private NetworkLobbyManager _lobbyManager;
	private BaseEventCondition[] _children;
	
	public Fading fadingNode;
	public GameObject outlineSystem;

	private bool _dead;

	// Use this for initialization
	[ServerCallback]
	void Start () {
		_lobbyManager = FindObjectOfType<NetworkLobbyManager>();
		_children = GetComponentsInChildren<BaseEventCondition>();
	}
	
	// Update is called once per frame
	[ServerCallback]
	void Update () {

		if (_dead) return;

		foreach (BaseEventCondition ec in _children)
		{
			if (EventObserver.Instance.CheckEventCall(ec))
			{
				ec.Satisfies = true;
				_dirty = true;
			}
			else
			{
				ec.Satisfies = false;
			}
		}

		if (_dirty)
		{
			bool allSatisfies = true;

			// Does every EventCondition satisfies?
			for (int i = 0; i < transform.childCount; ++i)
			{
				BaseEventCondition ec = transform.GetChild(i).GetComponent<BaseEventCondition>();
				if (ec == null)
				{
					Debug.LogError(transform.parent.name + " : Victory condition has some wrong child. That should be removed.");
					continue;
				}

				if (ec.Satisfies) continue;
				else
				{
					allSatisfies = false;
					break;
				}
			}

			// If all of conditions be satisfied than victory
			if (allSatisfies)
			{
				_dead = true;
				StartCoroutine(GameOver());
			}

			_dirty = false;
		}
	}

	IEnumerator GameOver()
	{
		if (outlineSystem != null) outlineSystem.SetActive(false);

		yield return new WaitForSeconds(1.2f);
		
		// Fade out
		fadingNode.Active = true;

		while (fadingNode.Active)
			yield return null;

		QuarterView.Instance.gameObject.SetActive(false);

		// Load Lobby Scene
		_lobbyManager.ServerChangeScene(_lobbyManager.lobbyScene);
	}
}
