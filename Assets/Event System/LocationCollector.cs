﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class LocationCollector : MonoBehaviour {

	private Location[] m_locations;

	// Use this for initialization
	void Start () {
		// Find Location Objects in run time
		m_locations = FindObjectsOfType<Location>();
	}
	
#if UNITY_EDITOR
	// Update is called once per frame
	void Update () {
		// Do following tasks only in editor mode not run time
		if (Application.isEditor && !Application.isPlaying)
		{
			// Find Location Objects in the scene
			m_locations = FindObjectsOfType<Location>();
		}
	}
#endif

	public Location GetLocation(Location location)
	{
		foreach(Location l in m_locations)
		{
			if (l == location) return l;
		}

		return null;
	}

	public Location GetLocation(string name)
	{
		foreach (Location l in m_locations)
		{
			if (l.name.Equals(name)) return l;
		}

		return null;
	}
}
