﻿using UnityEngine;
using System.Collections;

public class SwitchManager : MonoBehaviour {

	public static int SWITCH_SIZE = 20;

	private bool[] _switch;

	// Use this for initialization
	void Start () {
		_switch = new bool[SWITCH_SIZE];
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SetSwitch(int index, bool value)
	{
		if(index > -1 && index < SWITCH_SIZE)
			_switch[index] = value;
	}

	public bool CheckSwitchOn(int index)
	{
		if (index > -1 && index < SWITCH_SIZE)
			return _switch[index];
		else
			return false;
    }
}
