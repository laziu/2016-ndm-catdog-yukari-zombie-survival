﻿using UnityEngine;
using System.Collections.Generic;

public class Location : MonoBehaviour {

	private List<GameObject> m_subjects = new List<GameObject>();

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	// 충돌할 경우 충돌한 오브젝트의 레퍼런스를 갖고 있는다
	void OnTriggerEnter(Collider other)
	{
		m_subjects.Add(other.gameObject);
	}

	// 더 이상 충돌하지 않을 경우 갖고 있던 오브젝트의 레퍼런스를 버린다
	void OnTriggerExit(Collider other)
	{
		if(m_subjects.Contains(other.gameObject))
		{
			m_subjects.Remove(other.gameObject);
		}
	}

	// 해당 오브젝트가 충돌중인가?
	public bool IsCollidingWith(GameObject subject)
	{
		return m_subjects.Contains(subject);
	}

	public bool IsCollidingWith(string layer)
	{
		foreach(GameObject go in m_subjects)
		{
			if (go.layer == LayerMask.NameToLayer(layer))
				return true;
		}

		return false;
	}
}
