﻿using UnityEngine;
using System.Collections;

public class Explosion : MonoBehaviour {

	public Light light;
	public float lifeTime;

	[Header("SE")]
	public AudioClip[] explode;

	private float _lightTimer;
	private float _lifeTimer;

	// Use this for initialization
	void Start () {
		light.enabled = true;
		_lightTimer = 0f;
		_lifeTimer = 0f;

		if(explode.Length > 0)
		{
			AudioSource audio = GetComponent<AudioSource>();
			if(audio != null)
			{
				int rand = Random.Range(0, explode.Length);
				audio.clip = explode[rand];
				audio.Play();
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(_lightTimer < 0.05f)
		{
			_lightTimer += Time.deltaTime;
		}
		else
		{
			_lightTimer = 0.05f;
			light.enabled = false;
		}

		if (_lifeTimer < lifeTime)
		{
			_lifeTimer += Time.deltaTime;
		}
		else
		{
			Destroy(this.gameObject);
		}
	}
}
