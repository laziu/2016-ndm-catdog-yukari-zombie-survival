﻿using UnityEngine;
using System.Collections;

public class SoulParticle : MonoBehaviour {

	public float magnitude;
	public float frequancy;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 k = new Vector3(0, Mathf.Sin(Time.time * frequancy) * magnitude, 0);
		transform.Translate(k, Space.Self);
	}
}
