﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Light))]
public class SineCurveLight : MonoBehaviour {

	Light light;

	float initRange;

	// Use this for initialization
	void Start () {
		light = GetComponent<Light>();
		initRange = light.range;
	}
	
	// Update is called once per frame
	void Update () {
		light.range = initRange + Mathf.Sin(Time.time *10f) * 0.2f + 5f;
		transform.up = Vector3.up;
	}
}
