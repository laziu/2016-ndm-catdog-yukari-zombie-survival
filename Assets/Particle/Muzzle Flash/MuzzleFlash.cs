﻿using UnityEngine;
using System.Collections;

public class MuzzleFlash : MonoBehaviour {

	ParticleSystem particle;
	Light light;

	// Use this for initialization
	void Start () {
		particle = GetComponent<ParticleSystem>();
		light = GetComponent<Light>();

		light.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Emit()
	{
		particle.Emit(10);

		StartCoroutine(Spark(0.05f));
	}

	IEnumerator Spark(float time)
	{
		light.enabled = true;

		yield return new WaitForSeconds(time);

		light.enabled = false;
	}
}
