﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class YukariPanel : MonoBehaviour {

	public Image portrait;
	public Slider hp;
	public Text ammo;

	[Header("Portraits")]
	public Sprite normal;
	public Sprite danger;
	public Sprite dead;

	public PlayerInfo Target { get; set; }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Init()
	{
		SetHP(Target.health);
		if (Target.currentWeapon != null)
			SetAmmo(Target.currentWeapon.GetComponent<Weapon>().ammo);
		else
			NoWeapon();
	}

	public void ChangePortraitNormal()
	{
		if(normal != null) portrait.sprite = normal;
	}

	public void ChangePortraitDanger()
	{
		if (danger != null) portrait.sprite = danger;
	}

	public void ChangePortraitDead()
	{
		if (dead != null) portrait.sprite = dead;
	}

	public void SetHP(float value)
	{
		float k = 0f;
		if (value > 0f)
			k = value / Target.maxHealth;

		hp.value = k;

		if (k > 0.39f)
			ChangePortraitNormal();
		else if (k > 0)
			ChangePortraitDanger();
		else
			ChangePortraitDead();
	}

	public void SetAmmo(int bullet)
	{
		ammo.text = bullet.ToString();
	}

	public void NoWeapon()
	{
		ammo.text = "";
	}
}
