﻿using UnityEngine;
using System.Collections;

public class PopupPanel : MonoBehaviour {

	public Popup ctrl;
	public Popup shift;
	public Popup z;
	public Popup arrowAlt;

	[Space]
	public float fadingSpeed;
	public float showingTime;

	// Use this for initialization
	void Start () {
		HideAll();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	private void HideAll()
	{
		ctrl.SetAlpha(0f);
		shift.SetAlpha(0f);
		z.SetAlpha(0f);
		arrowAlt.SetAlpha(0f);
	}

	public void ShowCtrl()
	{
		StopCoroutine("AlphaAnimation");
		HideAll();
		StartCoroutine(AlphaAnimation(ctrl));
	}

	public void ShowShift()
	{
		StopCoroutine("AlphaAnimation");
		HideAll();
		StartCoroutine(AlphaAnimation(shift));
	}

	public void ShowZ()
	{
		StopCoroutine("AlphaAnimation");
		HideAll();
		StartCoroutine(AlphaAnimation(z));
	}

	public void ShowArrowAlt()
	{
		StopCoroutine("AlphaAnimation");
		HideAll();
		StartCoroutine(AlphaAnimation(arrowAlt));
	}

	private IEnumerator AlphaAnimation(Popup popup)
	{
		float alpha = 0f;

		while(alpha < 1f)
		{
			alpha += Time.deltaTime * fadingSpeed;
			if (alpha > 1f) alpha = 1f;
			popup.SetAlpha(alpha);

			yield return new WaitForEndOfFrame();
		}

		yield return new WaitForSeconds(showingTime);

		alpha = popup.CurrentAlpha;

		while(alpha > 0f)
		{
			alpha -= Time.deltaTime * fadingSpeed;
			if (alpha < 0f) alpha = 0f;
			popup.SetAlpha(alpha);

			yield return new WaitForEndOfFrame();
		}
	}
}
