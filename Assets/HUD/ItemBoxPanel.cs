﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ItemBoxPanel : MonoBehaviour {

	public Image portrait;
	public Slider hp;
	public Text ammo;

	public ItemBox Target { get; set; }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Init()
	{
		SetHP(Target.heal);
		SetAmmo(Target.ammo);
	}

	public void SetHP(float value)
	{
		float k = 0f;
		if(value > 0f)
			k = value / Target.maxHeal;
		hp.value = k;
	}

	public void SetAmmo(int bullet)
	{
		ammo.text = bullet.ToString();
	}
}
