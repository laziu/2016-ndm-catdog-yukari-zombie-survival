﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Popup : MonoBehaviour {

	public Image image;
	public Text text;

	public float CurrentAlpha { get { return image.color.a; } }

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SetAlpha(float alpha)
	{
		Color c1 = image.color;
		c1.a = alpha;
		image.color = c1;

		Color c2 = text.color;
		c2.a = alpha;
		text.color = c2;
	}
}
