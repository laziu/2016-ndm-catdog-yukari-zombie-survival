﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HUD : MonoBehaviour {

	#region Singleton
	private static HUD _instance = null;
	public static HUD Instance
	{
		get
		{
			if (_instance == null) Debug.LogError("HUD has not awaken.");
			return _instance;
		}
	}

	void Awake()
	{
		if (_instance != null)
		{
			Destroy(this.gameObject);
			return;
		}
		else
		{
			_instance = this;
		}
	}
	#endregion

	public YukariPanel yukari;
	public MakiPanel maki;
	public ItemBoxPanel itemBox;
	public PopupPanel popup;
	public Text timer;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
