﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections;

public class YukariBottomPanel : MonoBehaviour {

    public Image black;
    public Button[] campaignList;

	// Use this for initialization
	void Start () {
		//EnableCampaignButtons(false);
	}
	
	// Update is called once per frame
	void Update () {
        // set alpha of campaign buttons
        ClearAllAlpha();
        Image image = campaignList[YukariLobbyManager.Instance._campaignNumber].GetComponent<Image>();
        Color c = image.color;
        c.a = 0.4f;
        image.color = c;
    }

	public void EnableCampaignButtons(bool value)
	{
		foreach (Button b in campaignList)
		{
			if (b != null) b.interactable = value;
		}
	}

    #region OnClickCampagin

    private void ClearAllAlpha()
    {
        foreach(Button b in campaignList)
        {
            Image image = b.GetComponent<Image>();
            Color c = image.color;
            c.a = 0f;
            image.color = c;
        }
    }

    public void OnClickCampagin1()
    {
        // Begin fading

        // Set campaign info
        // this should be broadcasted to all of clients
        NetworkLobbyPlayer[] lobbySlots = YukariLobbyManager.Instance.lobbySlots;

        for (int i = 0; i < lobbySlots.Length; ++i)
        {
            YukariLobbyPlayer p = lobbySlots[i] as YukariLobbyPlayer;

            if (p != null)
            {
                p.RpcOnCampaignNumber(0);
            }
        }
    }

    public void OnClickCampagin2()
    {
        NetworkLobbyPlayer[] lobbySlots = YukariLobbyManager.Instance.lobbySlots;

        for (int i = 0; i < lobbySlots.Length; ++i)
        {
            YukariLobbyPlayer p = lobbySlots[i] as YukariLobbyPlayer;

            if (p != null)
            {
                p.RpcOnCampaignNumber(1);
            }
        }
    }
    #endregion
}
