﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class YukariMainPanel : MonoBehaviour {

    public Image thumbnailImage;
    public Button readyButton;
    public Button unreadyButton;
    public Image loadingIcon;

    [Space]
    [Header("Campaign Thumbnails")]
    [Tooltip("Each sprite should be in the identical index number with its campaign's")]
    public Sprite[] thumbnailSprites;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void InitIconVisability()
    {
        readyButton.gameObject.SetActive(true);
        unreadyButton.gameObject.SetActive(false);
        loadingIcon.enabled = false;
    }

    public void ChangeThumbnail(int campaignNumber)
    {
		if(thumbnailSprites.Length > 0)
		{
			Sprite sprite = thumbnailSprites[campaignNumber];
			if(sprite != null)
			{
				thumbnailImage.sprite = sprite;
				thumbnailImage.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, thumbnailImage.sprite.rect.width);
				thumbnailImage.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, thumbnailImage.sprite.rect.height);
			}
			else
			{
				Debug.LogError("Yukari Main Panel : Thumbnail not found.");
			}
		}
    }
}
