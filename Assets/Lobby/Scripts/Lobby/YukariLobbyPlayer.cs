﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using Prototype.NetworkLobby;
using UnityEngine.UI;

public class YukariLobbyPlayer : NetworkLobbyPlayer {

    // Sent by LobbyManager
    [HideInInspector]
    public Button readyButton;
    [HideInInspector]
    public Button unreadyButton;
    [HideInInspector]
    public Button[] campaignButtons;

    [HideInInspector]
    public InputField nicknameField;

    [SyncVar(hook = "OnMyNickname")]
    public string nickname = "";
    [SyncVar(hook = "OnMyCharacter")]
    public Character character = Character.NullCharacter;

    public override void OnClientEnterLobby()
    {
        base.OnClientEnterLobby();

		Debug.Log("OnClientEnterLobby");

		if (YukariLobbyManager.Instance != null) YukariLobbyManager.Instance.OnPlayersNumberModified(1);

		// 이 타이밍에는 내가 로컬 플레이어인지 알지 못하는 거 같다
		// Note: isLocalPlayer is not guaranteed to be set until OnStartLocalPlayer is called.

		if (isLocalPlayer)
        {
            SetupLocalPlayer();
        }
        else
        {
            SetupOtherPlayer();
        }
		
    }

	public override void OnStartLocalPlayer()
	{
		base.OnStartLocalPlayer();

		SetupLocalPlayer();
	}

	public override void OnStartAuthority()
    {
        base.OnStartAuthority();

		Debug.Log("OnStartAuthority");

		// 여기서 Rpc로 해당 플레이어의 초기화를 해준다
		// 얘는 오브젝트가 생성될 때 서버에서 한번 호출된다
		
        //RpcSetupLocalPlayer();
    }

    void SetupLocalPlayer()
    {
		Debug.Log("SetupLocalPlayer");
		YukariLobbyManager.Instance.mainPanel.readyButton.onClick.RemoveAllListeners();
		YukariLobbyManager.Instance.mainPanel.readyButton.onClick.AddListener(OnReadyClicked);

		YukariLobbyManager.Instance.mainPanel.unreadyButton.onClick.RemoveAllListeners();
		YukariLobbyManager.Instance.mainPanel.unreadyButton.onClick.AddListener(OnUnreadyClicked);

        OnClientReady(false);

        YukariLobbyManager.Instance.topPanel.nicknameField.onEndEdit.AddListener(CmdNicknameChanged);

        // Set character.
        // Character selection is not implemented yet.
        if(YukariLobbyManager.Instance._playerNumber == 1)
        {
            CmdCharacterChanged(Character.YuzukiYukari);
        }
        else if(YukariLobbyManager.Instance._playerNumber == 2)
        {
            CmdCharacterChanged(Character.TsurumakiMaki);
        }

        // host only can interact with campaign buttons
        if(isServer)
        {
            foreach(Button b in campaignButtons)
            {
                b.interactable = true;
            }
        }

        // selectable SCG setting by referring to campaign info
        // SetupSCGButton();

        // Require players list item setting
        // but how?
    }

    void SetupOtherPlayer()
    {
		// It might be players list item setting
		Debug.Log("SetupOtherPlayer");
    }

    public override void OnClientReady(bool readyState)
    {
		Debug.Log("OnClientReady");

		if (!isLocalPlayer) return;

        if(readyState)
        {
			YukariLobbyManager.Instance.mainPanel.readyButton.gameObject.SetActive(false);
			YukariLobbyManager.Instance.mainPanel.unreadyButton.gameObject.SetActive(true);
        }
        else
        {
			YukariLobbyManager.Instance.mainPanel.readyButton.gameObject.SetActive(true);
			YukariLobbyManager.Instance.mainPanel.unreadyButton.gameObject.SetActive(false);
        }
    }

    [Command]
    public void CmdNicknameChanged(string name)
    {
        nickname = name;
    }

    [Command]
    public void CmdCharacterChanged(Character character)
    {
        this.character = character;
    }

    [ClientRpc]
    public void RpcChangeHostName(string hostName)
    {
        YukariLobbyManager.Instance.hostInfo.text = hostName;
    }

	public void OnLoading()
	{
		RpcShowLoadingIcon();
	}

    [ClientRpc]
    public void RpcShowLoadingIcon()
    {
		// set loading icon active
		if (isLocalPlayer)
		{
			YukariLobbyManager.Instance.mainPanel.loadingIcon.enabled = true;
			YukariLobbyManager.Instance.mainPanel.unreadyButton.gameObject.SetActive(false);
		}
	}

	[ClientRpc]
	public void RpcSetupLocalPlayer()
	{
		if (isLocalPlayer)
			SetupLocalPlayer();
	}

    /// <summary>
    /// call YukariLobbyManager.OnCampaignNumber on every client
    /// </summary>
    [ClientRpc]
    public void RpcOnCampaignNumber(int number)
    {
        if(isLocalPlayer)
        {
            YukariLobbyManager.Instance.OnCampaignNumber(number);
        }
    }

    // ---------------------------------- OnClick callbacks

    public void OnReadyClicked()
    {
		Debug.Log("OnReadyClicked");
        SendReadyToBeginMessage();
    }

    public void OnUnreadyClicked()
    {
        SendNotReadyToBeginMessage();
    }

    // ---------------------------------- SyncVar hooks

    public void OnMyNickname(string newNickname)
    {
        nickname = newNickname;

        if (isServer)
        {
            RpcChangeHostName(nickname);
        }
    }

    public void OnMyCharacter(Character newCharacter)
    {
        
    }
}
