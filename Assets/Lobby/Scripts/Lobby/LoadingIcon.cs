﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LoadingIcon : MonoBehaviour {

    public Sprite[] sprites;

    private float _timer;
    private Image _image;
    private int _index;

	// Use this for initialization
	void Start () {
        _image = GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {
        _timer += Time.deltaTime;

        if(_timer > 0.1f)
        {
            _index++;
            if (_index == sprites.Length)
                _index = 0;

            _timer = 0f;
        }

        _image.sprite = sprites[_index];
	}
}
