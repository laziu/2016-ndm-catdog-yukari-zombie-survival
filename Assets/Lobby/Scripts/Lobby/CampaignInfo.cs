﻿
public static class CampaignInfo
{
    public static readonly string[] Scene =
    {
        "Campaign01",
        "Campaign02"
    };

    public static readonly Character[] Starring =
    {
        Character.YuzukiYukari | Character.TsurumakiMaki,
        Character.YuzukiYukari | Character.TsurumakiMaki
    };

    public static readonly string[] Title =
    {
        "제 1화 : 저 생존자가 됩니다",
        "제 2화 : 주위에 방어선을 구축하는 거다"
    };

    public static readonly string[] Summary =
    {
        "또 다시 한결같은 마이페이스 유카리에게 휘말려버린 마키. 이번엔 좀비?",
        "편의점에 갖혀버린 유카리와 마키, 코토노하 자매가 올 때까지 버텨야 한다."
    };
}