﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class YukariTitle : MonoBehaviour {

    public Text title;
    public Text summary;
    public Image logo;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        //Bob logo
        logo.rectTransform.rotation = new Quaternion(0f, 0f, Mathf.Sin(Time.time) * 0.05f, 1f);
    }
}
