﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Networking.Types;
using UnityEngine.Networking.Match;

public class YukariLobbyManager : NetworkLobbyManager {

    public static YukariLobbyManager Instance;

    [Tooltip("Time in second between all players ready & match start")]
    public float prematchCountdown = 3.0f;

    [Space]
    [Header("UI Reference")]
    public GameObject yukariLobby;
    public YukariMainPanel mainPanel;
    public YukariBottomPanel bottomPanel;
    public YukariTopPanel topPanel;
    public YukariTitle title;

	public Button startHost;
	public Button back;

	public InputField nickname;
    public Text hostInfo;

    // Currently selected campaign.
    // Only be changed by host and broadcasted all clients.
    [HideInInspector]
    public int _campaignNumber = 0;

    //Client numPlayers from NetworkManager is always 0, so we count (throught connect/destroy in LobbyPlayer) the number
    //of players, so that even client know how many player there is.
    [HideInInspector]
    public int _playerNumber = 0;

    //used to disconnect a client properly when exiting the matchmaker
    [HideInInspector]
    public bool _isMatchmaking = false;

    // used to notice to client that all clients are ready
    [HideInInspector]
    public bool _allready = false;

    // used to notice to YukariLobbyPlayer that campaign number is changed
    [HideInInspector]
    public bool _campaignDirty = false;

    protected bool _disconnectServer = false;

    protected ulong _currentMatchID;

    protected YukariLobbyHook _lobbyHooks;


    // Use this for initialization
    void Start () {
		Debug.Log("YukariLobbyManager Starts");
        Instance = this;

        _lobbyHooks = GetComponent<YukariLobbyHook>();

        GetComponent<Canvas>().enabled = true;

		DontDestroyOnLoad(gameObject);

		// Start host as soon as game starts
		StartHost();
    }
	
	// Update is called once per frame
	void Update () {
	
	}

	// There is bug that server instance is not clearly removed when StopHost() is called.
	// so we should to call NetworkServer.Reset() method before StartHost() or StartClient()
	public override NetworkClient StartHost()
	{
		NetworkServer.Reset();
		return base.StartHost();
	}

	public override void OnLobbyClientSceneChanged(NetworkConnection conn)
    {
        if (SceneManager.GetSceneAt(0).name == lobbyScene)
        {
            yukariLobby.SetActive(true);

			Debug.Log("OnClientSceneChanged");
			/*
			if (conn.playerControllers[0].unetView.isServer)
			{
				Debug.Log("server");
				
			}
			else
			{
				Debug.Log("client");
				
			}
			*/
		}
		else
        {
            yukariLobby.SetActive(false);
        }

        mainPanel.InitIconVisability();
    }

    public void SetServerInfo(string hostName, int campaign)
    {
        hostInfo.text = hostName;
        OnCampaignNumber(campaign);
    }

	public void ConnectButton_Host()
	{
		StopHost();
		networkAddress = topPanel.ipField.text;
		//NetworkServer.Reset();
		StartClient();
	}

	public void ConnectButton_Client()
	{
		StopClient();
		networkAddress = topPanel.ipField.text;
		//NetworkServer.Reset();
		StartClient();
	}

    // ----------------- Server management

    public void AddLocalPlayer()
    {
        TryToAddPlayer();
    }

    public void RemovePlayer(YukariLobbyPlayer player)
    {
        player.RemovePlayer();
    }

    public void StopHostClbk()
    {
        StopHost();
		yukariLobby.SetActive(true);
		bottomPanel.EnableCampaignButtons(false);
	}

    public void StopClientClbk()
    {
        StopClient();
		yukariLobby.SetActive(true);
	}

    //===================

    public override void OnStartHost()
    {
        base.OnStartHost();

		Debug.Log("OnStartHost");

		string s = "";
        if (nickname.text == "") s = "Nonamed";
        else s = nickname.text;

        SetServerInfo(s, _campaignNumber);

		// enable campaign buttons
		bottomPanel.EnableCampaignButtons(true);
	}

    public override void OnMatchCreate(CreateMatchResponse matchInfo)
    {
        base.OnMatchCreate(matchInfo);
        _currentMatchID = (ulong)matchInfo.networkId;
    }

    public void OnDestroyMatch(BasicResponse extendedInfo)
    {
        if (_disconnectServer)
        {
            StopMatchMaker();
            StopHost();
        }
    }

    public void OnPlayersNumberModified(int count)
    {
        _playerNumber += count;

        int localPlayerCount = 0;
        foreach (PlayerController p in ClientScene.localPlayers)
            localPlayerCount += (p == null || p.playerControllerId == -1) ? 0 : 1;
    }

    private int GetMaxPlayer(Character starring)
    {
        int result = 0;

        if ((starring & Character.YuzukiYukari) != 0) result++;
        if ((starring & Character.TsurumakiMaki) != 0) result++;
        if ((starring & Character.KotonohaAkane) != 0) result++;
        if ((starring & Character.KotonohaAoi) != 0) result++;

        return result;
    }

    public void OnCampaignNumber(int newNumber)
    {
        if (newNumber < 0) return;

        // change lobbymanager data
        _campaignNumber = newNumber;
        playScene = CampaignInfo.Scene[newNumber];
        maxPlayers = GetMaxPlayer(CampaignInfo.Starring[newNumber]);

        // change thumbnail
        mainPanel.ChangeThumbnail(_campaignNumber);

        // change title and summary
        title.title.text = CampaignInfo.Title[_campaignNumber];
        title.summary.text = CampaignInfo.Summary[_campaignNumber];
    }

    // ----------------- Server callbacks ------------------

    public override GameObject OnLobbyServerCreateLobbyPlayer(NetworkConnection conn, short playerControllerId)
    {
		Debug.Log("Create lobby player");
        GameObject obj = Instantiate(lobbyPlayerPrefab.gameObject) as GameObject;

        YukariLobbyPlayer newPlayer = obj.GetComponent<YukariLobbyPlayer>();
		if (_playerNumber == 1) newPlayer.character = Character.YuzukiYukari;
		else if (_playerNumber == 2) newPlayer.character = Character.TsurumakiMaki;
        //newPlayer.readyButton = mainPanel.readyButton;
        //newPlayer.unreadyButton = mainPanel.unreadyButton;
        //newPlayer.nicknameField = topPanel.nicknameField;
        //newPlayer.campaignButtons = bottomPanel.campaignList;


        for (int i = 0; i < lobbySlots.Length; ++i)
        {
            YukariLobbyPlayer p = lobbySlots[i] as YukariLobbyPlayer;

            if (p != null)
            {
                // print notice in comments tab

                // edit players tab items
            }
        }

        return obj;
    }

    public override void OnLobbyServerPlayerRemoved(NetworkConnection conn, short playerControllerId)
    {
        for (int i = 0; i < lobbySlots.Length; ++i)
        {
            YukariLobbyPlayer p = lobbySlots[i] as YukariLobbyPlayer;

            if (p != null)
            {
                // print notice in comments tab

                // edit players tab items
            }
        }
    }

    public override void OnLobbyServerDisconnect(NetworkConnection conn)
    {
        for (int i = 0; i < lobbySlots.Length; ++i)
        {
            YukariLobbyPlayer p = lobbySlots[i] as YukariLobbyPlayer;

            if (p != null)
            {
                
            }
        }
    }

    public override bool OnLobbyServerSceneLoadedForPlayer(GameObject lobbyPlayer, GameObject gamePlayer)
    {
		//This hook allows you to apply state data from the lobby-player to the game-player
		//just subclass "LobbyHook" and add it to the lobby object.
		Debug.Log("OnLobbyServerSceneLoadedForPlayer");
        if (_lobbyHooks)
            _lobbyHooks.OnLobbyServerSceneLoadedForPlayer(this, lobbyPlayer, gamePlayer);

        return true;
    }

    // --- Countdown management

    public override void OnLobbyServerPlayersReady()
    {
        // check whether all ready
        _allready = true;
        for (int i = 0; i < lobbySlots.Length; ++i)
        {
            if (lobbySlots[i] != null)
                _allready &= lobbySlots[i].readyToBegin;
        }

        if (_allready)
            StartCoroutine(ServerCountdownCoroutine());
    }

    public IEnumerator ServerCountdownCoroutine()
    {
		// set loading icon active
		for (int i = 0; i < lobbySlots.Length; ++i)
		{
			YukariLobbyPlayer p = lobbySlots[i] as YukariLobbyPlayer;

			if (p != null)
			{
				p.OnLoading();
			}
		}

		yield return new WaitForSeconds(prematchCountdown);

        ServerChangeScene(playScene);
    }

    // ----------------- Client callbacks ------------------

    public override void OnClientConnect(NetworkConnection conn)
    {
        base.OnClientConnect(conn);

		Debug.Log("OnClientConnect");
		
		topPanel.connectButton.onClick.RemoveAllListeners();

		if (!NetworkServer.active)
        {//only to do on pure client (not self hosting client)
            for (int i = 0; i < lobbySlots.Length; ++i)
            {
                YukariLobbyPlayer p = lobbySlots[i] as YukariLobbyPlayer;

                if (p != null)
                {
                    // find server and get server info
                    if(p.isServer)
                    {
                        SetServerInfo(p.nickname, _campaignNumber);
                    }
                }
            }
			Debug.Log("not active");
			topPanel.connectButton.onClick.AddListener(ConnectButton_Client);
		}
		else
		{
			Debug.Log("active");
			topPanel.connectButton.onClick.AddListener(ConnectButton_Host);
		}
    }

	public override void OnClientDisconnect(NetworkConnection conn)
	{
		base.OnClientDisconnect(conn);
		yukariLobby.SetActive(true);
	}

	public override void OnClientError(NetworkConnection conn, int errorCode)
	{
		base.OnClientError(conn, errorCode);
		yukariLobby.SetActive(true);
	}
}
