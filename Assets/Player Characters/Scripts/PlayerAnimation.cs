﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

/// <summary>
/// Animation control of player character
/// </summary>
public class PlayerAnimation : NetworkBehaviour {

	private Animator _animator;							// Animator
	private Transform _character;						// Child object named "Character"
    private PlayerControl _pControl;
    private CharacterController _cControl;

	public GameObject soul;

	public Animator Animator { get { return _animator; } set { _animator = value; } }

	// Use this for initialization
	void Start () {
        _pControl = GetComponent<PlayerControl>();
        _cControl = GetComponent<CharacterController>();
	}
	
	// Update is called once per frame
	void Update () {
		if (_animator == null) return;

        if (_pControl == null) return;

		if (_pControl.isMoving)
		{
			Vector3 focus = new Vector3(-_pControl.currentHorizontal, 0, -_pControl.currentVertical);
            //_character.rotation = Quaternion.LookRotation(focus);
            SetPlayerLookRotation(focus);
            _animator.SetBool("Run", true);
		}
		else 
		{
			_animator.SetBool("Run", false);
		}

        if(_cControl.isGrounded)
        {
            _animator.SetBool("Grounded", true);
        }
        else
        {
            _animator.SetBool("Grounded", false);
        }
	}

	public void EmitSoul()
	{
		soul.GetComponent<ParticleSystem>().Emit(1);
		soul.GetComponent<AudioSource>().Play();
	}

    public void SetAnimationParameter(string parameter, bool value)
    {
		CmdSetAnimationParameterBool(parameter, value);
    }

    public void SetAnimationParameter(string parameter, float value)
    {
		CmdSetAnimationParameterFloat(parameter, value);
    }

    public void SetAnimationParameter(string parameter)
    {
		CmdSetAnimationParameterTrigger(parameter);
    }

    private void SetPlayerLookRotation(Vector3 focus)
    {
        transform.rotation = Quaternion.LookRotation(focus);
    }

    //////////////////////////////////////////////////////////////////////////////
    // CmdSetAnimationParameter
    //////////////////////////////////////////////////////////////////////////////
     
    /// <summary>
    /// Broadcasts SetAnimationParameter
    /// </summary>
    [Command]
    public void CmdSetAnimationParameterBool(string parameter, bool value)
    {
        RpcSetAnimationParameterBool(parameter, value);
    }
    [ClientRpc]
    private void RpcSetAnimationParameterBool(string parameter, bool value)
    {
		_animator.SetBool(parameter, value);
	}

    /// <summary>
    /// Broadcasts SetAnimationParameter
    /// </summary>
    [Command]
    public void CmdSetAnimationParameterFloat(string parameter, float value)
    {
        RpcSetAnimationParameterFloat(parameter, value);
    }
    [ClientRpc]
    private void RpcSetAnimationParameterFloat(string parameter, float value)
    {
		_animator.SetFloat(parameter, value);
	}

    /// <summary>
    /// Broadcasts SetAnimationParameter
    /// </summary>
    [Command]
    public void CmdSetAnimationParameterTrigger(string parameter)
    {
        RpcSetAnimationParameterTrigger(parameter);
    }
    [ClientRpc]
    private void RpcSetAnimationParameterTrigger(string parameter)
    {
		_animator.SetTrigger(parameter);
	}
}
