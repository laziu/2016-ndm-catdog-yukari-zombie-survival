﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Interaction Box is always watching interactable objects.
/// Closest one will be choosen if more than one interactable are in the box.
/// Attached to the "Melee Hit Box" Object which is child of Player Character.
/// </summary>
[RequireComponent(typeof(BoxCollider))]
public class InteractionBox : MonoBehaviour {

    public Interactable Target { get; set; }

    private List<Interactable> _interactables;

    /// <summary>
    /// If false, Target is null even though there are interactables in the area.
    /// </summary>
    public bool Enabled { get; set; }

	// Use this for initialization
	void Start () {
        _interactables = new List<Interactable>();
        Enabled = true;
	}
	
	// Update is called once per frame
	void Update () {
        
        Target = null;

        // Turn off all outlines at first
        foreach (Interactable i in _interactables)
        {
            
            i.gameObject.layer = LayerMask.NameToLayer("Default");
        }

        if(Enabled)
        {
            // Update the target at every moment
            Interactable result = null;

            foreach (Interactable i in _interactables)
            {
                if (result == null)
                {
                    result = i;
                    continue;
                }

                float d1 = Vector3.Distance(transform.position, result.transform.position);
                float d2 = Vector3.Distance(transform.position, i.transform.position);

                if (d1 > d2)
                {
                    result = i;
                }
            }

            Target = result;

            // And turn on outline of the target 
            if (Target != null)
                Target.gameObject.layer = LayerMask.NameToLayer("Interactable");
        }
	}

    void OnTriggerEnter(Collider c)
    {
        Interactable i = c.GetComponent<Interactable>();

        if (i == null) return;

        _interactables.Add(i);
    }

    void OnTriggerExit(Collider c)
    {
        Interactable i = c.GetComponent<Interactable>();

        if (i == null) return;

        // Turn off outline
        // To prevent a situation that the target object's outline not be extinguished
        i.gameObject.layer = LayerMask.NameToLayer("Default");

        if (_interactables.Contains(i)) _interactables.Remove(i);
    }
}
