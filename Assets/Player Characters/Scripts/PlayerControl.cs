﻿using UnityEngine;
using UnityEngine.Networking;

/// <summary>
/// Character control by player input
/// </summary>
[RequireComponent(typeof(CharacterController))]
public class PlayerControl : NetworkBehaviour
{

	public float speed = 2.0F;
	public float jumpSpeed = 6.0F;
	public float gravity = 20.0F;
	public float maxBodyRotation = 0.5f;
	public float maxHeadRotation = 0.5f;
	public float mouseSpeed = 0.1f;

	// To push objects
	public float pushPower = 3.0f;
	public float weight = 6.0f;

	private Vector3 _moveDirection = Vector3.zero;
    private float _moveDirectionY = 0f;

	private CharacterController _controller;
    private PlayerInfo _info;

    // Sync Vars
    [SyncVar]
    public bool isMoving;
    [SyncVar]
    public float currentVertical;
    [SyncVar]
    public float currentHorizontal;
    [SyncVar]
    public bool carrySomething;

	// will be enabled by dialogue action
	public bool canMove;

    // This field points player's melee hit box
    public MeleeHitBox MeleeHitBox { get; set; }
    // This field points player's interaction box
    public InteractionBox InteractionBox { get; set; }
	// This field points player's shoot origin
	public Transform ShootOrigin { get; set; }

    [ClientCallback]
	void Start()
	{
		Debug.Log("Player Start");
		_controller = GetComponent<CharacterController>();
        _info = GetComponent<PlayerInfo>();

		
		// Make camera chase character
        if(isLocalPlayer)
        {
			QuarterView.Instance.StopCameraAnimation();
			QuarterView.Instance.elastic = true;
			QuarterView.Instance.timeLength = 0.2f;
            QuarterView.Instance.MoveTo(transform);
            QuarterView.Instance.Zoom(-10);
        }
		

        // Find "Melee Hit Box"
        Transform hitBox = transform.Find("Melee Hit Box");
        if (hitBox != null)
            MeleeHitBox = hitBox.GetComponent<MeleeHitBox>();
        else
            Debug.LogError("Player Character : Melee Hit Box not found.");

        // Find "Interaction Box"
        Transform interactionBox = transform.Find("Interaction Box");
        if (interactionBox != null)
            InteractionBox = interactionBox.GetComponent<InteractionBox>();
        else
            Debug.LogError("Player Character : Interaction Box not found.");

		ShootOrigin = transform.Find("Shoot Origin");
		if (ShootOrigin == null) Debug.LogError("Player Character : Shoot Origin not found.");
    }
	
    [ClientCallback]
	void Update()
	{
		// Only the client can control the player character
		if (!isLocalPlayer) return;

        if (_info.dead) return;

		if (!canMove) return;

        //////////////////////////////////////////////////////////////////////
        // Movement
        //////////////////////////////////////////////////////////////////////

		float horizontal = Input.GetAxis("Horizontal");
		float vertical = Input.GetAxis("Vertical");
        float rawHorizontal = Input.GetAxisRaw("Horizontal");
        float rawVertical = Input.GetAxisRaw("Vertical");

        if (rawHorizontal != 0 || rawVertical != 0)
            CmdSetMovingStatus(true, vertical, horizontal);
        else
            CmdSetMovingStatus(false, vertical, horizontal);

        _moveDirection = new Vector3(-horizontal, 0, -vertical);
        _moveDirection *= speed;
        _moveDirection.y = _moveDirectionY;

        if (_controller.isGrounded)
		{
            _moveDirectionY = 0f;

            if (Input.GetButton("Jump"))
			    _moveDirectionY = jumpSpeed;

		}

		_moveDirectionY -= gravity * Time.deltaTime;
		_controller.Move(_moveDirection * Time.deltaTime);

        //////////////////////////////////////////////////////////////////////
        // Use Weapons
        //////////////////////////////////////////////////////////////////////

        // Left Ctrl key
        if(Input.GetButton("Fire1"))
        {
			if (carrySomething)
			{
				if (_info.currentBarricade != null)
				{
					CmdThrowBarricade();
				}
				if (_info.currentItemBox != null)
				{
					CmdThrowItemBox();
				}
			}
			else
			{
				CmdAttack();
			}
        }

        //////////////////////////////////////////////////////////////////////
        // Interaction
        //////////////////////////////////////////////////////////////////////

        // Shift key
        if(Input.GetButtonDown("Fire3"))
        {
			if (carrySomething)
			{
				if(_info.currentBarricade != null)
				{
					CmdDropBarricade();
				}
				if(_info.currentItemBox != null)
				{
					CmdDropItemBox();
				}
			}
			else
			{
				// I poke you
				CmdPoke();
			}
        }
		
		// 'Z' key
		if(Input.GetKeyDown(KeyCode.Z))
		{
			if(!carrySomething)
				CmdDropWeapon();
		}

		if(Input.GetKeyDown(KeyCode.Escape))
		{
			if (isServer)
			{
				YukariLobbyManager.Instance.ServerChangeScene(YukariLobbyManager.Instance.lobbyScene);
			}
		}
    }

    [ServerCallback]
	void OnControllerColliderHit(ControllerColliderHit hit)
	{
        Rigidbody rb = hit.collider.attachedRigidbody;
        Vector3 force;

        // no rigidbody
        if (rb == null || rb.isKinematic) return;

        // We use gravity and weight to push things down, we use
        // our velocity and push power to push things other directions
        if (hit.moveDirection.y < -0.3)
        {
            force = new Vector3(0, -0.5f, 0) * gravity * weight;
        }
        else
        {
            force = hit.controller.velocity * pushPower;
        }

        // Push objects
        rb.AddForceAtPosition(force, hit.point);
	}
    
    [Command]
    void CmdSetMovingStatus(bool value, float vertical, float horizontal)
    {
        isMoving = value;
        currentVertical = vertical;
        currentHorizontal = horizontal;
    }

    [Command]
    public void CmdSetCarryingStatus(bool value)
    {
        carrySomething = value;
		GameObject weapon = _info.currentWeapon;
		if (weapon != null)
			weapon.SetActive(!value);
    }

	[Command]
	public void CmdDropWeapon()
	{
		Debug.Log("CmdDropWeapon");
		DropWeapon();
	}
	public void DropWeapon()
	{
		if (_info.currentWeapon != null)
		{
			Debug.Log("currentWeapon");
			_info.currentWeapon.GetComponent<Weapon>().Drop();
		}
	}

	[Command]
	public void CmdDropBarricade()
	{
		DropBarricade();
	}
	public void DropBarricade()
	{
		_info.currentBarricade.GetComponent<Barricade>().Drop();
	}

	[Command]
	public void CmdDropItemBox()
	{
		DropItemBox();
	}
	public void DropItemBox()
	{
		_info.currentItemBox.GetComponent<ItemBox>().Drop();
	}

	[Command]
	public void CmdThrowBarricade()
	{
		ThrowBarricade();
	}
	public void ThrowBarricade()
	{
		_info.currentBarricade.GetComponent<Barricade>().Throw();
	}

	[Command]
	public void CmdThrowItemBox()
	{
		ThrowItemBox();
	}
	public void ThrowItemBox()
	{
		_info.currentItemBox.GetComponent<ItemBox>().Throw();
	}

	[Command]
	public void CmdAttack()
	{
		if (!carrySomething)
		{
			GameObject curWeapon = _info.currentWeapon;
			Weapon weapon = null;
			if (curWeapon != null) weapon = _info.currentWeapon.GetComponent<Weapon>();
			if (weapon != null) weapon.Attack();
		}
	}

	[Command]
	private void CmdPoke()
	{
		Interactable target = InteractionBox.Target;
		if (target != null)
		{
			// I poke you
			target.Player = this.gameObject;
			// giving itself behavior should also be done on client
			RpcSetPokePlayer();

			target.Poke();
		}
	}

	[ClientRpc]
	private void RpcSetPokePlayer()
	{
		Interactable target = InteractionBox.Target;
		if (target != null)
		{
			target.Player = this.gameObject;
		}
	}
}