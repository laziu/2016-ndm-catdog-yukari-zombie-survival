﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Collider))]
public class Teleporter : MonoBehaviour {

	public Teleporter destination;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider c)
	{
		if (destination == null) return;

		PlayerControl control = c.GetComponent<PlayerControl>();
		if(control != null)
		{
			control.transform.position = destination.transform.position;
		}
	}
}
