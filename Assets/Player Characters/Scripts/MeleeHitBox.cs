﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// MeleeHitBox is always watching attackable objects.
/// Attached to the "Melee Hit Box" Object which is child of Player Character.
/// </summary>
[RequireComponent(typeof(BoxCollider))]
public class MeleeHitBox : MonoBehaviour {

    private readonly float HIT_BOX_SIZE_Y = 1f;

    private List<Attackable> _attackables;
    private BoxCollider _hitBox;

    public List<Attackable> GetAttackables() { return _attackables; }

    // Use this for initialization
    void Start () {
        _attackables = new List<Attackable>();
        _hitBox = GetComponent<BoxCollider>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    /// <summary>
    /// Melee weapons set hit box size on Item.Take(GameObject player) method
    /// Y axis size is fixed
    /// </summary>
    public void SetHitBoxSize(float x, float z)
    {
        _hitBox.size = new Vector3(x, HIT_BOX_SIZE_Y, z);
        _hitBox.center = new Vector3(_hitBox.center.x, _hitBox.center.y, (z / 2f) - 0.5f);
    }

    void OnTriggerEnter(Collider c)
    {
        Attackable attackable = c.gameObject.GetComponent<Attackable>();
        if (attackable != null)
        {
            _attackables.Add(attackable);
        }
    }

    void OnTriggerExit(Collider c)
    {
        Attackable attackable = c.gameObject.GetComponent<Attackable>();
        if(_attackables.Contains(attackable))
        {
            _attackables.Remove(attackable);
        }
    }
}
