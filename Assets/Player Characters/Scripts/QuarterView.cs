﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Make Quarter view camera chasing player by attaching it to a camera
/// </summary>
[RequireComponent(typeof(Camera))]
public class QuarterView : MonoBehaviour
{

	private static QuarterView _instance = null;

	private Transform _destPosition;           // The object that camera chases
	private float _zoom = 0;                    // Value gained from Zoom()

	private Quaternion _defaultRotation = Quaternion.Euler(45, 180, 0);

	// for uniform move
	private float _timer;
	private Vector3 _lVelocity;
	private Vector3 _aVelocity;

	private bool _animated;

	public bool elastic;
	[Tooltip("Time in seconds to reach to the destination.")]
	public float timeLength;

	[Header("Nodes")]
	[Tooltip("Camera moves along the nodes when StartCameraAnimation() is called")]
	public Transform[] nodes;

	private Queue<Transform> _nodeQueue;

	private Quaternion initRot;
	private float tValue;

	public static QuarterView Instance
	{
		get
		{
			if (_instance == null) Debug.LogError("QuarterView has not awaken.");
			return _instance;
		}
	}

	void Awake()
	{
		if (_instance != null)
		{
			Destroy(this.gameObject);
			return;
		}
		else
		{
			_instance = this;
		}
	}

	// Use this for initialization
	void Start()
	{
		_lVelocity = Vector3.zero;
		_aVelocity = Vector3.zero;
		_nodeQueue = new Queue<Transform>();
		foreach (Transform n in nodes)
		{
			_nodeQueue.Enqueue(n);
		}

		//Debug code
		//StartCoroutine(StartDebug());
	}

	// Update is called once per frame
	void Update()
	{

		if (_animated)
		{
			if (_lVelocity == Vector3.zero && _aVelocity == Vector3.zero)
			{
				if (_nodeQueue.Count > 0)
					MoveTo(_nodeQueue.Dequeue());
			}
		}

		if (_destPosition != null)
		{
			// Bias to zoom
			Vector3 bias = transform.forward * _zoom;

			Vector3 destPos = _destPosition.position + bias;
			Quaternion destRot = _destPosition.rotation;
			Vector3 myPos = transform.position;
			Quaternion myRot = transform.rotation;
			float delta = Time.deltaTime;

			// elastic move
			// elastic move dosen't care about rotation
			if (elastic)
			{
				float X = destPos.x;
				float Y = destPos.y;
				float Z = destPos.z;
				float myX = myPos.x;
				float myY = myPos.y;
				float myZ = myPos.z;

				myX = Mathf.Lerp(myX, X, delta / timeLength);
				myY = Mathf.Lerp(myY, Y, delta / timeLength);
				myZ = Mathf.Lerp(myZ, Z, delta / timeLength);

				transform.position = new Vector3(myX, myY, myZ);
			}
			// uniform move
			else
			{
				_timer += delta;
				if (_lVelocity == Vector3.zero)
					_lVelocity = (destPos - myPos) / timeLength;
				if (_aVelocity == Vector3.zero)
				{
					float x = destRot.x - myRot.x;
					float y = destRot.y - myRot.y;
					float z = destRot.z - myRot.z;
					_aVelocity = new Vector3(x, y, z) / timeLength;
					initRot = new Quaternion(myRot.x, myRot.y, myRot.z, myRot.w);
				}

				// arrived
				if (_timer >= timeLength)
				{
					float newDelta = delta - (_timer - timeLength);
					transform.position += _lVelocity * newDelta;

					//Vector3 dRot = _aVelocity * newDelta;
					//float x = transform.rotation.x + dRot.x;
					//float y = transform.rotation.y + dRot.y;
					//float z = transform.rotation.z + dRot.z;
					//transform.rotation = Quaternion.Euler(x, y, z);
					//transform.Rotate(dRot.x, dRot.y, dRot.z);
					tValue += newDelta / timeLength;
					transform.rotation = Quaternion.Slerp(initRot, destRot, tValue);

					tValue = 0f;
					_timer = timeLength;
					_lVelocity = Vector3.zero;
					_aVelocity = Vector3.zero;
				}
				// keep moving
				else
				{
					transform.position += _lVelocity * delta;

					//Vector3 dRot = _aVelocity * delta;
					//float x = transform.rotation.x + dRot.x;
					//float y = transform.rotation.y + dRot.y;
					//float z = transform.rotation.z + dRot.z;
					//transform.rotation = Quaternion.Euler(x, y, z);
					//transform.Rotate(dRot.x, dRot.y, dRot.z);
					tValue += delta / timeLength;
					transform.rotation = Quaternion.Slerp(initRot, destRot, tValue);
				}
			}
		}
	}

	private void ElasticMove(Vector3 destPos)
	{

	}

	private void UniformMove()
	{

	}

	public void MoveTo(Transform dest)
	{
		_destPosition = dest;
		if (!elastic) _timer = 0f;
	}

	/// <summary>
	/// Camera position vector will be added by (transform.forward * value)
	/// </summary>
	public void Zoom(float value)
	{
		_zoom = value;
	}

	/// <summary>
	/// Camera starts to move along the nodes
	/// </summary>
	public void StartCameraAnimation()
	{
		_animated = true;
	}
	public void StopCameraAnimation()
	{
		_animated = false;
		_nodeQueue.Clear();
		transform.rotation = _defaultRotation;
	}

	/// <summary>
	/// Clear node queue and fill with the new nodes
	/// </summary>
	public void SetNodes(Transform[] nodes)
	{
		_nodeQueue.Clear();
		foreach (Transform n in nodes)
		{
			_nodeQueue.Enqueue(n);
		}
	}

	IEnumerator StartDebug()
	{
		yield return new WaitForSeconds(2f);

		elastic = false;
		StartCameraAnimation();
	}
}
