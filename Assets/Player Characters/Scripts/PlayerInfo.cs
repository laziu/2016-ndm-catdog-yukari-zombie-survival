﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

/// <summary>
/// 4 Characters
/// </summary>
public enum Character
{
    NullCharacter = 0,
    YuzukiYukari = 1,
    TsurumakiMaki = 2,
    KotonohaAkane = 4,
    KotonohaAoi = 8
}

/// <summary>
/// Manage every player infomation except that of player action caused by input.
/// </summary>
public class PlayerInfo : NetworkBehaviour {

	public GameObject yukariModel;
    public GameObject makiModel;

	public float maxHealth;

    //Sync vars
    [SyncVar]
    public Character character;
    [SyncVar(hook = "OnHealth")]
    public float health;
    [SyncVar]
    public GameObject currentWeapon;
	[SyncVar]
	public GameObject currentItemBox;
	[SyncVar]
	public GameObject currentBarricade;
    [SyncVar(hook = "OnDead")]
    public bool dead;

	private bool _modelLoaded;

	// Use this for initialization
	void Start () {
		Debug.Log("PlayerInfo Start");

		// Temporary code
		if (isServer)
		{
			if (isLocalPlayer)
				character = Character.YuzukiYukari;
			else
				character = Character.TsurumakiMaki;
		}
	}
	
	// Update is called once per frame
	//[ServerCallback]
	void Update () {

		// Load model
		if(!_modelLoaded)
		{
			OnCharacter(character);
			EventObserver.Instance.SubmitPlayerReference(this);
			_modelLoaded = true;
		}

		if(isServer)
		{
			if (health <= 0 && !dead)
			{
				dead = true;
			}
		}
	}

	public void GetDamaged(float damage)
	{
		CmdGetDamaged(damage);
	}

	[Command]
	private void CmdGetDamaged(float damage)
	{
		// decrease player health
		health -= damage;

		// do proper task on each client
		RpcGetDamaged(damage);
	}
	[ClientRpc]
	private void RpcGetDamaged(float damage)
	{
		if(isLocalPlayer)
		{
			// screen effect or something?
		}
		else
		{
			// well...
		}
	}

    [Command]
    public void CmdAddHealth(float value)
    {
        health += value;
    }

	[Command]
	public void CmdSetCurrentWeapon(GameObject weapon)
	{
		currentWeapon = weapon;
	}

	[Command]
	public void CmdSetCurrentItemBox(GameObject itemBox)
	{
		currentItemBox = itemBox;
	}

	// ------------ syncvar hooks

	private void OnHealth(float newHealth)
	{
		health = newHealth;

		switch(character)
		{
			case Character.YuzukiYukari:
				HUD.Instance.yukari.SetHP(newHealth);
				break;

			case Character.TsurumakiMaki:
				HUD.Instance.maki.SetHP(newHealth);
				break;
		}
	}

	private void OnDead(bool newValue)
	{
		dead = newValue;

		if(newValue)
		{
			if(isLocalPlayer)
			{
				GetComponent<PlayerAnimation>().SetAnimationParameter("Dead", true);
			}
			StartCoroutine(EmitSoul());
		}
	}

	IEnumerator EmitSoul()
	{
		yield return new WaitForSeconds(1f);

		GetComponent<PlayerAnimation>().EmitSoul();
	}

	private void OnCharacter(Character newCharacter)
	{
		Debug.Log("OnCharacter");

		character = newCharacter;

		GameObject model = null;

		switch (newCharacter)
		{
			case Character.YuzukiYukari:
				if (yukariModel != null) model = Instantiate(yukariModel);
				HUD.Instance.yukari.Target = this;
				HUD.Instance.yukari.Init();
				break;

			case Character.TsurumakiMaki:
				if (makiModel != null) model = Instantiate(makiModel);
				HUD.Instance.maki.Target = this;
				HUD.Instance.maki.Init();
				break;
		}

		if (model != null)
		{
			model.transform.SetParent(this.transform);
			model.transform.localPosition = Vector3.zero;
			model.transform.localRotation = Quaternion.identity;
			model.transform.localScale = new Vector3(1, 1, 1);
			model.name = "Character";
		}

		PlayerAnimation anim = GetComponent<PlayerAnimation>();

		Transform _character = transform.FindChild("Character");
		if (_character == null) Debug.LogError("Player Character : Character not found.");
		anim.Animator = _character.GetComponent<Animator>();
	}
}
