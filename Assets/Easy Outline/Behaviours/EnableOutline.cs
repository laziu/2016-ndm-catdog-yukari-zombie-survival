﻿using UnityEngine;
using System.Collections;

public class EnableOutline : MonoBehaviour {

	public GameObject outlineSystem;
	public GameObject chainSaw;
	public GameObject mustang;

	// Use this for initialization
	void Start () {
		if (outlineSystem == null) return;
		if (chainSaw == null) return;
		if (mustang == null) return;

		outlineSystem.SetActive(false);

		StartCoroutine(Delay());
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	IEnumerator Delay()
	{
		yield return new WaitForSeconds(7f);

		outlineSystem.SetActive(true);
	}
}
