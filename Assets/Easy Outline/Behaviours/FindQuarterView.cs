﻿using UnityEngine;
using System.Collections;

public class FindQuarterView : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Camera camera = FindObjectOfType<QuarterView>().GetComponent<Camera>();
        OutlineSystem[] systems = transform.GetComponentsInChildren<OutlineSystem>();
        foreach(OutlineSystem s in systems)
        {
            s.mainCamera = camera;
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
