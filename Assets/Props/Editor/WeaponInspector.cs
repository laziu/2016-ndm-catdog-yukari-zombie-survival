﻿using UnityEditor;

[CustomEditor(typeof(Weapon), true)]
public class WeaponInspector : Editor
{
	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI();

		Weapon myTarget = (Weapon)target;

		myTarget.dontUseAmmo = EditorGUILayout.Toggle("Dont Use Ammo", myTarget.dontUseAmmo);

		if (!myTarget.dontUseAmmo)
		{
			myTarget.maxAmmo = EditorGUILayout.IntField("Max Ammo", myTarget.maxAmmo);
			myTarget.ammo = EditorGUILayout.IntField("Ammo", myTarget.ammo);
		}
	}
}
