﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public abstract class Item : NetworkBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    
	}

    /// <summary>
    /// It will  be called when the player character touches this item
    /// </summary>
    public abstract void Take(GameObject player);

    [ServerCallback]
    void OnTriggerEnter(Collider c)
    {
        if(c.gameObject.tag.Equals("Player"))
        {
			Take(c.gameObject);
        }
    }
}
