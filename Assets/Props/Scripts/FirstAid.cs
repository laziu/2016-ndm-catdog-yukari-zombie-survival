﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System;

public class FirstAid : Item {

    public float heal;
	public AudioClip eat;

	private GameObject _player;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public override void Take(GameObject player)
    {
		_player = player;

        PlayerInfo info = player.GetComponent<PlayerInfo>();
        if (info == null) return;


		if(info.currentItemBox == null)
		{
			if (info.health >= info.maxHealth) return;

			if (info.health + heal > info.maxHealth)
				info.health = info.maxHealth;
			else
				info.health += heal;
		}
        else
		{
			ItemBox itemBox = info.currentItemBox.GetComponent<ItemBox>();

			if (itemBox.heal >= itemBox.maxHeal) return;

			if (itemBox.heal + heal > itemBox.maxHeal)
				itemBox.heal = itemBox.maxHeal;
			else
				itemBox.heal += heal;
		}

		NetworkServer.Destroy(this.gameObject);
    }

	public override void OnNetworkDestroy()
	{
		if (eat != null)
		{
			AudioSource audio = _player.GetComponent<AudioSource>();
			audio.clip = eat;
			audio.Play();
		}
	}
}
