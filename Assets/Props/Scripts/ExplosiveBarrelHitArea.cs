﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ExplosiveBarrelHitArea : MonoBehaviour {

	private List<Rigidbody> _rigidbodys;
	private List<Attackable> _attackables;

	public List<Rigidbody> GetRigidbodys() { return _rigidbodys; }
	public List<Attackable> GetAttackables() { return _attackables; }

	// Use this for initialization
	void Start () {
		_rigidbodys = new List<Rigidbody>();
		_attackables = new List<Attackable>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter(Collider c)
	{
		Attackable a = c.GetComponent<Attackable>();
		if (a != null)
			_attackables.Add(a);
		else
		{
			Rigidbody r = c.GetComponent<Rigidbody>();
			if (r != null)
				_rigidbodys.Add(r);
		}
	}

	void OnTriggerExit(Collider c)
	{
		Attackable a = c.GetComponent<Attackable>();
		if(a != null)
		{
			if (_attackables.Contains(a))
				_attackables.Remove(a);
		}
		else
		{
			Rigidbody r = c.GetComponent<Rigidbody>();
			if (r != null)
				_rigidbodys.Remove(r);
		}
	}
}
