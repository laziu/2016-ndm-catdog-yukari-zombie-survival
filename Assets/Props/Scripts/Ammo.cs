﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System;

public class Ammo : Item {

	public int ammo;
	public AudioClip pickup;

	private GameObject _player;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public override void Take(GameObject player)
	{
		_player = player;

		PlayerInfo info = player.GetComponent<PlayerInfo>();
		if (info == null) return;

		bool get = false;

		if(info.currentItemBox == null)
		{
			if (info.currentWeapon != null)
			{
				Weapon weapon = info.currentWeapon.GetComponent<Weapon>();
				if(!weapon.dontUseAmmo)
				{
					if (weapon.ammo >= weapon.maxAmmo) return;

					if(weapon.ammo + ammo > weapon.maxAmmo)
					{
						weapon.ammo = weapon.maxAmmo;
					}
					else
					{
						weapon.ammo += ammo;
					}

					get = true;
				}
			}
		}
		else
		{
			ItemBox itemBox = info.currentItemBox.GetComponent<ItemBox>();

			if (itemBox.ammo >= itemBox.maxAmmo) return;

			if (itemBox.ammo + ammo > itemBox.maxAmmo)
				itemBox.ammo = itemBox.maxAmmo;
			else
				itemBox.ammo += ammo;
			
			get = true;
		}

		if(get) NetworkServer.Destroy(this.gameObject);
	}

	public override void OnNetworkDestroy()
	{
		if(pickup != null)
		{
			if(_player != null)
			{
				AudioSource audio = _player.GetComponent<AudioSource>();
				if (audio != null)
				{
					audio.clip = pickup;
					audio.Play();
				}
			}
		}
	}
}
