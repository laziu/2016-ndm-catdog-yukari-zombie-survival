﻿using UnityEngine;
using System.Collections;

public class Chainsaw : Melee {

	[Header("Chainsaw SE")]
	public AudioClip start;
	public AudioClip idle;
	public AudioClip highSpeed;
	public AudioClip die;

	private bool _isTaken;
	private bool _isAttacking;

	private float _startTime;
	
	void Start()
	{
		// base.Start()
		// can not access because of protection level
		_timer = cooldownTime;
		_audio = GetComponent<AudioSource>();

		if (start != null)
			_startTime = start.length;
		else
			_startTime = 0f;
	}

	// Update is called once per frame
	void Update () {

		// base.Update()
		// can not access because of protection level
		if (_timer < cooldownTime)
		{
			_timer += Time.deltaTime;
		}
		else
		{
			// Now we can swing again and animation is end
			_timer = cooldownTime;
		}

		// sound managing
		if (_audio == null) return;
		if (!_isTaken) return;

		if(Input.GetButton("Fire1"))
		{
			if (_isAttacking) return;
			if (highSpeed == null) return;
			_audio.clip = highSpeed;
			_audio.loop = true;
			_audio.Play();

			_isAttacking = true;
		}
		else if(Input.GetButtonUp("Fire1"))
		{
			_audio.Stop();
			// immediate play to avoid sound gap
			_audio.clip = idle;
			_audio.Play();
			_isAttacking = false;
		}
		else
		{
			if (!_audio.isPlaying)
			{
				_audio.clip = idle;
				_audio.loop = true;
				_audio.Play();
			}
		}
	}

	public override void Take(GameObject player)
	{
		base.Take(player);

		if (player.GetComponent<PlayerInfo>().currentWeapon != this.gameObject) return;

		if (_audio == null) return;
		if (start == null) return;
		_audio.clip = start;
		_audio.loop = false;
		_audio.Play();

		_isTaken = true;

		// to avoid sound gap :(
		StartCoroutine(WaitForStart());
	}

	public override void Drop()
	{
		base.Drop();

		_isTaken = false;
		_isAttacking = false;

		if (die == null) return;
		_audio.Stop();
		_audio.clip = die;
		_audio.loop = false;
		_audio.Play();
	}

	IEnumerator WaitForStart()
	{
		float timer = 0f;

		// wait for length of 'start'
		while(timer < _startTime - 0.7f)
		{
			timer += Time.deltaTime;

			// if player starts to fire, we don't need to wait
			// let's get out of this routine
			if (_isAttacking)
				yield break;

			// if player drop the item, we don't need to wait
			if (!_isTaken)
				yield break;

			yield return null;
		}

		if(_audio != null)
		{
			_audio.clip = idle;
			_audio.loop = true;
			_audio.Play();
		}

		yield break;
	}
}
