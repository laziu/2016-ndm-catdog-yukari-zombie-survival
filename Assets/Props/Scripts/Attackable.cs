﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

/// <summary>
/// Network Object that can be hit
/// </summary>
public abstract class Attackable : NetworkBehaviour {

    /// <summary>
    /// Call this method to attack the object to which this script be attached.
    /// </summary>
    public  void GetDamaged(GameObject agent, Vector3 impulse, float damage)
    {
        React(agent, impulse, damage);
    }

    /// <summary>
    /// Write tasks here to be conducted by the host such as decreasing HP.
    /// </summary>
    protected abstract void React(GameObject player, Vector3 impulse, float damage);
}
