﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System;
using System.Collections.Generic;

public class ExplosiveBarrel : Attackable {

	[Header("Particle")]
	public GameObject explosion;
	public GameObject fire;
	public ParticleSystem scratch;

	[Header("SE")]
	public AudioClip[] impact;

	[SyncVar]
	public float health;
	[SyncVar]
	public float damage;
	[SyncVar]
	public bool burning;

	private AudioSource _audio;

	public ExplosiveBarrelHitArea HitArea { get; set; }

	// Use this for initialization
	[ServerCallback]
	void Start () {
		_audio = GetComponent<AudioSource>();
		fire.SetActive(false);

		Transform hitArea = transform.Find("Hit Area");
		if (hitArea != null)
			HitArea = hitArea.GetComponent<ExplosiveBarrelHitArea>();
		else
			Debug.LogError("Explosive Barrel : Hit Area not found.");
	}
	
	// Update is called once per frame
	void Update () {
		if(isServer)
		{
			if (burning)
			{
				health -= 10f * Time.deltaTime;
			}

			if (health <= 0)
			{
				Explode();
			}
		}

		fire.transform.up = Vector3.up;
	}

	protected override void React(GameObject agent, Vector3 impulse, float damage)
	{
		if(agent.GetComponent<ExplosiveBarrel>() != null)
		{
			if(!burning)
			{
				LitFire();
			}

			return;
		}

		if (health > 0)
		{
			health -= damage;

			RpcEmitScratch(impulse);

			if(health < 50f)
			{
				RpcLitFire();
				burning = true;
			}
		}
	}

	public void LitFire()
	{
		health = 50f;
		RpcLitFire();
		burning = true;
	}

	[ClientRpc]
	private void RpcLitFire()
	{
		fire.SetActive(true);
	}

	[ClientRpc]
	private void RpcEmitScratch(Vector3 impulse)
	{
		if (scratch != null)
		{
			// emit particle direction to hit point
			scratch.transform.parent.rotation = Quaternion.LookRotation(-impulse);
			scratch.Emit(10);

			if (_audio != null)
			{
				if (impact.Length > 0)
				{
					int rand = UnityEngine.Random.Range(0, impact.Length);
					_audio.clip = impact[rand];
					_audio.Play();
				}
			}
		}
	}
	
	private void Explode()
	{
		List<Attackable> attackables = HitArea.GetAttackables();
		List<Rigidbody> rigidbodys = HitArea.GetRigidbodys();

		foreach (Attackable a in attackables)
		{
			/////////////////////////////////////////////////////////////////////
			/////////////////////////////////////////////////////////////////////
			// This is so BAD code
			// It must to be refactorred someday
			try
			{
				Vector3 impulse = a.transform.position - transform.position;
				a.GetDamaged(this.gameObject, impulse, damage);
			}
			// MissingReferenceException etc.
			catch(Exception e)
			{
				continue;
			}
			/////////////////////////////////////////////////////////////////////
			/////////////////////////////////////////////////////////////////////
		}

		foreach (Rigidbody r in rigidbodys)
		{
			if(r != null) r.AddExplosionForce(400f, transform.position, 4f);
		}

		NetworkServer.Destroy(this.gameObject);
	}

	public override void OnNetworkDestroy()
	{
		Instantiate(explosion, transform.position, Quaternion.identity);
	}
}
