﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System;

public enum WeaponID
{
    NullID = 0,
    Crowbar,
	Glock,
    YukariChainsaw,
    MakiMustang,
	M1014,
	SCAR
}

public abstract class Weapon : Item {

	[HideInInspector]
	public bool dontUseAmmo;
	[HideInInspector]
	[SyncVar(hook = "OnAmmo")]
	public int ammo;
	[HideInInspector]
	public int maxAmmo;

	[Space]
    public float damage;                    // Damage
    public float cooldownTime;              // Cooldown time
    public WeaponID id;                     // weapon id
    [Tooltip("Required to turn off the outline")]
    public GameObject mesh;

	protected GameObject Player { get; set; }

    // Use this for initialization
    void Start () {
        if (mesh == null) Debug.Log("Weapon : Mesh not found.");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	/// <summary>
	/// Drop the weapon
	/// </summary>
	public virtual void Drop()
	{
		Debug.Log("Drop()");

		//if (!isServer) return;

		// Set PlayerInfo current weapon to null
		Player.GetComponent<PlayerInfo>().currentWeapon = null;


		// Set parent to null
		transform.SetParent(null);

		// Turn on outline
		//RpcSetLayerRecursively();
		SetLayerRecursively(mesh, LayerMask.LayerToName(gameObject.layer));

		// Set player animation parameter
		Player.GetComponent<PlayerAnimation>().SetAnimationParameter("Weapon ID", (int)WeaponID.NullID);

		// Give force direction of player focus
		Rigidbody rb = GetComponent<Rigidbody>();
		if (rb != null)
		{
			rb.isKinematic = false;
			Vector3 dir = Player.transform.forward;
			rb.AddForce(dir * 5f, ForceMode.Impulse);
		}

		switch (Player.GetComponent<PlayerInfo>().character)
		{
			case Character.YuzukiYukari:
				HUD.Instance.yukari.NoWeapon();
				break;

			case Character.TsurumakiMaki:
				HUD.Instance.maki.NoWeapon();
				break;
		}

		Player = null;

		if (isServer)
		{
			RpcDrop();
		}
	}

	[ClientRpc]
	private void RpcDrop()
	{
		if (isServer) return;

		if(Player != null)
		{
			PlayerControl control = Player.GetComponent<PlayerControl>();
			if (control != null)
				control.DropWeapon();
		}
	}

    public override void Take(GameObject player)
    {
		// if player has already have somthing, skip take action
		if (player.GetComponent<PlayerInfo>().currentWeapon != null) return;

		// if player is carrying somthing, skip take action
		if (player.GetComponent<PlayerControl>().carrySomething) return;

		// if the weapon already has been taken, skip take action
		if (Player != null) return;

		// Get player
		Player = player;

		PlayerInfo info = Player.GetComponent<PlayerInfo>();
		PlayerAnimation anim = Player.GetComponent<PlayerAnimation>();

        // Set itself as player's current weapon and child
        info.currentWeapon = this.gameObject;

		SetToSlot();

        // Set player animation parameter
        anim.SetAnimationParameter("Weapon ID", (int)id);

		if(!info.currentWeapon.GetComponent<Weapon>().dontUseAmmo)
		{
			switch (info.character)
			{
				case Character.YuzukiYukari:
					HUD.Instance.yukari.SetAmmo(info.currentWeapon.GetComponent<Weapon>().ammo);
					break;

				case Character.TsurumakiMaki:
					HUD.Instance.maki.SetAmmo(info.currentWeapon.GetComponent<Weapon>().ammo);
					break;
			}
		}
		

		if(isServer)
		{
			RpcTake(info.netId);
		}
	}

	[ClientRpc]
	private void RpcSetLayerRecursively()
	{
		SetLayerRecursively(mesh, LayerMask.LayerToName(gameObject.layer));
	}

	private void SetLayerRecursively(GameObject obj, string layerName)
	{
		if (obj == null) return;

		obj.layer = LayerMask.NameToLayer(layerName);

		foreach (Transform child in obj.transform)
		{
			if (child == null) continue;

			SetLayerRecursively(child.gameObject, layerName);
		}
	}

	public abstract void Attack();

	[ClientRpc]
	private void RpcTake(NetworkInstanceId netId)
	{
		if (isServer) return;

		Player = ClientScene.FindLocalObject(netId);
		Debug.Log(Player);

		if (Player == null) return;

		//if (Player.GetComponent<PlayerInfo>().currentWeapon != null) return;

		//if (Player.GetComponent<PlayerControl>().carrySomething) return;

		SetToSlot();

		//PlayerInfo info = Player.GetComponent<PlayerInfo>();
		PlayerAnimation anim = Player.GetComponent<PlayerAnimation>();

		// Set itself as player's current weapon and child
		//info.currentWeapon = this.gameObject;

		// Set player animation parameter
		anim.SetAnimationParameter("Weapon ID", (int)id);
	}

	private void SetToSlot()
	{
		Transform slot = Player.transform.Find("Character/Body/Arms/Right Arm/Weapon Slot");
		if (slot != null)
		{
			Debug.Log("SetToSlot : Slot is not null");
			transform.SetParent(slot);
			transform.localPosition = Vector3.zero;
			transform.localRotation = Quaternion.identity;
			Rigidbody rb = GetComponent<Rigidbody>();
			rb.isKinematic = true;

			// Turn off outline
			if (mesh != null) SetLayerRecursively(mesh, "Current Weapon");
		}
	}

	// ----------------------- syncvar hooks

	private void OnAmmo(int newAmmo)
	{
		ammo = newAmmo;

		if(Player != null)
		{
			switch(Player.GetComponent<PlayerInfo>().character)
			{
				case Character.YuzukiYukari:
					HUD.Instance.yukari.SetAmmo(newAmmo);
					break;

				case Character.TsurumakiMaki:
					HUD.Instance.maki.SetAmmo(newAmmo);
					break;
			}
		}
	}
}
