﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System;

public class Gun : Weapon {

	[Space]
	[Header("SE")]
	public AudioClip draw;
	public AudioClip fire;

	[Space]
	[Header("Gun Properties")]
	public float range;
	[Tooltip("Require to play muzzle flash")]
	public MuzzleFlash muzzleFlash;

	
    public int shots;
    public float angle;

    private float _cooldownTimer;

	private AudioSource _audio;

	private bool _dontPlayDraw;

    public override void Attack()
    {
		// Shoot if cooldown is over
		if (_cooldownTimer == cooldownTime)
        {
            // Start counting cooldown time
            _cooldownTimer = 0;

			if (ammo <= 0) return;

			// Start animation
			Player.GetComponent<PlayerAnimation>().SetAnimationParameter("Attack");

			// Play Muzzle flash
			if (isServer)
				RpcEmitMuzzleFlash();

            Transform origin = Player.GetComponent<PlayerControl>().ShootOrigin;
			
            for (int i = 0; i < shots; i++)
            {
                Vector3 dir = Quaternion.AngleAxis(-angle / 2 + angle / (shots + 1) * (i + 1), Vector3.up) * origin.forward;
                RaycastHit hit;

				Debug.Log(dir);
				Debug.Log(Player.transform.forward);

				int layerMask = (1 << LayerMask.NameToLayer("Zombie")) | (1 << LayerMask.NameToLayer("Attackable"));

                if (Physics.Raycast(origin.position, dir, out hit, range, layerMask))
                {
                    Attackable attackable = hit.collider.gameObject.GetComponent<Attackable>();
                    if (attackable != null)
                    {
						Vector3 impulse = attackable.transform.position - Player.transform.position;
						attackable.GetDamaged(Player, impulse.normalized, damage);
					}
                }
            }

			ammo -= 1;
		}
    }

    // Use this for initialization
    void Start ()
    {
        _cooldownTimer = cooldownTime;
		_audio = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (_cooldownTimer < cooldownTime)
        {
            _cooldownTimer += Time.deltaTime;
        }
        else
        {
            // Now we can shoot again
            _cooldownTimer = cooldownTime;
        }
    }

	public override void Take(GameObject player)
	{
		base.Take(player);

		if (player.GetComponent<PlayerInfo>().currentWeapon != this.gameObject) return;
		if (_dontPlayDraw) return;

		if (_audio != null)
		{
			if(draw != null)
			{
				_audio.clip = draw;
				_audio.Play();
			}
		}

		_dontPlayDraw = true;
	}

	public override void Drop()
	{
		base.Drop();

		_dontPlayDraw = false;
	}

	[Command]
	void CmdEmitMuzzleFlash()
	{
		RpcEmitMuzzleFlash();
	}

	[ClientRpc]
	void RpcEmitMuzzleFlash()
	{
		if (muzzleFlash != null) muzzleFlash.Emit();

		if (_audio != null)
		{
			if (fire != null)
			{
				_audio.clip = fire;
				_audio.Play();
			}
		}
	}
}
