﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System;
using System.Collections.Generic;

public class Melee : Weapon {

    public float range;             // z size
    public float width;             // x size
    public float swingTime;         // swing time

	[Header("SE")]
	public AudioClip[] swing;
	public AudioClip[] hit;

    protected MeleeHitBox _hitBox;
	protected AudioSource _audio;

    protected float _timer;

    // Use this for initialization
    void Start () {
        _timer = cooldownTime;
		_audio = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
	    if(_timer < cooldownTime)
        {
            _timer += Time.deltaTime;
        }
        else
        {
            // Now we can swing again and animation is end
            _timer = cooldownTime;
        }
	}

    public override void Attack()
    {
        // Swing if cooldown is over
        if(_timer == cooldownTime)
        {
            StartCoroutine(Swing());
        }
    }

    public override void Take(GameObject player)
    {
        base.Take(player);

		// Set hit box size
		if (Player == null) return;
		_hitBox = Player.GetComponent<PlayerControl>().MeleeHitBox;
        if(_hitBox != null) _hitBox.SetHitBoxSize(width, range);
    }

    private IEnumerator Swing()
    {
        // Start counting cooldown time
        _timer = 0;

        // Start animation
        Player.GetComponent<PlayerAnimation>().SetAnimationParameter("Attack");

		// Wait for swing time
		yield return new WaitForSeconds(swingTime);

		bool hit = false;

        // Decide attacked object and send message to host
        List<Attackable> attackables = _hitBox.GetAttackables();
        foreach (Attackable a in attackables)
        {
            Vector3 impulse = a.transform.position - Player.transform.position;
            a.GetDamaged(Player, impulse.normalized, damage);

			hit = true;
        }

		PlaySound(hit);
    }

	private void PlaySound(bool hit)
	{
		if (_audio == null) return;

		if(isServer)
			RpcPlaySound(hit);
	}

	[ClientRpc]
	private void RpcPlaySound(bool hit)
	{
		Debug.Log("RpcPlaySound");
		if(hit)
		{
			PlayHitSound();
		}
		else
		{
			PlaySwingSound();
		}
	}
	
	private void PlaySwingSound()
	{
		if(swing.Length > 0)
		{
			int rand = UnityEngine.Random.Range(0, swing.Length);
			_audio.clip = swing[rand];
			_audio.Play();
		}
	}
	
	private void PlayHitSound()
	{
		if (hit.Length > 0)
		{
			int rand = UnityEngine.Random.Range(0, hit.Length);
			_audio.clip = hit[rand];
			_audio.Play();
		}
	}
}
