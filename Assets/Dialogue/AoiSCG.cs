﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class AoiSCG : SCGObject {

	public Image body;
	public Image cloth;
	public Image blood1;
	public Image blood2;

	public bool enableBlood1;
	public bool enableBlood2;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public override void ChangeSCG(Sprite sprite)
	{
		body.sprite = sprite;
	}

	public override void Hide()
	{
		body.enabled = false;
		cloth.enabled = false;
		blood1.enabled = false;
		blood2.enabled = false;
	}

	public override void Show()
	{
		body.enabled = true;
		cloth.enabled = true;
		blood1.enabled = enableBlood1;
		blood2.enabled = enableBlood2;
	}
}
