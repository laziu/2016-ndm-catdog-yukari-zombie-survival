﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SCGManager : MonoBehaviour {

	#region Singleton
	private static SCGManager m_instance = null;
	public static SCGManager Instance
	{
		get
		{
			if (m_instance == null) Debug.LogError("SCGManager has not awaken.");
			return m_instance;
		}
	}

	void Awake()
	{
		if (m_instance != null)
		{
			Destroy(this.gameObject);
			return;
		}
		else
		{
			m_instance = this;
		}
	}
	#endregion

	public YukariSCG YukariSCG;
	public MakiSCG MakiSCG;
	public AkaneSCG AkaneSCG;
	public AoiSCG AoiSCG;

	public Image Black;

	// Use this for initialization
	void Start () {
		if (YukariSCG == null) Debug.LogError("SCGManager : Yukari SCG not found.");
		else YukariSCG.Hide();

		if (MakiSCG == null) Debug.LogError("SCGManager : Maki SCG not found.");
		else MakiSCG.Hide();

		if (AkaneSCG == null) Debug.LogError("SCGManager : Akane SCG not found.");
		else AkaneSCG.Hide();

		if (AoiSCG == null) Debug.LogError("SCGManager : Aoi SCG is not found.");
		else AoiSCG.Hide();

		if (Black == null) Debug.LogError("SCGManager : Black not found.");
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
