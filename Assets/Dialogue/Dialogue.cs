﻿using UnityEngine;
using System.Collections.Generic;
using EventCondition;
using System;

[Serializable]
public class Dialogue : MonoBehaviour {

	private Conditions m_conditions;
	private ActionNodes m_actionNodes;

	public bool IsActive { get; set; }

	// Use this for initialization
	void Start () {
		m_conditions = transform.FindChild("Conditions").GetComponent<Conditions>();
		if (m_conditions == null) Debug.LogError(transform.name + " : Conditions script is null.");

		m_actionNodes = transform.FindChild("Action Nodes").GetComponent<ActionNodes>();
		if (m_actionNodes == null) Debug.LogError(transform.name + " : Action Nodes script is null.");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	/// <summary>
	/// Call ActionNodes.StartAction()
	/// </summary>
	public void Activate()
	{
		IsActive = true;
		m_actionNodes.Parent = this;
		m_actionNodes.StartAction();
	}
}
