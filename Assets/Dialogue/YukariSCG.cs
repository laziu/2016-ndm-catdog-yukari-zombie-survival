﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class YukariSCG : SCGObject {

	public Image body;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public override void ChangeSCG(Sprite sprite)
	{
		body.sprite = sprite;
	}

	public override void Hide()
	{
		body.enabled = false;
	}

	public override void Show()
	{
		body.enabled = true;
	}
}
