﻿using UnityEngine;
using System.Collections;
using Action;
using System;

public class HideSCG : BaseAction
{
	[SerializeField]
	private Character m_character;

	public Character Character { get { return m_character; } set { m_character = value; } }

	public override ActionType Type
	{
		get
		{
			return ActionType.HideSCG;
		}
	}

	protected override void VirtualUpdate()
	{
		switch (m_character)
		{
			case Character.YuzukiYukari:
				SCGManager.Instance.YukariSCG.Hide();
				break;

			case Character.TsurumakiMaki:
				SCGManager.Instance.MakiSCG.Hide();
				break;

			case Character.KotonohaAkane:
				SCGManager.Instance.AkaneSCG.Hide();
				break;

			case Character.KotonohaAoi:
				SCGManager.Instance.AoiSCG.Hide();
				break;
		}

		CallNextActionNode();
	}
}
