﻿using UnityEngine;
using System.Collections;
using Action;
using System;

namespace Action
{
	public class DebugLog : BaseAction
	{

		[SerializeField]
		private string m_text;

		public override ActionType Type { get { return ActionType.DebugLog; } }
		public string Text { get { return m_text; } set { m_text = value; } }

		// Use this for initialization
		void Start()
		{

		}

		protected override void VirtualUpdate()
		{
			Debug.Log(Text);
			CallNextActionNode();
		}
	}
}
