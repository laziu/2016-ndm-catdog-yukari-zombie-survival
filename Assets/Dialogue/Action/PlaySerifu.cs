﻿using UnityEngine;
using System.Collections;
using Action;
using System;

public class PlaySerifu : BaseAction
{
	[SerializeField]
	private Character m_character;
	[SerializeField]
	private AudioClip m_serifu;
	[SerializeField]
	private string m_subtitle;
	[SerializeField]
	private bool m_wait = true;

	private bool m_played = false;

	public override ActionType Type
	{
		get
		{
			return ActionType.PlaySerifu;
		}
	}

	public Character Character { get { return m_character; } set { m_character = value; } }
	public AudioClip Serifu { get { return m_serifu; } set { m_serifu = value; } }
	public string Subtitle { get { return m_subtitle; } set { m_subtitle = value; } }
	public bool Wait { get { return m_wait; } set { m_wait = value; } }

	protected override void VirtualUpdate()
	{
		bool playing = false;
		
		// Play serifu at first
		if(!m_played)
		{
			switch (m_character)
			{
				case Character.YuzukiYukari:
					SerifuManager.Instance.PlaySerifu(this);
					break;

				case Character.TsurumakiMaki:
					SerifuManager.Instance.PlaySerifu(this);
					break;

				case Character.KotonohaAkane:
					SerifuManager.Instance.PlaySerifu(this);
					break;

				case Character.KotonohaAoi:
					SerifuManager.Instance.PlaySerifu(this);
					break;
			}

			// Request to print subtitle
			if(!Subtitle.Equals("")) SubtitleManager.Instance.PrintSubtitle(this);

			m_played = true;
		}

		// Wait until the serifu is over if the Wait option is selected
		if(m_wait)
		{
			// Is my serifu playing?
			playing = SerifuManager.Instance.IsPlaying(this);
		}

		if (m_played && !playing)
		{
			CallNextActionNode();
		}
	}
}
