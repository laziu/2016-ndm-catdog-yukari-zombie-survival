﻿using UnityEngine;
using System.Collections;
using Action;
using System;

public class ShowSCG : BaseAction
{
	[SerializeField]
	private Character m_character;

	public Character Character { get { return m_character; } set { m_character = value; } }

	public override ActionType Type
	{
		get
		{
			return ActionType.ShowSCG;
		}
	}

	protected override void VirtualUpdate()
	{
		switch(m_character)
		{
			case Character.YuzukiYukari:
				SCGManager.Instance.YukariSCG.Show();
				break;

			case Character.TsurumakiMaki:
				SCGManager.Instance.MakiSCG.Show();
				break;

			case Character.KotonohaAkane:
				SCGManager.Instance.AkaneSCG.Show();
				break;

			case Character.KotonohaAoi:
				SCGManager.Instance.AoiSCG.Show();
				break;
		}

		CallNextActionNode();
	}
}
