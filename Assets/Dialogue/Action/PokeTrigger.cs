﻿using UnityEngine;
using System.Collections;
using Action;
using System;

public class PokeTrigger : BaseAction {

    [SerializeField]
    private TriggerBehaviour _trigger;

    public TriggerBehaviour Trigger { get { return _trigger; } set { _trigger = value; } }

    public override ActionType Type
    {
        get
        {
            return ActionType.PokeTrigger;
        }
    }

    protected override void VirtualUpdate()
    {
        Trigger.Poke();
        CallNextActionNode();
    }
}
