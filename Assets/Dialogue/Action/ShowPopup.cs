﻿using UnityEngine;
using System.Collections;
using Action;
using System;

public enum PopupOption
{
	Ctrl,
	Shift,
	Z,
	ArrowAlt
}

public class ShowPopup : BaseAction {

	[SerializeField]
	private PopupOption _option;
	
	public PopupOption Option { get { return _option; } set { _option = value; } }

	public override ActionType Type
	{
		get
		{
			return ActionType.ShowPopup;
		}
	}

	protected override void VirtualUpdate()
	{
		Debug.Log("Show Popup");
		switch(_option)
		{
			case PopupOption.Ctrl:
				HUD.Instance.popup.ShowCtrl();
				break;

			case PopupOption.Shift:
				HUD.Instance.popup.ShowShift();
				break;

			case PopupOption.Z:
				HUD.Instance.popup.ShowZ();
				break;

			case PopupOption.ArrowAlt:
				Debug.Log("Show Arrow Alt");
				HUD.Instance.popup.ShowArrowAlt();
				break;

			default:
				break;
		}

		CallNextActionNode();
	}
}
