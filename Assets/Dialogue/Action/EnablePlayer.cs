﻿using UnityEngine;
using System.Collections;
using Action;
using System;

public class EnablePlayer : BaseAction {

	[SerializeField]
	private bool _enable;

	public bool Enable { get { return _enable; } set { _enable = value; } }

	public override ActionType Type
	{
		get
		{
			return ActionType.EnablePlayer;
		}
	}

	protected override void VirtualUpdate()
	{
		GameObject p1 = EventObserver.Instance.GetPlayerReference(Character.YuzukiYukari);
		GameObject p2 = EventObserver.Instance.GetPlayerReference(Character.TsurumakiMaki);
		//GameObject p3 = EventObserver.Instance.GetPlayerReference(Character.KotonohaAkane);
		//GameObject p4 = EventObserver.Instance.GetPlayerReference(Character.KotonohaAoi);

		PlayerControl c1 = null;
		PlayerControl c2 = null;
		//PlayerControl c3 = null;
		//PlayerControl c4 = null;
		if (p1 != null) c1 = p1.GetComponent<PlayerControl>();
		if (p2 != null) c2 = p2.GetComponent<PlayerControl>();
		//if (p3 != null) c3 = p3.GetComponent<PlayerControl>();
		//if (p4 != null) c4 = p4.GetComponent<PlayerControl>();

		if (c1 != null && c1.isLocalPlayer) c1.canMove = true;
		else if (c2 != null && c2.isLocalPlayer) c2.canMove = true;
		//else if (c3 != null && c3.isLocalPlayer) c3.canMove = true;
		//else if (c4 != null && c4.isLocalPlayer) c4.canMove = true;

		CallNextActionNode();
	}
}
