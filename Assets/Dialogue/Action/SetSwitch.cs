﻿using UnityEngine;
using System.Collections;
using Action;
using System;

public class SetSwitch : BaseAction {

	[SerializeField]
	private int _index;
	[SerializeField]
	private bool _state;

	public int Index { get { return _index; } set { _index = value; } }
	public bool State { get { return _state; } set { _state = value; } }

	public override ActionType Type
	{
		get
		{
			return ActionType.SetSwitch;
		}
	}

	protected override void VirtualUpdate()
	{
		EventObserver.Instance.SwitchManager.SetSwitch(_index, _state);
		CallNextActionNode();
	}
}
