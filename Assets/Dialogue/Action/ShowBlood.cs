﻿using UnityEngine;
using System.Collections;
using Action;
using System;

public class ShowBlood : BaseAction {
	
	public Character character;
	public int blood;
	public bool enable;

	public override ActionType Type
	{
		get
		{
			return ActionType.ShowBlood;
		}
	}

	protected override void VirtualUpdate()
	{
		switch (character)
		{
			case Character.YuzukiYukari:
				
				break;

			case Character.TsurumakiMaki:
				
				break;

			case Character.KotonohaAkane:
				if(blood == 0)
					SCGManager.Instance.AkaneSCG.enableBlood1 = enable;
				else
					SCGManager.Instance.AkaneSCG.enableBlood2 = enable;
				break;

			case Character.KotonohaAoi:
				if (blood == 0)
					SCGManager.Instance.AoiSCG.enableBlood1 = enable;
				else
					SCGManager.Instance.AoiSCG.enableBlood2 = enable;
				break;
		}

		CallNextActionNode();
	}
}
