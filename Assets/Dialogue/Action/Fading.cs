﻿using UnityEngine;
using System.Collections;
using Action;
using System;
using UnityEngine.UI;

public enum FadingOption
{
	FadeIn,
	FadeOut
}

public class Fading : BaseAction {

	[SerializeField]
	private FadingOption option;
	[SerializeField]
	private float time;

	private float _timer;
	private Image _black;

	public FadingOption Option { get { return option; } set { option = value; } }
	public float FadingTime { get { return time; } set { time = value; } }
	
	public override ActionType Type
	{
		get
		{
			return ActionType.Fading;
		}
	}

	protected override void VirtualStart()
	{
		_timer = 0f;
		_black = SCGManager.Instance.Black;
	}

	protected override void VirtualUpdate()
	{
		if (time == 0f) return;

		_timer += Time.deltaTime;

		if (_timer > time) _timer = time;

		Color c = new Color();
		switch (Option)
		{
			case FadingOption.FadeIn:
				c = _black.color;
				c.a = 1 - (_timer / time);
				_black.color = c;
				break;

			case FadingOption.FadeOut:
				c = _black.color;
				c.a = _timer / time;
				_black.color = c;
				break;
		}

		if (_timer == time) CallNextActionNode();
	}
}
