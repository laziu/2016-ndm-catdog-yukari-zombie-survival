﻿using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// Collection of Dialogue Action Nodes
/// </summary>
public enum ActionType
{
    DebugLog,
    Timer,
    PlaySerifu,
    ShowSCG,
    HideSCG,
    ChangeSCG,
    SetSwitch,
    PokeTrigger,
	Fading,
	MoveCamera,
	ShowPopup,
	EnablePlayer,
	ShowBlood,
	AddTime
}

namespace Action
{
	/// <summary>
	/// Base class of dialogue actions
	/// It has VirtualStart and VirtualUpdate to replace Start and Update logic
	/// </summary>
	[Serializable]
	public abstract class BaseAction : MonoBehaviour
	{
		[SerializeField]
		private BaseAction m_nextActionNode;
		[SerializeField]
		private bool m_active;

		public abstract ActionType Type { get; }
		public BaseAction NextActionNode { get { return m_nextActionNode; } set { m_nextActionNode = value; } }
		public bool Active { get { return m_active; } set { m_active = value; } }

		// ActionNodes
		public ActionNodes Parent { get; set; }

		void Start()
		{
			VirtualStart();
		}

		protected void CallNextActionNode()
		{
			this.Active = false;
			if (NextActionNode != null)
			{
				// Activate next node and send parent info
				NextActionNode.Active = true;
				NextActionNode.Parent = Parent;
			}
			// It is end point
			else
			{
				Parent.Done();
			}
		}

		void Update()
		{
			if (Active) VirtualUpdate();
		}

		/// <summary>
		/// Use this function by overriding it instead of Start()
		/// </summary>
		protected virtual void VirtualStart() { }

		/// <summary>
		/// Use this function by overriding it instead of Update()
		/// It has to contain the CallNextActionNode()
		/// </summary>
		protected abstract void VirtualUpdate();
	}
}
