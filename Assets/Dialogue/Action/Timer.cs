﻿using UnityEngine;
using System.Collections;
using System;

namespace Action
{
	public class Timer : BaseAction
	{
		[SerializeField]
		private float m_time;
		[SerializeField]
		private float m_limit;

		public override ActionType Type
		{
			get
			{
				return ActionType.Timer;
			}
		}
		public float TimeLimit { get { return m_limit; } set { m_limit = value; } }

		protected override void VirtualUpdate()
		{
			if (m_time >= m_limit)
			{
				m_time = 0;
				CallNextActionNode();
			}
			else
			{
				m_time += Time.deltaTime;
			}
		}
	}
}
