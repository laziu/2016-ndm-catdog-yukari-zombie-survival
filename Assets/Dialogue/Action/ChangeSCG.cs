﻿using UnityEngine;
using System.Collections;
using Action;
using System;

public class ChangeSCG : BaseAction
{
	[SerializeField]
	private Character m_character;

	[SerializeField]
	private int m_spriteIndex;

	private Sprite m_sprite;

	public Character Character { get { return m_character; } set { m_character = value; } }
	public int SpriteIndex { get { return m_spriteIndex; } set { m_spriteIndex = value; } }

	public override ActionType Type
	{
		get
		{
			return ActionType.ChangeSCG;
		}
	}

	protected override void VirtualStart()
	{
		switch(Character)
		{
			case Character.YuzukiYukari:
				m_sprite = Resources.Load<Sprite>(SCGPath.YukariSCGPath[SpriteIndex]);
				break;

			case Character.TsurumakiMaki:
				m_sprite = Resources.Load<Sprite>(SCGPath.MakiSCGPath[SpriteIndex]);
				break;

			case Character.KotonohaAkane:
				m_sprite = Resources.Load<Sprite>(SCGPath.AkaneSCGPath[SpriteIndex]);
				break;

			case Character.KotonohaAoi:
				m_sprite = Resources.Load<Sprite>(SCGPath.AoiSCGPath[SpriteIndex]);
				break;
		}
	}

	protected override void VirtualUpdate()
	{
		switch (Character)
		{
			case Character.YuzukiYukari:
				SCGManager.Instance.YukariSCG.ChangeSCG(m_sprite);
				break;

			case Character.TsurumakiMaki:
				SCGManager.Instance.MakiSCG.ChangeSCG(m_sprite);
				break;

			case Character.KotonohaAkane:
				SCGManager.Instance.AkaneSCG.ChangeSCG(m_sprite);
				break;

			case Character.KotonohaAoi:
				SCGManager.Instance.AoiSCG.ChangeSCG(m_sprite);
				break;
		}

		CallNextActionNode();
	}
}
