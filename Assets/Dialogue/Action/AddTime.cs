﻿using UnityEngine;
using System.Collections;
using Action;
using System;

public class AddTime : BaseAction {

	[SerializeField]
	private int _seconds;

	public int Seconds { get { return _seconds; } set { _seconds = value; } }

	public override ActionType Type
	{
		get
		{
			return ActionType.AddTime;
		}
	}

	protected override void VirtualUpdate()
	{
		CampaignTimer.Instance.AddTime(_seconds);

		CallNextActionNode();
	}
}
