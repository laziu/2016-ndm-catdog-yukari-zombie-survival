﻿using UnityEngine;
using System.Collections;
using Action;
using System;

public class MoveCamera : BaseAction {

	[SerializeField]
	private Transform _destination;
	[SerializeField]
	private float _zoom;
	[SerializeField]
	private float _timeLength;
	[SerializeField]
	private bool _elastic;

	public Transform Destination { get { return _destination; } set { _destination = value; } }
	public float Zoom { get { return _zoom; } set { _zoom = value; } }
	public float TimeLength { get { return _timeLength; } set { _timeLength = value; } }
	public bool Elastic { get { return _elastic; } set { _elastic = value; } }

	public override ActionType Type
	{
		get
		{
			return ActionType.MoveCamera;
		}
	}

	protected override void VirtualUpdate()
	{
		if (_destination != null)
		{
			QuarterView.Instance.MoveTo(_destination);
		}
		else
		{
			GameObject p1 = EventObserver.Instance.GetPlayerReference(Character.YuzukiYukari);
			GameObject p2 = EventObserver.Instance.GetPlayerReference(Character.TsurumakiMaki);
			GameObject p3 = EventObserver.Instance.GetPlayerReference(Character.KotonohaAkane);
			GameObject p4 = EventObserver.Instance.GetPlayerReference(Character.KotonohaAoi);

			Transform t = null;

			if (p1.GetComponent<PlayerControl>().isLocalPlayer)
				t = p1.transform;
			else if (p2.GetComponent<PlayerControl>().isLocalPlayer)
				t = p2.transform;
			else if (p3.GetComponent<PlayerControl>().isLocalPlayer)
				t = p3.transform;
			else if (p4.GetComponent<PlayerControl>().isLocalPlayer)
				t = p4.transform;

			QuarterView.Instance.MoveTo(t);
		}
		QuarterView.Instance.Zoom(_zoom);
		QuarterView.Instance.timeLength = _timeLength;
		QuarterView.Instance.elastic = _elastic;

		CallNextActionNode();
	}
}
