﻿using UnityEngine;
using System.Collections;

public abstract class SCGObject : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	/// <summary>
	/// Enable images
	/// </summary>
	public abstract void Show();

	/// <summary>
	/// Disable images
	/// </summary>
	public abstract void Hide();

	/// <summary>
	/// Change SCG
	/// </summary>
	/// <param name="spriteIndex">New sprite</param>
	public abstract void ChangeSCG(Sprite sprite);
}
