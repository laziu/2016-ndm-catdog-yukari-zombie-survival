﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Camera))]
public class SerifuManager : MonoBehaviour {

	#region Singleton
	private static SerifuManager m_instance = null;
	public static SerifuManager Instance
	{
		get
		{
			if (m_instance == null) Debug.LogError("SerifuManager has not awaken.");
			return m_instance;
		}
	}

	void Awake()
	{
		if (m_instance != null)
		{
			Destroy(this.gameObject);
			return;
		}
		else
		{
			m_instance = this;
		}
	}
	#endregion

	// Audio Source components of main camera
	public AudioSource yukariSpeaker;
	public AudioSource makiSpeaker;
	public AudioSource akaneSpeaker;
	public AudioSource aoiSpeaker;

	private PlaySerifu m_currentYukariSerifu = null;
	private PlaySerifu m_currentMakiSerifu = null;
	private PlaySerifu m_currentAkaneSerifu = null;
	private PlaySerifu m_currentAoiSerifu = null;

	#region Properties
	public bool YukariSerifuIsPlaying { get { return yukariSpeaker.isPlaying; } }
	public bool MakiSerifuIsPlaying { get { return makiSpeaker.isPlaying; } }
	public bool AkaneSerifuIsPlaying { get { return akaneSpeaker.isPlaying; } }
	public bool AoiSerifuIsPlaying { get { return aoiSpeaker.isPlaying; } }
	#endregion

	public bool IsPlaying(PlaySerifu requester)
	{
		bool result = false;

		switch (requester.Character)
		{
			case Character.YuzukiYukari:
				result = m_currentYukariSerifu == requester && YukariSerifuIsPlaying;
				break;

			case Character.TsurumakiMaki:
				result = m_currentMakiSerifu == requester && MakiSerifuIsPlaying;
				break;

			case Character.KotonohaAkane:
				result = m_currentAkaneSerifu == requester && AkaneSerifuIsPlaying;
				break;

			case Character.KotonohaAoi:
				result = m_currentAoiSerifu == requester && AoiSerifuIsPlaying;
				break;
		}

		return result;
	}


	// Use this for initialization
	void Start () {
		if (yukariSpeaker == null) Debug.LogError("Ykari Speaker is null.");
		if (makiSpeaker == null) Debug.LogError("Maki Speaker is null.");
		if (akaneSpeaker == null) Debug.LogError("Akane Speaker is null.");
		if (aoiSpeaker == null) Debug.LogError("Aoi Speaker is null.");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	///////////////////////////////////////////////////////////////////////
	// Play Serifu, Stop Serifu methods
	// if current serifu is playing, cancel it and play requested new serifu
	///////////////////////////////////////////////////////////////////////

	public void PlaySerifu(PlaySerifu requester)
	{
		// Stop current serifu anyway
		StopSerifu(requester);

		// Play requested serifu
		// Update current serifu buffer
		switch(requester.Character)
		{
			case Character.YuzukiYukari:
				yukariSpeaker.clip = requester.Serifu;
				yukariSpeaker.Play();
				m_currentYukariSerifu = requester;
				break;

			case Character.TsurumakiMaki:
				makiSpeaker.clip = requester.Serifu;
				makiSpeaker.Play();
				m_currentMakiSerifu = requester;
				break;

			case Character.KotonohaAkane:
				akaneSpeaker.clip = requester.Serifu;
				akaneSpeaker.Play();
				m_currentAkaneSerifu = requester;
				break;

			case Character.KotonohaAoi:
				aoiSpeaker.clip = requester.Serifu;
				aoiSpeaker.Play();
				m_currentAoiSerifu = requester;
				break;
		}
	}

	public void StopSerifu(PlaySerifu requester)
	{
		// Stop current serifu
		// Clear current serifu buffer
		switch (requester.Character)
		{
			case Character.YuzukiYukari:
				if (YukariSerifuIsPlaying) yukariSpeaker.Stop();
				m_currentYukariSerifu = null;
				break;

			case Character.TsurumakiMaki:
				if (MakiSerifuIsPlaying) makiSpeaker.Stop();
				m_currentMakiSerifu = null;
				break;

			case Character.KotonohaAkane:
				if (AkaneSerifuIsPlaying) akaneSpeaker.Stop();
				m_currentAkaneSerifu = null;
				break;

			case Character.KotonohaAoi:
				if (AoiSerifuIsPlaying) aoiSpeaker.Stop();
				m_currentAoiSerifu = null;
				break;
		}
	}
}
