﻿using UnityEngine;
using System.Collections;
using EventCondition;
using System;

public class PlayTime : BaseEventCondition {

    [SerializeField]
    private float _minutes;

    public float Minutes { get { return _minutes; } set { _minutes = value; } }

    public override EventType Type
    {
        get
        {
            return EventType.PlayTime;
        }
    }

    protected override void VirtualStart()
    {
        
    }
}
