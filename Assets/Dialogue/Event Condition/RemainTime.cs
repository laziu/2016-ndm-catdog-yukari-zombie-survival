﻿using UnityEngine;
using System.Collections;
using EventCondition;
using System;

public class RemainTime : BaseEventCondition {

	[SerializeField]
	private int _seconds;

	public int Seconds { get { return _seconds; } set { _seconds = value; } }

	public override EventType Type
	{
		get
		{
			return EventType.RemainTime;
		}
	}

	protected override void VirtualStart()
	{
		
	}
}
