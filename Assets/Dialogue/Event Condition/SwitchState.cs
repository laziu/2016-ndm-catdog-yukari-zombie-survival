﻿using UnityEngine;
using System.Collections;
using EventCondition;
using System;

public class SwitchState : BaseEventCondition {

	[SerializeField]
	private int _index;
	[SerializeField]
	private bool _state;

	public int Index { get { return _index; } set { _index = value; } }
	public bool State { get { return _state; } set { _state = value; } }

	public override EventType Type
	{
		get
		{
			return EventType.SwitchState;
		}
	}

	protected override void VirtualStart()
	{
		
	}
}
