﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System;

/// <summary>
/// Events that Event Observer is watching
/// </summary>
public enum EventType
{
	Unconditional,
    LocationTouch,
    TotalZombieKillCount,
    PlayTime,
    SwitchState,
	PlayerDie,
	RemainTime
}

namespace EventCondition
{
	[Serializable]
	public abstract class BaseEventCondition : NetworkBehaviour
	{
		[SerializeField]
		private Conditions _parent;
		[SerializeField]
		private bool _satisfies;

		#region Properties
		public abstract EventType Type { get; }
		public Conditions Parent { get { return _parent; } set { _parent = value; } }
		public bool Satisfies { get { return _satisfies; } set { _satisfies = value; } }
		#endregion

		bool start;

		protected abstract void VirtualStart();

		void Start()
		{
			Transform myParent = transform.parent;
			if (myParent == null) Debug.LogError(name + " : It has to be child of Dialogue/Conditions.");
			else Parent = myParent.GetComponent<Conditions>();

			VirtualStart();

			StartCoroutine(Wait());
		}

        [ServerCallback]
		void Update()
		{
			if (!start) return;

			if (Parent == null) return;

			if (!(Parent.PlayOnce && Parent.Played))
			{
				if (EventObserver.Instance.CheckEventCall(this))
				{
					RpcConditionSatisfy();
				}
				else
				{
					RpcConditionNotSatisfy();
				}
			}
		}

        [ClientRpc]
        private void RpcConditionSatisfy()
        {
            Satisfies = true;
            Parent.SetDirty();
        }

        [ClientRpc]
        private void RpcConditionNotSatisfy()
        {
            Satisfies = false;
        }

		IEnumerator Wait()
		{
			yield return new WaitForSeconds(2f);

			start = true;
			Debug.Log("BaseEventCondition : Event Check Start");
		}
	}
}
