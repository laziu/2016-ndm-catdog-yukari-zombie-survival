﻿using UnityEngine;
using EventCondition;
using System.Collections;
using System;

public class PlayerDie : BaseEventCondition {

	[SerializeField]
	private Character _character;

	public Character Character { get { return _character; } set { _character = value; } }

	public override EventType Type
	{
		get
		{
			return EventType.PlayerDie;
		}
	}

	protected override void VirtualStart()
	{
		
	}
}
