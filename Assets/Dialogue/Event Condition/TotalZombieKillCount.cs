﻿using UnityEngine;
using System.Collections;
using EventCondition;
using System;

public class TotalZombieKillCount : BaseEventCondition {

	[SerializeField]
	private int _count;

	public int Count { get { return _count; } set { _count = value; } }

	public override EventType Type
	{
		get
		{
			return EventType.TotalZombieKillCount;
		}
	}

	protected override void VirtualStart()
	{
		
	}
}
