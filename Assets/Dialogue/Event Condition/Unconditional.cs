﻿using UnityEngine;
using System.Collections;
using EventCondition;
using System;

public class Unconditional : BaseEventCondition {

	public override EventType Type
	{
		get
		{
			return EventType.Unconditional;
		}
	}

	protected override void VirtualStart()
	{
		
	}
}
