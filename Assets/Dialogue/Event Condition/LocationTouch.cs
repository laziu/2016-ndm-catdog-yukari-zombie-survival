﻿using UnityEngine;
using System.Collections;
using System;

public enum LocationTouchOption
{
    Player,
    Object,
	Zombie
}

namespace EventCondition
{
	public class LocationTouch : BaseEventCondition
	{
        [SerializeField]
        private LocationTouchOption _option;
        [SerializeField]
        private Character _character;
		[SerializeField]
		private Location _location;
		[SerializeField]
		private GameObject _subject;

		#region Properties
		public override EventType Type { get { return EventType.LocationTouch; } }
        public LocationTouchOption Option { get { return _option; } set { _option = value; } }
        public Character Character { get { return _character; } set { _character = value; } }
		public Location Location { get { return _location; } set { _location = value; } }
		public GameObject Subject { get { return _subject; } set { _subject = value; } }
		#endregion

		protected override void VirtualStart()
		{
			if (Location == null) Debug.LogError(name + " : Location is null.");
			if(_option == LocationTouchOption.Object && Subject == null) Debug.LogError(name + " : Subject is null.");
		}
	}
}
