﻿using UnityEngine;
using System.Collections.Generic;

public class DialogueManager : MonoBehaviour {

	#region Singleton
	private static DialogueManager m_instance = null;
	public static DialogueManager Instance
	{
		get
		{
			if (m_instance == null) Debug.LogError("DialogueManager has not awaken.");
			return m_instance;
		}
	}

	void Awake()
	{
		if (m_instance != null)
		{
			Destroy(this.gameObject);
			return;
		}
		else
		{
			m_instance = this;
		}
	}
	#endregion

	private Queue<Dialogue> m_dialogueQueue = new Queue<Dialogue>();
	private Dialogue m_activeDialogue;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(m_dialogueQueue.Count > 0)
		{
			if(!m_activeDialogue.IsActive)
			{
				// Dequeue and activate the next dialogue
				m_activeDialogue = m_dialogueQueue.Dequeue();
				m_activeDialogue.Activate();
			}
		}
	}

	public void WaitToStart(Dialogue dialogue)
	{
		if (m_activeDialogue == null)
		{
			m_activeDialogue = dialogue;
			m_activeDialogue.Activate();
		}
		else
		{
			m_dialogueQueue.Enqueue(dialogue);
		}
	}
}
