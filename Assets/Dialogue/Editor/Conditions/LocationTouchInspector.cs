﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using EventCondition;

[CustomEditor(typeof(LocationTouch))]
public class LocationTouchInspector : Editor {
    
	public override void OnInspectorGUI()
	{
		LocationTouch myTarget = (LocationTouch)target;

		GUILayout.BeginVertical();

        myTarget.Option = (LocationTouchOption)EditorGUILayout.EnumPopup(myTarget.Option, GUILayout.Width(100));

        switch(myTarget.Option)
        {
            case LocationTouchOption.Player:
                GUILayout.BeginHorizontal();
                myTarget.Location = (Location)EditorGUILayout.ObjectField(myTarget.Location, typeof(Location), true, GUILayout.Width(120));
                EditorGUILayout.LabelField("에 ", GUILayout.Width(30));
                GUILayout.EndHorizontal();

                GUILayout.BeginHorizontal();
                myTarget.Character = (Character)EditorGUILayout.EnumPopup(myTarget.Character, GUILayout.Width(110));
                EditorGUILayout.LabelField("(이)가 닿았을 때");
                GUILayout.EndHorizontal();
                break;

            case LocationTouchOption.Object:
                GUILayout.BeginHorizontal();
                myTarget.Location = (Location)EditorGUILayout.ObjectField(myTarget.Location, typeof(Location), true, GUILayout.Width(120));
                EditorGUILayout.LabelField("에 ", GUILayout.Width(30));
                GUILayout.EndHorizontal();

                GUILayout.BeginHorizontal();
                myTarget.Subject = (GameObject)EditorGUILayout.ObjectField(myTarget.Subject, typeof(GameObject), true, GUILayout.Width(160));
                EditorGUILayout.LabelField("(이)가 닿았을 때");
                GUILayout.EndHorizontal();
                break;

			case LocationTouchOption.Zombie:
				GUILayout.BeginHorizontal();
				myTarget.Location = (Location)EditorGUILayout.ObjectField(myTarget.Location, typeof(Location), true, GUILayout.Width(120));
				EditorGUILayout.LabelField("에 좀비가 닿았을 때");
				GUILayout.EndHorizontal();
				break;
		}
		GUILayout.EndVertical();
	}
}
