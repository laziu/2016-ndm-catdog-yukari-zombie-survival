﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using EventCondition;

[CustomEditor(typeof(Conditions))]
public class DialogueConditionsInspector : Editor {

	private EventType m_selectedEventType;

	public override void OnInspectorGUI()
	{
		Conditions myTarget = (Conditions)target;

		// Play Once checkbox
		myTarget.PlayOnce = EditorGUILayout.Toggle("Play Once", myTarget.PlayOnce);

		EditorGUILayout.Space();

		// Add Condition Button
		GUILayout.BeginHorizontal();
		m_selectedEventType = (EventType)EditorGUILayout.EnumPopup(m_selectedEventType, GUILayout.Width(100));

		if(GUILayout.Button("Add Condition"))
		{
			CreateNewCondition(myTarget);
		}
		GUILayout.EndHorizontal();
	}

	private void CreateNewCondition(Conditions myTarget)
	{
		
		GameObject go = new GameObject(SelectName());
		go.transform.parent = myTarget.transform;

		switch (m_selectedEventType)
		{
			case EventType.Unconditional:
				go.AddComponent<Unconditional>();
				break;

			case EventType.LocationTouch:
				go.AddComponent<LocationTouch>();
				break;

			case EventType.TotalZombieKillCount:
				go.AddComponent<TotalZombieKillCount>();
				break;

			case EventType.PlayTime:
                go.AddComponent<PlayTime>();
				break;

			case EventType.SwitchState:
				go.AddComponent<SwitchState>();
				break;

			case EventType.PlayerDie:
				go.AddComponent<PlayerDie>();
				break;

			case EventType.RemainTime:
				go.AddComponent<RemainTime>();
				break;
		}
	}

	// Set name of new EventCondition
	private string SelectName()
	{
		string namePrefix = "";

		switch (m_selectedEventType)
		{
			case EventType.Unconditional:
				namePrefix = "Uncondtional ";
				break;

			case EventType.LocationTouch:
				namePrefix = "Location Touch ";
				break;

			case EventType.TotalZombieKillCount:
				namePrefix = "Total Zombie Kill Count ";
				break;

			case EventType.PlayTime:
				namePrefix = "Play Time ";
				break;

			case EventType.SwitchState:
				namePrefix = "Check Switch ";
				break;

			case EventType.PlayerDie:
				namePrefix = "Player Die ";
				break;

			case EventType.RemainTime:
				namePrefix = "Remain Time ";
				break;
		}

		int count = 1;
		string name = namePrefix + count;

		BaseEventCondition[] ecs = FindObjectsOfType<BaseEventCondition>();
		for (int i = 0; i < ecs.Length; ++i)
		{
			name = namePrefix + count;
			foreach (BaseEventCondition ec in ecs)
			{
				if (ec.name.Equals(name))
				{
					count++;
					break;
				}
			}
		}

		return namePrefix + count;
	}
}
