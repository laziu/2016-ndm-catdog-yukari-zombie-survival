﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(SwitchState))]
public class SwitchStateInspector : Editor {

	private string[] _switchOptions = new string[SwitchManager.SWITCH_SIZE];
	private string[] _stateOptions = new string[2] { "On", "Off" };
	private int _selected;

	public override void OnInspectorGUI()
	{
		SwitchState myTarget = (SwitchState)target;
		for(int i=0; i<SwitchManager.SWITCH_SIZE; ++i)
		{
			_switchOptions[i] = "Switch " + i;
		}

		GUILayout.BeginVertical();

		GUILayout.BeginHorizontal();
		myTarget.Index = EditorGUILayout.Popup(myTarget.Index, _switchOptions, GUILayout.Width(80));
		EditorGUILayout.LabelField("가 ", GUILayout.Width(20));
		_selected = myTarget.State ? 0 : 1;
		_selected = EditorGUILayout.Popup(_selected, _stateOptions, GUILayout.Width(60));
		myTarget.State = _selected == 0 ? true : false;
		EditorGUILayout.LabelField("이면", GUILayout.Width(30));
		GUILayout.EndHorizontal();

		GUILayout.EndVertical();
	}
}
