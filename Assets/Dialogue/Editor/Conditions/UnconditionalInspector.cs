﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(Unconditional))]
public class UnconditionalInspector : Editor {

	public override void OnInspectorGUI()
	{
		EditorGUILayout.LabelField("무조건");
	}
}
