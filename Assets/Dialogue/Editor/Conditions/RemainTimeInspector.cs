﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(RemainTime))]
public class RemainTimeInspector : Editor {

	public override void OnInspectorGUI()
	{
		RemainTime myTarget = (RemainTime)target;

		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField("캠페인 타이머가 ", GUILayout.Width(80));
		myTarget.Seconds = EditorGUILayout.IntField(myTarget.Seconds, GUILayout.Width(50));
		EditorGUILayout.LabelField("초 남았을 때", GUILayout.Width(70));
		EditorGUILayout.EndHorizontal();
	}
}
