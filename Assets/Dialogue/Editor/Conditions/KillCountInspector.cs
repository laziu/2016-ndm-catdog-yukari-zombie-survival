﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(TotalZombieKillCount))]
public class KillCountInspector : Editor {

	public override void OnInspectorGUI()
	{
		TotalZombieKillCount myTarget = (TotalZombieKillCount)target;

		GUILayout.BeginHorizontal();
		EditorGUILayout.LabelField("좀비를", GUILayout.Width(50));
		myTarget.Count = EditorGUILayout.IntField(myTarget.Count, GUILayout.Width(50));
		EditorGUILayout.LabelField("마리 죽이면", GUILayout.Width(70));
		GUILayout.EndHorizontal();
	}
}
