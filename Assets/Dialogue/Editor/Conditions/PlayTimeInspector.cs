﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(PlayTime))]
public class PlayTimeInspector : Editor {

    public override void OnInspectorGUI()
    {
        PlayTime myTarget = (PlayTime)target;

        GUILayout.BeginVertical();

        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("캠페인 플레이 시간이 ", GUILayout.Width(120));
        myTarget.Minutes = EditorGUILayout.FloatField(myTarget.Minutes, GUILayout.Width(30));
        EditorGUILayout.LabelField("분 지나면", GUILayout.Width(60));
        GUILayout.BeginHorizontal();

    }
}
