﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(PlayerDie))]
public class PlayerDieInspector : Editor {

	public override void OnInspectorGUI()
	{
		PlayerDie myTarget = (PlayerDie)target;

		GUILayout.BeginVertical();

		GUILayout.BeginHorizontal();
		myTarget.Character = (Character)EditorGUILayout.EnumPopup(myTarget.Character, GUILayout.Width(110));
		EditorGUILayout.LabelField("가 죽으면");
		GUILayout.EndHorizontal();

		GUILayout.EndVertical();
	}
}
