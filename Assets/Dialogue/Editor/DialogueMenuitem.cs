﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class DialogueMenuitem : MonoBehaviour {

    [MenuItem("GameObject/Dialogue/New Dialogue", priority = 0)]
    static void CreateNewDialogue()
    {
        // Create New Dialogue
        // Select its name
        int count = 1;
        string name = "";

        Dialogue[] dialogues = FindObjectsOfType<Dialogue>();
        for (int i = 0; i < dialogues.Length; ++i)
        {
            name = "New Dialogue " + count;
            foreach (Dialogue dialogue in dialogues)
            {
                if (dialogue.name.Equals(name))
                {
                    count++;
                    break;
                }
            }
        }

        GameObject newDialogue = new GameObject("New Dialogue " + count);
        newDialogue.transform.position = Vector3.zero;
        newDialogue.transform.rotation = Quaternion.identity;
        newDialogue.AddComponent<Dialogue>();

        // If "Dialogue System" gameobject exsists, set the new dialogue as a child
        DialogueManager dialogueSystem = FindObjectOfType<DialogueManager>();
        if (dialogueSystem != null)
        {
            newDialogue.transform.parent = dialogueSystem.transform;
        }

        // Add child of "Conditions"
        GameObject conditions = new GameObject("Conditions");
        conditions.transform.parent = newDialogue.transform;
        conditions.transform.localPosition = Vector3.zero;
        conditions.transform.localRotation = Quaternion.identity;
        conditions.AddComponent<Conditions>();


        // Add child of "Action Nodes"
        GameObject actionNodes = new GameObject("Action Nodes");
        actionNodes.transform.parent = newDialogue.transform;
        actionNodes.transform.localPosition = Vector3.zero;
        actionNodes.transform.localRotation = Quaternion.identity;
        actionNodes.AddComponent<ActionNodes>();
    }

    [MenuItem("GameObject/Dialogue/Dialogue System", priority = 0)]
    static void CreateDialogueSystem()
    {
        DialogueManager system = FindObjectOfType<DialogueManager>();
        if (system == null)
        {
            GameObject newSystem = new GameObject("Dialogue System");
            newSystem.transform.position = Vector3.zero;
            newSystem.transform.rotation = Quaternion.identity;
            newSystem.AddComponent<DialogueManager>();
        }
        else
        {
            Debug.LogError("Dialogue System is already exsists. Check whether there is the gameobject which contains DialogueManager script.");
        }

        EventObserver observer = FindObjectOfType<EventObserver>();
        if (observer == null)
        {
            GameObject newObserver = new GameObject("Event Observer");
            newObserver.transform.position = Vector3.zero;
            newObserver.transform.rotation = Quaternion.identity;
            newObserver.AddComponent<EventObserver>();

            GameObject child1 = new GameObject("Location Observer");
            child1.transform.parent = newObserver.transform;
            child1.transform.localPosition = Vector3.zero;
            child1.transform.localRotation = Quaternion.identity;
            child1.AddComponent<LocationObserver>();
            child1.AddComponent<LocationCollector>();

            GameObject child2 = new GameObject("Data Observer");
            child2.transform.parent = newObserver.transform;
            child2.transform.localPosition = Vector3.zero;
            child2.transform.localRotation = Quaternion.identity;
            child2.AddComponent<SwitchManager>();
        }
        else
        {
            Debug.LogError("Event Observer is already exsists. Check whether there is the gameobject which contains EventObserver script.");
        }
    }
}
