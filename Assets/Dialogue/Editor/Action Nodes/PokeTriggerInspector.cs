﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using Action;

[CustomEditor(typeof(PokeTrigger))]
public class PokeTriggerInspector : Editor {

    public override void OnInspectorGUI()
    {
        PokeTrigger myTarget = (PokeTrigger)target;

        GUILayout.BeginVertical();

        GUILayout.BeginHorizontal();
        myTarget.Trigger = (TriggerBehaviour)EditorGUILayout.ObjectField(myTarget.Trigger, typeof(TriggerBehaviour), true, GUILayout.Width(200));
        EditorGUILayout.LabelField("를 콕 찌릅니다.", GUILayout.Width(180));
        GUILayout.EndHorizontal();

		EditorGUILayout.Space();
		myTarget.NextActionNode = (BaseAction)EditorGUILayout.ObjectField("Next Action Node", myTarget.NextActionNode, typeof(BaseAction), true);

		GUILayout.EndVertical();
    }
}
