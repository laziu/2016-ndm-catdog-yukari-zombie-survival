﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using Action;

[CustomEditor(typeof(Timer))]
public class TimerInspector : Editor {

	public override void OnInspectorGUI()
	{
		Timer myTarget = (Timer)target;

		EditorGUILayout.BeginHorizontal();
		myTarget.TimeLimit = EditorGUILayout.FloatField(myTarget.TimeLimit, GUILayout.Width(30));
		EditorGUILayout.LabelField("초 후에", GUILayout.Width(50));
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.BeginHorizontal();
		myTarget.NextActionNode = (BaseAction)EditorGUILayout.ObjectField(myTarget.NextActionNode, typeof(BaseAction), true, GUILayout.Width(160));
		EditorGUILayout.LabelField("을(를) 실행합니다.", GUILayout.Width(100));
		EditorGUILayout.EndHorizontal();
	}
}
