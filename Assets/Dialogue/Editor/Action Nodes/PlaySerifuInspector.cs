﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using Action;

[CustomEditor(typeof(PlaySerifu))]
public class PlaySerifuInspector : Editor {

	private string[] m_options = { "기다리고", "기다리지 않고" };
	private int m_selected;

	public override void OnInspectorGUI()
	{
		PlaySerifu myTarget = (PlaySerifu)target;

		EditorGUILayout.BeginHorizontal();
		myTarget.Character = (Character)EditorGUILayout.EnumPopup(myTarget.Character, GUILayout.Width(110));
		EditorGUILayout.LabelField("가");
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.BeginHorizontal();
		myTarget.Serifu = (AudioClip)EditorGUILayout.ObjectField(myTarget.Serifu, typeof(AudioClip), true, GUILayout.Width(160));
		EditorGUILayout.LabelField("라고 말합니다.");
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.Space();
		EditorGUILayout.LabelField("자막");
		myTarget.Subtitle = EditorGUILayout.TextArea(myTarget.Subtitle, GUILayout.Height(30));

		EditorGUILayout.Space();
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField("대사가 끝날 때까지", GUILayout.Width(100));
		m_selected = myTarget.Wait ? 0 : 1;
		m_selected = EditorGUILayout.Popup(m_selected, m_options, GUILayout.Width(90));
		myTarget.Wait = m_selected == 0 ? true : false;
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.BeginHorizontal();
		myTarget.NextActionNode = (BaseAction)EditorGUILayout.ObjectField(myTarget.NextActionNode, typeof(BaseAction), true, GUILayout.Width(160));
		EditorGUILayout.LabelField("을(를) 실행합니다.", GUILayout.Width(100));
		EditorGUILayout.EndHorizontal();
	}
}
