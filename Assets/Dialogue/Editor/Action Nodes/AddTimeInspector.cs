﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using Action;

[CustomEditor(typeof(AddTime))]
public class AddTimeInspector : Editor {

	public override void OnInspectorGUI()
	{
		AddTime myTarget = (AddTime)target;

		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField("캠페인 타이머에 ", GUILayout.Width(80));
		myTarget.Seconds = EditorGUILayout.IntField(myTarget.Seconds, GUILayout.Width(50));
		EditorGUILayout.LabelField("초 추가", GUILayout.Width(50));
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.Space();
		myTarget.NextActionNode = (BaseAction)EditorGUILayout.ObjectField("Next Action Node", myTarget.NextActionNode, typeof(BaseAction), true);
	}
}
