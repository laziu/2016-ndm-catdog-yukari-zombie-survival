﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using Action;

[CustomEditor(typeof(DebugLog))]
public class DebugLogInpector : Editor {

	public override void OnInspectorGUI()
	{
		DebugLog myTarget = (DebugLog)target;

		myTarget.Text = EditorGUILayout.TextArea(myTarget.Text, GUILayout.Height(30));

		EditorGUILayout.Space();

		myTarget.NextActionNode = (BaseAction)EditorGUILayout.ObjectField("Next Action Node", myTarget.NextActionNode, typeof(BaseAction), true);
	}
}
