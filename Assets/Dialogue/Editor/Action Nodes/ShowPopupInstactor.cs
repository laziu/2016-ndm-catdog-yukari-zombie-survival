﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using Action;

[CustomEditor(typeof(ShowPopup))]
public class ShowPopupInstactor : Editor {

	public override void OnInspectorGUI()
	{
		ShowPopup myTarget = (ShowPopup)target;

		EditorGUILayout.BeginHorizontal();
		myTarget.Option = (PopupOption)EditorGUILayout.EnumPopup(myTarget.Option, GUILayout.Width(100));
		EditorGUILayout.LabelField("설명 팝업을 보입니다.");
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.Space();
		myTarget.NextActionNode = (BaseAction)EditorGUILayout.ObjectField("Next Action Node", myTarget.NextActionNode, typeof(BaseAction), true);
	}
}
