﻿using UnityEngine;
using UnityEditor;
using Action;
using System.Collections;

[CustomEditor(typeof(EnablePlayer))]
public class EnablePlayerInspector : Editor {

	private string[] _options = { "활성화", "비활성화" };
	private int _selected;

	public override void OnInspectorGUI()
	{
		EnablePlayer myTarget = (EnablePlayer)target;

		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField("로컬 플레이어를", GUILayout.Width(90));
		_selected = myTarget.Enable ? 0 : 1;
		_selected = EditorGUILayout.Popup(_selected, _options, GUILayout.Width(70));
		myTarget.Enable = _selected == 0 ? true : false;
		EditorGUILayout.LabelField("합니다.");
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.Space();
		myTarget.NextActionNode = (BaseAction)EditorGUILayout.ObjectField("Next Action Node", myTarget.NextActionNode, typeof(BaseAction), true);
	}
}
