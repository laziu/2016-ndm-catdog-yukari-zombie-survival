﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using Action;

[CustomEditor(typeof(ChangeSCG))]
public class ChangeSCGInspector : Editor {

	private int m_selected;

	public override void OnInspectorGUI()
	{
		ChangeSCG myTarget = (ChangeSCG)target;

		EditorGUILayout.BeginHorizontal();
		myTarget.Character = (Character)EditorGUILayout.EnumPopup(myTarget.Character, GUILayout.Width(110));
		EditorGUILayout.LabelField("의 SCG를");
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.BeginHorizontal();
		m_selected = myTarget.SpriteIndex;
		switch (myTarget.Character)
		{
			case Character.YuzukiYukari:
				m_selected = EditorGUILayout.Popup(m_selected, SCGPath.YukariSCGOption, GUILayout.Width(90));
				break;

			case Character.TsurumakiMaki:
				m_selected = EditorGUILayout.Popup(m_selected, SCGPath.MakiSCGOption, GUILayout.Width(90));
				break;

			case Character.KotonohaAkane:
				m_selected = EditorGUILayout.Popup(m_selected, SCGPath.AkaneSCGOption, GUILayout.Width(90));
				break;

			case Character.KotonohaAoi:
				m_selected = EditorGUILayout.Popup(m_selected, SCGPath.AoiSCGOption, GUILayout.Width(90));
				break;
		}
		myTarget.SpriteIndex = m_selected;
		EditorGUILayout.LabelField("(으)로 변경합니다.");
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.Space();
		myTarget.NextActionNode = (BaseAction)EditorGUILayout.ObjectField("Next Action Node", myTarget.NextActionNode, typeof(BaseAction), true);
	}
}
