﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using Action;

[CustomEditor(typeof(ShowSCG))]
public class ShowSCGInspector : Editor {

	public override void OnInspectorGUI()
	{
		ShowSCG myTarget = (ShowSCG)target;

		EditorGUILayout.BeginHorizontal();
		myTarget.Character = (Character)EditorGUILayout.EnumPopup(myTarget.Character, GUILayout.Width(110));
		EditorGUILayout.LabelField("의 SCG를 보입니다.");
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.Space();
		myTarget.NextActionNode = (BaseAction)EditorGUILayout.ObjectField("Next Action Node", myTarget.NextActionNode, typeof(BaseAction), true);
	}
}
