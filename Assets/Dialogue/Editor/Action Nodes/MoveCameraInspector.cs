﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using Action;

[CustomEditor(typeof(MoveCamera))]
public class MoveCameraInspector : Editor {

	public override void OnInspectorGUI()
	{
		MoveCamera myTarget = (MoveCamera)target;

		EditorGUILayout.BeginVertical();

		myTarget.Destination = (Transform)EditorGUILayout.ObjectField("Destination" ,myTarget.Destination, typeof(Transform), true);
		myTarget.Zoom = EditorGUILayout.FloatField("Zoom", myTarget.Zoom);
		myTarget.TimeLength = EditorGUILayout.FloatField("Time Length", myTarget.TimeLength);
		myTarget.Elastic = EditorGUILayout.Toggle("Elastic", myTarget.Elastic);
		myTarget.NextActionNode = (BaseAction)EditorGUILayout.ObjectField("Next Action Node", myTarget.NextActionNode, typeof(BaseAction), true);

		EditorGUILayout.EndVertical();
	}
}
