﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using Action;

[CustomEditor(typeof(ActionNodes))]
public class DialogueActionNodesInspector : Editor {

	private ActionType m_selectedActionType;

	public override void OnInspectorGUI()
	{
		ActionNodes myTarget = (ActionNodes)target;

		// Start action select field
		myTarget.StartActionNode = (GameObject)EditorGUILayout.ObjectField("Start Action Node", myTarget.StartActionNode, typeof(GameObject), true);

		EditorGUILayout.Space();

		// Add Action Node Button
		GUILayout.BeginHorizontal();
		m_selectedActionType = (ActionType)EditorGUILayout.EnumPopup(m_selectedActionType, GUILayout.Width(100));

		if (GUILayout.Button("Add Action Node"))
		{
			CreateNewActionNode(myTarget);
		}
		GUILayout.EndHorizontal();
	}

	private void CreateNewActionNode(ActionNodes myTarget)
	{
		GameObject go = new GameObject(SelectName());
		go.transform.parent = myTarget.transform;

		switch (m_selectedActionType)
		{
			case ActionType.DebugLog:
				go.AddComponent<DebugLog>();
				break;

			case ActionType.Timer:
				go.AddComponent<Timer>();
				break;

			case ActionType.PlaySerifu:
				go.AddComponent<PlaySerifu>();
				break;

			case ActionType.ShowSCG:
				go.AddComponent<ShowSCG>();
				break;

			case ActionType.HideSCG:
				go.AddComponent<HideSCG>();
				break;

			case ActionType.ChangeSCG:
				go.AddComponent<ChangeSCG>();
				break;

			case ActionType.SetSwitch:
				go.AddComponent<SetSwitch>();
				break;

            case ActionType.PokeTrigger:
                go.AddComponent<PokeTrigger>();
                break;

			case ActionType.Fading:
				go.AddComponent<Fading>();
				break;

			case ActionType.MoveCamera:
				go.AddComponent<MoveCamera>();
				break;

			case ActionType.ShowPopup:
				go.AddComponent<ShowPopup>();
				break;

			case ActionType.EnablePlayer:
				go.AddComponent<EnablePlayer>();
				break;

			case ActionType.ShowBlood:
				go.AddComponent<ShowBlood>();
				break;

			case ActionType.AddTime:
				go.AddComponent<AddTime>();
				break;
		}

		// 지금 만든 Action Node가 현재 유일한 Action Node라면
		// Start Action Node로 설정해 준다.
		if(myTarget.transform.childCount == 1)
		{
			myTarget.StartActionNode = go;
		}
	}

	// 새로만들 Action Node의 이름을 정한다.
	private string SelectName()
	{
		string namePrefix = "";

		switch (m_selectedActionType)
		{
			case ActionType.DebugLog:
				namePrefix = "Debug Log ";
				break;

			case ActionType.Timer:
				namePrefix = "Timer ";
				break;

			case ActionType.PlaySerifu:
				namePrefix = "Play Serifu ";
				break;

			case ActionType.ShowSCG:
				namePrefix = "Show SCG ";
				break;

			case ActionType.HideSCG:
				namePrefix = "Hide SCG ";
				break;

			case ActionType.ChangeSCG:
				namePrefix = "Change SCG ";
				break;

			case ActionType.SetSwitch:
				namePrefix = "Set Switch ";
				break;

            case ActionType.PokeTrigger:
                namePrefix = "Poke Trigger ";
                break;

			case ActionType.Fading:
				namePrefix = "Fading ";
				break;

			case ActionType.MoveCamera:
				namePrefix = "Move Camera ";
				break;

			case ActionType.ShowPopup:
				namePrefix = "Show Popup ";
				break;

			case ActionType.EnablePlayer:
				namePrefix = "Enable Player ";
				break;

			case ActionType.ShowBlood:
				namePrefix = "Show blood ";
				break;

			case ActionType.AddTime:
				namePrefix = "Add Time ";
				break;
		}

		int count = 1;
		string name = namePrefix + count;

		BaseAction[] actions = FindObjectsOfType<BaseAction>();
		for (int i = 0; i < actions.Length; ++i)
		{
			name = namePrefix + count;
			foreach (BaseAction action in actions)
			{
				if (action.name.Equals(name))
				{
					count++;
					break;
				}
			}
		}

		return namePrefix + count;
	}
}
