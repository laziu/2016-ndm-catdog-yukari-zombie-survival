﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using Action;

[CustomEditor(typeof(Fading))]
public class FadingInspector : Editor {

	public override void OnInspectorGUI()
	{
		Fading myTarget = (Fading)target;

		EditorGUILayout.BeginHorizontal();
		myTarget.FadingTime = EditorGUILayout.FloatField(myTarget.FadingTime, GUILayout.Width(30));
		EditorGUILayout.LabelField("초 동안", GUILayout.Width(50));
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.BeginHorizontal();
		myTarget.Option = (FadingOption)EditorGUILayout.EnumPopup(myTarget.Option, GUILayout.Width(70));
		EditorGUILayout.LabelField("합니다.", GUILayout.Width(100));
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.Space();
		myTarget.NextActionNode = (BaseAction)EditorGUILayout.ObjectField("Next Action Node", myTarget.NextActionNode, typeof(BaseAction), true);
	}
}
