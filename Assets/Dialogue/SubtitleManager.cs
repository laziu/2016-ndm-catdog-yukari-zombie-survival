﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class SubtitleManager : MonoBehaviour {

	#region Singleton
	private static SubtitleManager m_instance = null;
	public static SubtitleManager Instance
	{
		get
		{
			if (m_instance == null) Debug.LogError("SubtitleManager has not awaken.");
			return m_instance;
		}
	}
    #endregion

    public Font font;

	[Space]
	public Color yukariColor;
	public Color makiColor;
	public Color akaneColor;
	public Color aoiColor;

	void Awake()
	{
		if (m_instance != null)
		{
			Destroy(this.gameObject);
			return;
		}
		else
		{
			m_instance = this;
		}
	}

	// Requester and its corresponing subtitle object is placed at the same position(index) of each queue
	private Queue<PlaySerifu> m_requesterQueue = new Queue<PlaySerifu>();									// Queue of requester
	private Queue<GameObject> m_subtitleQueue = new Queue<GameObject>();									// Queue of printed subtitles

	// Use this for initialization
	void Start () {
        if (font == null) Debug.Log("SubtitleManager : Font not found.");
	}
	
	// Update is called once per frame
	void Update () {
		// Check whether the front serifu is playing or not
		if(m_requesterQueue.Count > 0)
		{
			PlaySerifu front = m_requesterQueue.Peek();

			// if front serifu is end then remove its subtitle
			if(!SerifuManager.Instance.IsPlaying(front))
			{
				m_requesterQueue.Dequeue();
				RemoveSubtitle(m_subtitleQueue.Dequeue());
			}
		}
	}

	public void PrintSubtitle(PlaySerifu requester)
	{
		// Make and enqueue a new text ui
		GameObject subtitleObject = CreateSubtitle(requester.Character, requester.Subtitle);
		m_requesterQueue.Enqueue(requester);
		m_subtitleQueue.Enqueue(subtitleObject);

		// Set proper position
		RectTransform rt = subtitleObject.GetComponent<RectTransform>();
		rt.anchoredPosition = new Vector2(0, -480);
	}

	private GameObject CreateSubtitle(Character character, string subtitle)
	{
		GameObject newSubtitle = new GameObject("Subtitle");
		newSubtitle.transform.SetParent(transform, false);

		RectTransform rt = newSubtitle.AddComponent<RectTransform>();
		rt.anchoredPosition = Vector2.zero;
		rt.sizeDelta = new Vector2(1000, 100);

		newSubtitle.AddComponent<CanvasRenderer>();

		Text text = newSubtitle.AddComponent<Text>();
        //text.font = Font.CreateDynamicFontFromOSFont("malgun.ttf", 36);
        text.font = font;
		text.alignment = TextAnchor.MiddleCenter;
		text.fontStyle = FontStyle.Bold;
		text.fontSize = 36;
		text.color = new Color(255, 255, 255);

		ContentSizeFitter fitter = newSubtitle.AddComponent<ContentSizeFitter>();
		fitter.horizontalFit = ContentSizeFitter.FitMode.Unconstrained;
		fitter.verticalFit = ContentSizeFitter.FitMode.PreferredSize;

		Outline outline = newSubtitle.AddComponent<Outline>();
		outline.effectDistance = new Vector2(1, -1);

        outline.effectColor = new Color(0, 0, 0);
		switch(character)
		{
			case Character.YuzukiYukari:
                text.color = yukariColor;
                //outline.effectColor = new Color(150, 0, 180);
				text.text = "유카리 : " + subtitle;
				break;

			case Character.TsurumakiMaki:
                text.color = makiColor;
                //outline.effectColor = new Color(215, 190, 0);
                text.text = "마키 : " + subtitle;
				break;

			case Character.KotonohaAkane:
                text.color = akaneColor;
                //outline.effectColor = new Color(255, 0, 90);
                text.text = "아카네 : " + subtitle;
				break;

			case Character.KotonohaAoi:
                text.color = aoiColor;
                //outline.effectColor = new Color(0, 160, 255);
                text.text = "아오이 : " + subtitle;
				break;
		}

		return newSubtitle;
	}

	private void RemoveSubtitle(GameObject subtitle)
	{
		Destroy(subtitle);
	}
}
