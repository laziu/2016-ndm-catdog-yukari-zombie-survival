﻿using UnityEngine;
using System.Collections;
using Action;
using System;

[Serializable]
public class ActionNodes : MonoBehaviour {

	[SerializeField]
	private GameObject m_startActionNode;

	private BaseAction s_startActionNode;

	// The first action starts when the conditions are satisfied
	public GameObject StartActionNode { get { return m_startActionNode; } set { m_startActionNode = value; } }

	// Dialogue
	public Dialogue Parent { get; set; }

	// Use this for initialization
	void Start () {
		if (StartActionNode == null) Debug.LogError(transform.parent.name + " : Start Action Node is null.");
		else s_startActionNode = StartActionNode.GetComponent<BaseAction>();

		if (s_startActionNode == null) Debug.LogError(transform.parent.name + " : Start Action Node is not valid.");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	/// <summary>
	/// Start first action node and sent parent info
	/// </summary>
	public void StartAction()
	{
		s_startActionNode.Parent = this;
		s_startActionNode.Active = true;

		Debug.Log(Parent.name + " starts.");
	}

	/// <summary>
	/// Set Dialogue.IsActive to false
	/// </summary>
	public void Done()
	{
		Parent.IsActive = false;

		Debug.Log(Parent.name + " is done.");
	}
}
