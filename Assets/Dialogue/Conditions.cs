﻿using UnityEngine;
using System.Collections;
using EventCondition;
using System;

[Serializable]
public class Conditions : MonoBehaviour {

	private bool m_dirty = false;

	[SerializeField]
	private bool m_playOnce = true;
	[SerializeField]
	private bool m_played;
	
	// Play only once if true
	public bool PlayOnce { get { return m_playOnce; } set { m_playOnce = value; } }

	// Has it played at least once?
	public bool Played { get { return m_played; } set { m_played = value; } }

	// Use this for initialization
	void Start () {
		Played = false;
	}
	
	// Update is called once per frame
	void Update () {
		if(m_dirty)
		{
			bool allSatisfies = true;
			
			// Does every EventCondition satisfies?
			for(int i=0; i<transform.childCount; ++i)
			{
				BaseEventCondition ec = transform.GetChild(i).GetComponent<BaseEventCondition>();
				if(ec == null)
				{
					Debug.LogError(transform.parent.name + " : This dialogue has some wrong child. It should be removed.");
					continue;
				}

				if (ec.Satisfies) continue;
				else
				{
					allSatisfies = false;
					break;
				}
			}

			// If all of conditions be satisfied than start dialogue
			if(allSatisfies)
			{
				// Send this dialogue to DialogueManager if this is not played
				// DialogueManager place this dialogue into the queue and activate it when the privious dialouges finish
				if(!Played) DialogueManager.Instance.WaitToStart(transform.parent.GetComponent<Dialogue>());

				Played = true;
			}

			m_dirty = false;
		}
	}

	// Message that Event Condition Objects will throw when they detect their events
	// then, Condition Block starts to check event
	public void SetDirty()
	{
		m_dirty = true;
	}
}
