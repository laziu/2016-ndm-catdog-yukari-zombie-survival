﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class TireMotion : MonoBehaviour {

    [SerializeField]
    private GameObject[] tires = new GameObject[4];

    private Rigidbody _rigidbody;

    void Start() {
        _rigidbody = GetComponent<Rigidbody>();
    }

    void Update() {
        var velocity = transform.InverseTransformDirection(_rigidbody.velocity);
        var speed = velocity.z;
        var deltaAngle = speed * Time.deltaTime / Mathf.PI * 180f;
        foreach ( var tire in tires ) {
            tire.transform.Rotate(deltaAngle, 0, 0);
//            var rotation = tire.transform.localRotation.eulerAngles;
//            rotation.x += deltaAngle;
//            tire.transform.localRotation = Quaternion.Euler(rotation);
        }
	}
}
