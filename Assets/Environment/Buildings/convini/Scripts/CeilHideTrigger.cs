﻿using UnityEngine;

/// <summary>
/// Ceil and wall hide behavior of convini
/// </summary>
[RequireComponent(typeof(Collider))]
public class CeilHideTrigger : MonoBehaviour {

    [SerializeField]
    private GameObject _mesh;           // mesh to hide

    private int _triggerCount = 0;      // triggered count in volume

    private Renderer[] _renderers;      // real mesh to hide

    void Start() {
        _renderers = _mesh.GetComponentsInChildren<Renderer>();
    }

    void OnTriggerEnter(Collider col) {
        if ( col.tag == "Player" )
            if ( _triggerCount++ == 0 )
                foreach ( var r in _renderers )
                    r.enabled = false;
    }

    void OnTriggerExit(Collider col) {
        if ( col.tag == "Player" )
            if ( --_triggerCount == 0 )
                foreach ( var r in _renderers )
                    r.enabled = true;
    }
}
