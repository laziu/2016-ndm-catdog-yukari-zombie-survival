﻿using UnityEngine;

/// <summary>
/// Door open behavior of convini
/// </summary>
[RequireComponent(typeof(Collider))]
public class DoorOpenTrigger : MonoBehaviour {

    [SerializeField]
    private Transform _doorLeft;        // left door object
    [SerializeField]
    private Transform _doorRight;       // right door object

    private float _speed = 10.0f;       // door move speed
    private float _offset = 14.0f;      // door open offset

    private int _triggerCount = 0;      // triggered count in volume
    private float _velocity = 0f;       // current door velocity
    private float _position = 0f;       // current door position

	void Update() {
        if (_triggerCount > 0) {
            if (_position < _offset) {
                _velocity = Mathf.Min(Mathf.Max(0, _velocity) + 2 * _speed * Time.deltaTime, _speed);
                _position += _velocity * Time.deltaTime;
                _doorLeft .localPosition = WithX(_doorLeft .localPosition, -_position);
                _doorRight.localPosition = WithX(_doorRight.localPosition,  _position);
            }
        }
        else {
            if (_position > 0f) {
                _velocity = Mathf.Max(Mathf.Min(0, _velocity) - 2 * _speed * Time.deltaTime, -_speed);
                _position += _velocity * Time.deltaTime;
                _doorLeft .localPosition = WithX(_doorLeft .localPosition, -_position);
                _doorRight.localPosition = WithX(_doorRight.localPosition,  _position);
            }
        }
    }

    void OnTriggerEnter(Collider col) {
        if ( col.gameObject.layer == LayerMask.NameToLayer("Player") ||
				col.gameObject.layer == LayerMask.NameToLayer("Zombie"))
            ++_triggerCount;
    }

    void OnTriggerExit(Collider col) {
		if (col.gameObject.layer == LayerMask.NameToLayer("Player") ||
				col.gameObject.layer == LayerMask.NameToLayer("Zombie"))
			--_triggerCount;
    }

    private static Vector3 WithX(Vector3 v, float x) {
        return new Vector3(x, v.y, v.z);
    }
}